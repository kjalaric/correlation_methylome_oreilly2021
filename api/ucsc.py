'''
API handler for UCSC genome browser.

https://genome.ucsc.edu/goldenPath/help/api.html
'''

import os
import json
import requests
from bs4 import BeautifulSoup
from file_constants import *
from database.simplecg import *

r_url = "https://api.genome.ucsc.edu/"
UCSC_HGTRACKS_SEARCH_URL = "https://genome.ucsc.edu/cgi-bin/hgTracks?&org={}&db={}&position={}"  # organism, assembly, gene
USCS_CGSEARCH_URL = "https://genome.ucsc.edu/cgi-bin/hgTracks?position={}&db={}"
SEARCH_DELAY = 1.0

def _test():
    print(json.dumps(requests.get("https://api.genome.ucsc.edu/list/ucscGenomes").json(), indent = 4, sort_keys = True))


def getFromServer():
    r_params = {}
    r = requests.get(url = r_url, params = r_params)
    return r


def ucscHgTracksSearch(gene, organism="Human", assembly="hg38", save_html=False):  # hg19 is GRCh37
    '''
    I don't know if this is always going to work. It is reverse-engineered from the search on the website.
    '''
    r_url = UCSC_HGTRACKS_SEARCH_URL.format(organism, assembly, gene)
    r_params = {}
    r = requests.get(url = r_url, params = r_params)

    if save_html:
        with open(os.path.join(UCSC_SEARCH_CACHE, gene + ".html"), "wb") as fo:
            fo.write(r.content)

    return r.content


def ucscSearchCg(cg, assembly="hg19", override=False, ignoreDb=False, database_loc=SIMPLE_CG_DATABASE_LOCATION):
    if not ignoreDb:
        db = SimpleCgDatabaseHandler(database_loc)
        if not override:
            search = db.findCg(cg)
            if search is not None:
                return search[1]

    r_url = USCS_CGSEARCH_URL.format(cg, assembly)

    r_params = {}
    r = requests.get(url = r_url, params = r_params)

    soup = BeautifulSoup(r.content, 'html.parser')

    print(soup)

    c = soup.find('map', attrs={"name": "map_side_knownGene"})  # we are pulling this out from the gui directly

    gene = str(c).split("title=\"")[1].split("\"/>")[0]

    if not ignoreDb:
        db.write(cg, gene)
        db.commit()
        db.close()

    return gene

if __name__ == "__main__":
    print(ucscSearchCg("chr21:3577855", ignoreDb=True))