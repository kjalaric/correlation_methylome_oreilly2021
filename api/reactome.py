'''
API handler for reactome database.
See https://reactome.org/dev
'''

import os
import requests
import json
import re
import pickle as pkl
import common.file_format

try:
    from bs4 import BeautifulSoup
    _bs4_available = True
except:
    print("BS4 not available in this environment. Advanced functions will not be available.")
    _bs4_available = False


REACTOME_SEARCH_URL = "https://reactome.org/ContentService/search/query?query={}&species=Homo%20sapiens&cluster=true"
REACTOME_PATHWAY_DB_SEARCH_URL = "https://reactome.org/ContentService/data/pathway/{}/containedEvents"
REACTOME_PATHWAY_ENTITY_DB_SEARCH_URL = "https://reactome.org/ContentService/data/participants/{}/participatingPhysicalEntities"
REACTOME_DETAIL_DB_SEARCH_URL = "https://reactome.org/content/detail/{}#Homo%20sapiens"
REACTOME_PATHWAY_SEARCH_LOW_LEVEL_FROM_ID = "https://reactome.org/ContentService/data/pathways/low/diagram/entity/{}?species=9606"  # ID


class CacheLoc(object):
    reactome = os.path.join(common.file_format.cache_dir, "reactome/")
    search = os.path.join(reactome, "search/")
    pathway = os.path.join(reactome, "pathway/")
    entity = os.path.join(reactome, "entity/")
    detail = os.path.join(reactome, "detail/")


def init():
    # initialises the search cache if it isn't already there

    for directory in [
        CacheLoc.search,
        CacheLoc.pathway,
        CacheLoc.entity,
        CacheLoc.detail
    ]:
        if not os.path.exists(directory):
            os.mkdir(directory)



def searchReactome(search_query, save_json=True, override_cache=False, verbose=False):
    json_loc = os.path.join(CacheLoc.search, search_query + ".json")

    if not override_cache:
        if os.path.exists(json_loc):
            if verbose:
                print(f"Search for {search_query} exists in cache.")
            with open(json_loc, "r") as fi:
                return json.load(fi)

    r_params = {}
    r_url = REACTOME_SEARCH_URL.format(search_query.replace(" ", "%20"))
    r = requests.get(url=r_url, params=r_params)

    if save_json:
        with open(json_loc, "w+") as fo:
            json.dump(r.json(), fo)

    return r.json()


def findGeneProductId(gene_name, save_json=True, override_cache=False, verbose=False):
    """
    :param gene_name:
    :param save_json:
    :param override_cache:
    :param verbose:
    :return: gene_id, exact_type
    """
    # use at your own risk
    search_results = searchReactome(gene_name, save_json, override_cache, verbose)

    # no search results: return None
    try:
        if search_results['code'] == 404:
            return None, None
    except KeyError:
        pass

    # some search results: return first hit for name
    try:
        for results in search_results["results"]:
            for entry in results["entries"]:
                try:
                    if entry['name'].split(">")[1].split("<")[0].upper() == gene_name.upper():
                        return entry['id'], entry['exactType']
                except IndexError:  # sometimes they're formatted normally so the above will throw an index error
                    if entry['name'].upper() == gene_name.upper():
                        return entry['id'], entry['exactType']
    except KeyError:
        pass

    # some search results but none match name: return first hit anyway

    for results in search_results["results"]:
        for entry in results["entries"]:
            return entry['id'], entry['exactType']



    return None, None


def searchPathwayDatabase(id, save_json=True, override_cache=False, verbose=False):
    json_loc = os.path.join(CacheLoc.pathway, id + ".json")

    if not override_cache:
        if os.path.exists(json_loc):
            if verbose:
                print(f"Search for {id} exists in cache.")
            with open(json_loc, "r") as fi:
                return json.load(fi)

    r_params = {}
    r_url = REACTOME_PATHWAY_DB_SEARCH_URL.format(id)
    r = requests.get(url = r_url, params = r_params)

    if save_json:
        with open(json_loc, "w+") as fo:
            json.dump(r.json(), fo)

    return r.json()


def searchEntityWithinPathway(entity_id, save_json=True, override_cache=False, verbose=False):
    json_loc = os.path.join(CacheLoc.entity, entity_id + "_entities.json")

    if not override_cache:
        if os.path.exists(json_loc):
            if verbose:
                print(f"Search for {entity_id} exists in cache.")
            with open(json_loc, "r") as fi:
                return json.load(fi)

    r_params = {}
    r_url = REACTOME_PATHWAY_ENTITY_DB_SEARCH_URL.format(entity_id)
    r = requests.get(url = r_url, params = r_params)

    if save_json:
        with open(json_loc, "w+") as fo:
            json.dump(r.json(), fo)

    return r.json()


def searchPathwaysFromId(id_, save_json=True, override_cache=False, verbose=False):
    json_loc = os.path.join(CacheLoc.pathway, id_ + "_low.json")

    if not override_cache:
        if os.path.exists(json_loc):
            if verbose:
                print(f"Search for {id_} exists in cache.")
            with open(json_loc, "r") as fi:
                return json.load(fi)

    r_params = {}
    r_url = REACTOME_PATHWAY_SEARCH_LOW_LEVEL_FROM_ID.format(id_)
    r = requests.get(url=r_url, params=r_params)

    if save_json:
        with open(json_loc, "w+") as fo:
            json.dump(r.json(), fo)

    return r.json()


def getPathwaysAssociatedWithId(id_, save_json=True, override_cache=False, verbose=False):
    result = searchPathwaysFromId(id_, save_json, override_cache, verbose)
    pathways = list()
    for x in result:
        try:
            pathways.append(x['stId'])
        except KeyError:
            continue
        except TypeError as e:  # wtf
            if result['code'] == 404:
                continue
            else:
                print(e)
                print(id_)
                print(result)
                raise
    return pathways


def summationFromJson(fjson, exacttype='Pathway'):
    for x in fjson["results"]:
        for xx in x["entries"]:
            if xx['exactType'] == exacttype:
                yield f"{xx['id']}: {re.sub('<[^<]+?>', '', xx['summation'])}".strip("\n")


def idsFromJson(fjson, exacttype='Pathway'):
    if "results" not in fjson.keys():
        return

    for x in fjson["results"]:
        for xx in x["entries"]:
            if xx['exactType'] == exacttype:
                yield xx['id']


def summariseFromIdSearch(fjson):
    for x in fjson:
        yield f"{x['stId']}:" + '\n\t' + '\n\t'.join([q for q in x['name']]) + '\n'


def summariseFromEntitySearch(fjson):
    for x in fjson:
        yield f"{x['stId']}: {', '.join(x['name'])} - {x['displayName']}"


def csvLineFromEntitySearch(fjson):
    for x in fjson:
        yield f"{x['stId'].replace(',', ';')},{x['displayName'].replace(',', ';')},{'; '.join(x['name']).replace(',', ';')}\n"


if _bs4_available:
    def getDetail(detail_term, save_search=True, override_cache=False, verbose=False):
        search_loc = os.path.join(CacheLoc.detail, detail_term + ".pkl")
        r = None
        if not override_cache:
            if os.path.exists(search_loc):
                if verbose:
                    print(f"Search for {detail_term} exists in cache.")
                with open(search_loc, "rb") as fi:
                    r = pkl.load(fi)
        if r is None:
            r_params = {}
            r_url = REACTOME_DETAIL_DB_SEARCH_URL.format(detail_term.replace(" ", "%20"))
            r = requests.get(url = r_url, params = r_params)
            with open(search_loc, "wb") as fo:
                pkl.dump(r, fo)

        soup = BeautifulSoup(r.content, 'html.parser')
        # c = soup.find('h3', attrs={"class": "details-title"})
        c = str(soup.find('title')).split(" | ")[-1].split("</")[0]

        return c


    def getAllGenes(search_term, save_jsons=True, override_cache=False):
        search_json = searchReactome(search_term, save_jsons, override_cache)

        try:
            if search_json['code'] == 404:
                return []
        except KeyError:
            pass

        ids = list()
        for i in search_json["results"]:
            ids += [x["id"] for x in i["entries"]]
        ids = list(set(ids))

        pathways = list()
        not_found = list()
        for id_ in ids:
            pathway_response = searchPathwayDatabase(id_, save_jsons, override_cache)
            try:
                if pathway_response["code"] == 404:
                    not_found.append(id_)
                    continue
            except:
                pass

            for j in pathway_response:
                pathways.append(j["displayName"])  # there are more possible names under the "name" thing

        detailed = list()
        for x in not_found:
            detailed.append(getDetail(x, save_jsons, override_cache))

        return pathways + detailed



else:
    def getDetail(detail_term, save_jsons=True):
        raise Exception("BS4 not available in this environment.")

    def getAllGenes(search_term, save_jsons=True):
        raise Exception("BS4 not available in this environment.")





if __name__ == "__main__":
    # initialise
    init()
    for directory in [
        CacheLoc.search,
        CacheLoc.pathway,
        CacheLoc.entity,
        CacheLoc.detail
    ]:
        assert os.path.exists(directory)

    # unit test
    search_term = "nicotinamide riboside"
    search_response = searchReactome(search_term)
    print(search_response)
    assert os.path.exists(os.path.join(CacheLoc.search, search_term + ".json"))

    gene_search_term = "CD24"
    gene_search_response = findGeneId(gene_search_term)
    print(gene_search_response)
    assert gene_search_response == "R-HSA-430062"
    assert os.path.exists(os.path.join(CacheLoc.search, gene_search_term + ".json"))

    pathway_search_term = "R-HSA-197264"
    pathway_response = searchPathwayDatabase(pathway_search_term, save_json=True)
    print(pathway_response)
    assert os.path.exists(os.path.join(CacheLoc.pathway, pathway_search_term + ".json"))

    entity_search_term = "R-HSA-2309773"
    entity_response = searchEntityWithinPathway(entity_search_term, save_json=True)
    print(entity_response)
    assert os.path.exists(os.path.join(CacheLoc.entity, entity_search_term + "_entities.json"))

    detail_search_term = "R-ALL-549282"
    detail_response = getDetail(detail_search_term)
    print(detail_response)
    assert os.path.exists(os.path.join(CacheLoc.detail, detail_search_term + ".pkl"))




