import math
import pickle as pkl
import gtfparse
import common.file_format
import common.bio

_local_gtf_pkl_cache = {x.upper(): None for x in common.bio.chromosomes}


def _loadGencodeChromosome(chromosome):
    with open(common.file_format.pkl_gencode_chromosome.format(chromosome.upper()), "rb") as fi:
        return pkl.load(fi)


def _cacheHandleForChromosome(chromosome):
    if _local_gtf_pkl_cache[chromosome.upper()] is None:
        print(f"Loading chromosome {chromosome} into gencode cache...")
        _local_gtf_pkl_cache[chromosome.upper()] = _loadGencodeChromosome(chromosome.upper())
    return _local_gtf_pkl_cache[chromosome.upper()]


def _findFeature(df, locus):
    # binary search
    left = 0
    right = len(df) - 1
    m = 0
    while left <= right:
        m = math.floor((left + right)/2)
        position = df.iloc[m]
        if position['start'] > locus:
            right = m - 1
        else:
            if position['end'] > locus:
                return position["gene_name"], position["feature"], position["gene_type"]
            else:
                left = m + 1
    return None, None, None


# old version
def findAnnotatedFeature_oldversion(chromosome, locus, cache=True):
    if cache:
        if _local_gtf_pkl_cache[chromosome] is None:
            print(f"Loading chromosome {chromosome} into gencode cache...")
            with open(common.file_format.pkl_gencode_chromosome.format(chromosome), "rb") as fi:
                _local_gtf_pkl_cache[chromosome] = pkl.load(fi)
        df = _local_gtf_pkl_cache[chromosome]
    else:
        with open(common.file_format.pkl_gencode_chromosome.format(chromosome), "rb") as fi:
            df = pkl.load(fi)
    '''
    for i, feature in df.iterrows():
        if feature['end'] >= locus:
            if feature['start'] <= locus:
                # print(feature)
                return feature["gene_name"], feature["feature"], feature["gene_type"]
            else:
                break
    return None, None, None
    '''

    return _findFeature(df, locus)


def findAnnotatedFeature(chromosome, locus, cache=True):
    if cache:
        df = _cacheHandleForChromosome(chromosome)
    else:
        df = _loadGencodeChromosome(chromosome)

    return _findFeature(df, locus)


def genesInChromosome(chromosome, cache=True):
    if cache:
        df = _cacheHandleForChromosome(chromosome)
    else:
        df = _loadGencodeChromosome(chromosome)

    for gene in df.gene_name.unique():
        yield gene


if __name__ == "__main__":
    for x in genesInChromosome("y"):
        print(x)

    # print(findAnnotatedFeature("X", 24072640))  # EIF2S3 in illumina manifest
    # print(findAnnotatedFeature("5", 140242373))  # several PCDHA genes
    # print(findAnnotatedFeature("22", 5011810))  # manifest does not have any entries
