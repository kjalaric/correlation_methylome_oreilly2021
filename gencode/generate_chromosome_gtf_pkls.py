import pickle as pkl
import gtfparse
import common.file_format


if __name__ == "__main__":
    gencode = gtfparse.read_gtf(common.file_format.gencode_v37_loc)
    for chromosome in gencode.seqname.unique():
        chromo_df = gencode[gencode["seqname"] == chromosome]
        with open(common.file_format.pkl_gencode_chromosome_notag.format(chromosome), "wb") as fo:
            pkl.dump(chromo_df, fo)
