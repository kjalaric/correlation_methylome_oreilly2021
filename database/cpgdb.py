'''
Functions to deal with sqlite3 cpg databases.
All very insecure and prone to injection, but that is fine.
'''

import sqlite3

'''
DATABASE SPECIFICATION:
single table: cpg
name TEXT, genes TEXT, chromosome TEXT, position INTEGER, in_epic INTEGER, in_450k INTEGER
    name is the cpg number (e.g. cg01234567)
    gene is the abbreviated gene name (e.g. ABC1)
    chromosome and position are self-explanatory
    in_epic and in_450k are booleans (0 or 1) specifying if they're in an array
'''

CSV_BUFFER_SIZE_LINES = 50
PROGRESS_UPDATE_RATE = 1000


class CpgDatabaseHandler(object):
    def __init__(self, database):
        self._database_loc = database
        self.conn = sqlite3.connect(database)
        self.c = self.conn.cursor()

    def __del__(self):
        try:
            self.conn.close()
        except:
            pass

    def initialise_table(self, tablename="cpg"):
        '''
        this should only be run for new databases
        uses python's sqlite3, dont think this is valid sql
        '''
        self.c.execute(f"CREATE TABLE IF NOT EXISTS {tablename} (name TEXT PRIMARY KEY, genes TEXT, chromosome TEXT, position INTEGER, in_epic INTEGER, in_450k INTEGER)")
        self.conn.commit()

    def write(self, name, genes, chromosome, position, in_epic=False, in_450k=False, table="cpg", commit=False):
        self.c.execute(f"REPLACE INTO {table} (name, genes, chromosome, position, in_epic, in_450k) VALUES (?,?,?,?,?,?)", (name, genes, chromosome, position, in_epic, in_450k))
        if commit: self.conn.commit()

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

    def getCpgData(self, cpg):
        self.c.execute(f'SELECT * FROM cpg WHERE name=?', (cpg,))
        return self.c.fetchone()

    def dumpToCsv(self, csv_name, table="cpg", append=False):
        num_records = 0
        writeline = ""
        buffcount = 0
        if not append:
            with open(csv_name, "w") as fo:
                fo.write("name, genes, chromosome, position, in_epic, in_450k\n")
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        else:
            with open(csv_name, "a") as fo:
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        print(f"Finished processing {num_records} records.")


class GeneDatabaseStream(CpgDatabaseHandler):
    def __init__(self, database):
        self._database_loc = database
        self.conn = None
        self.c = None

    def __enter__(self):
        self.conn = sqlite3.connect(self._database_loc)
        self.c = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.conn.close()



if __name__ == "__main__":
    with GeneDatabaseStream("K:/database/cpg.db") as db:
        print(db.getCpgData("cg14797042"))  # epic only
        print(db.getCpgData("cg01086462"))  # 450k only
