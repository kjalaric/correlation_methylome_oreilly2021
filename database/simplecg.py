'''
Functions to deal with sqlite3 simple cg database (site + gene name only).
All very insecure and prone to injection, but that is fine.

kjalaric@gitlab
'''

import sqlite3

'''
DATABASE SPECIFICATION:
single table: cg
cg TEXT, gene TEXT
    cg is the cpg accession number (e.g. cg01253216)
    gene is the abbreviated gene name (e.g. PRKAA2)
'''

CSV_BUFFER_SIZE_LINES = 50
PROGRESS_UPDATE_RATE = 1000


class SimpleCgDatabaseHandler(object):
    def __init__(self, database):
        self._database_loc = database
        self.conn = sqlite3.connect(database)
        self.c = self.conn.cursor()

    def __del__(self):
        try:
            self.conn.close()
        except:
            pass

    def initialise_table(self, tablename="cg"):
        '''
        this should only be run for new databases
        uses python's sqlite3, dont think this is valid sql
        '''
        self.c.execute(f"CREATE TABLE IF NOT EXISTS {tablename} (cg TEXT PRIMARY KEY, gene TEXT)")
        self.conn.commit()

    def write(self, cg, gene, table="cg", commit=False):
        self.c.execute(f"REPLACE INTO {table} (cg, gene) VALUES (?,?)", (cg, gene))
        if commit: self.conn.commit()

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

    def dumpToCsv(self, table, csv_name, append=False):
        num_records = 0
        writeline = ""
        buffcount = 0
        if not append:
            with open(csv_name, "w") as fo:
                fo.write("cg,gene\n")
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        else:
            with open(csv_name, "a") as fo:
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        print(f"Finished processing {num_records} records.")

    def findCg(self, cgname):
        self.c.execute("SELECT * FROM cg WHERE cg=?", (cgname,))
        return self.c.fetchone()



class SimpleCgDatabaseStream(SimpleCgDatabaseHandler):
    def __init__(self, database):
        self._database_loc = database
        self.conn = None
        self.c = None

    def __enter__(self):
        self.conn = sqlite3.connect(self._database_loc)
        self.c = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.conn.close()


if __name__ == "__main__":
    from file_constants import SIMPLE_CG_DATABASE_LOCATION
    # initialise
    db = SimpleCgDatabaseHandler(SIMPLE_CG_DATABASE_LOCATION)
    db.initialise_table()
    db.commit()
    db.close()
