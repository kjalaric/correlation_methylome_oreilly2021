'''
Functions to deal with sqlite3 gene databases.
All very insecure and prone to injection, but that is fine.

kjalaric@gitlab
'''

import sqlite3

'''
DATABASE SPECIFICATION:
single table: genes
accession TEXT, gene TEXT, description TEXT, altnames TEXT, source TEXT, type TEXT, notes TEXT
    accession is the accession number (e.g. NM_006252)
    gene is the abbreviated gene name (e.g. PRKAA2)
    description is what this accession codes for (e.g. 5'-AMP-activated protein kinase catalytic subunit alpha-2)
    altnames are any alternative names, delimited by a semicolon
    source is where the gene was retrieved from
    type is a field which can specify whether it is protein-coding, intronic, etc.
    notes are any notes that might be useful
'''

CSV_BUFFER_SIZE_LINES = 50
PROGRESS_UPDATE_RATE = 1000


class GeneDatabaseHandler(object):
    def __init__(self, database):
        self._database_loc = database
        self.conn = sqlite3.connect(database)
        self.c = self.conn.cursor()

    def __del__(self):
        try:
            self.conn.close()
        except:
            pass

    def initialise_table(self, tablename="genes"):
        '''
        this should only be run for new databases
        uses python's sqlite3, dont think this is valid sql
        '''
        self.c.execute(f"CREATE TABLE IF NOT EXISTS {tablename} (accession TEXT, gene TEXT, description TEXT, altnames TEXT, source TEXT, type TEXT, notes TEXT)")
        self.conn.commit()

    def write(self, accession, gene, description, altnames="", source="", type="", notes="", table="genes", commit=False):
        self.c.execute(f"REPLACE INTO {table} (accession, gene, description, altnames, source, type, notes) VALUES (?,?,?,?,?,?,?)", (accession, gene, description, altnames, source, type ,notes))
        if commit: self.conn.commit()

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

    def dumpToCsv(self, table, csv_name, append=False):
        num_records = 0
        writeline = ""
        buffcount = 0
        if not append:
            with open(csv_name, "w") as fo:
                fo.write("accession,gene,description,altnames,source,type,notes\n")
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        else:
            with open(csv_name, "a") as fo:
                for row in self.c.execute(f'SELECT * FROM {table}'):
                    writeline += ",".join(row) + "\n"
                    buffcount += 1
                    if buffcount >= CSV_BUFFER_SIZE_LINES:
                        fo.write(writeline)
                        writeline = ""
                        buffcount = 0
                    num_records += 1
                    if not num_records % PROGRESS_UPDATE_RATE: print(f"Processed {num_records} records...")
                fo.write(writeline)
        print(f"Finished processing {num_records} records.")


class GeneDatabaseStream(GeneDatabaseHandler):
    def __init__(self, database):
        self._database_loc = database
        self.conn = None
        self.c = None

    def __enter__(self):
        self.conn = sqlite3.connect(self._database_loc)
        self.c = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.conn.close()

