"""
Caching utility. Uses pickle to save objects in the cache directory.
"""

import os
import pickle as pkl
import common.file_format


class CacheError(Exception):
    pass


class Cache(object):
    def __init__(self, cache_name, use_format=None, confirm_loc=True, make_cache_if_doesnt_exist=False):
        self.cache_name = cache_name
        self.location = os.path.join(common.file_format.cache_dir, cache_name)
        self.use_format = use_format

        if make_cache_if_doesnt_exist:
            try:
                os.makedirs(self.location)
            except FileExistsError:
                pass
        elif confirm_loc and not os.path.exists(self.location):
            raise Exception("Cache path does not exist: {}".format(self.location))

    def exists(self, filename):
        # checks to see if a specific file is in the cache
        if os.path.exists(os.path.join(self.location, filename)):
            return True
        else:
            return False

    def save(self, obj, *args):
        # saves obj with either the supplied filename (use_format=None) or formats the filename with the arguments
        if self.use_format is None:
            if len(args) > 1:
                raise CacheError("File format not supplied, can't use all args.")
            else:
                filename = args[0]
        else:
            filename = self.use_format.format(*args)

        with open(os.path.join(self.location, filename), "wb") as fo:
            pkl.dump(obj, fo)

    def saveNoExcept(self, obj, *args):
        # same as save() but will never throw an exception. good if we don't want to interrupt scripts_old due to caching
        try:
            self.save(obj, *args)
        except:
            pass

    def load(self, *args):
        # loads obj with either the supplied filename (use_format=None) or formats the filename with the arguments
        if self.use_format is None:
            if len(args) > 1:
                raise CacheError("File format not supplied, can't use all args.")
            else:
                filename = args[0]
        else:
            filename = self.use_format.format(*args)

        with open(os.path.join(self.location, filename), "rb") as fi:
            obj = pkl.load(fi)
        return obj

    def list(self, full_path=False):
        # returns a list of files in the cache, with or without the full path (as specified)
        if not full_path:
            return [x for x in os.listdir(self.location)]
        else:
            return [os.path.join(self.location, x) for x in os.listdir(self.location)]

    def clear(self):
        # deletes all files in the cache
        for x in self.list(full_path=True):
            os.remove(x)

    def purge(self):
        # deletes all files in the cache, then deletes the cache directory itself
        self.clear()
        os.rmdir(self.location)


if __name__ == "__main__":
    # unit test
    thing = ("this is", "a tuple")

    _cache = Cache("test", make_cache_if_doesnt_exist=True)
    _cache.clear()
    assert _cache.exists("testfile") == False
    _cache.save(thing, "testfile")
    assert _cache.exists("testfile")
    assert _cache.list(full_path=False) == ['testfile']
    assert _cache.list(full_path=True) == [os.path.join(common.file_format.cache_dir, "test", x) for x in os.listdir(os.path.join(common.file_format.cache_dir, "test"))]
    new_thing = _cache.load("testfile")
    for i, v in enumerate(new_thing):
        assert thing[i] == v
    _cache.purge()

    _cache = Cache("test2", use_format="{} - {}", make_cache_if_doesnt_exist=True)
    _cache.save(thing, thing[0], thing[1])
    assert os.listdir(_cache.location) == ['this is - a tuple']
    new_thing = _cache.load('this is', 'a tuple')
    for i, v in enumerate(new_thing):
        assert thing[i] == v
    _cache.purge()

    _cache = Cache("test3", make_cache_if_doesnt_exist=True)
    try:
        _cache.save(thing, thing[0], thing[1])
    except CacheError as e:
        error = e
    assert error is not None
    _cache.purge()

    error2 = None
    _cache = Cache("test4", make_cache_if_doesnt_exist=True)
    try:
        _cache.saveNoExcept(thing, thing[0], thing[1])
    except Exception as e:
        error2 = e
    assert error2 is None, error2
    _cache.purge()

    assert not os.path.exists(os.path.join(common.file_format.cache_dir, "test"))
    assert not os.path.exists(os.path.join(common.file_format.cache_dir, "test2"))
    assert not os.path.exists(os.path.join(common.file_format.cache_dir, "test3"))
    assert not os.path.exists(os.path.join(common.file_format.cache_dir, "test4"))


