import argparse
import pickle as pkl
import pandas as pd

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    args = parser.parse_args()

    with open(args.file, "rb") as fi:
        df = pkl.load(fi)

    print(df.shape)
    print(df.head())

