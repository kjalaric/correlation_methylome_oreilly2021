# converts pkl from protocol 4 to 3 (python 3.8+ -> 3.0 and later)
import pickle as pkl


def convertToProtocol3(file_location):
    with open(file_location, "rb") as fi:
        obj = pkl.load(fi)
    with open(file_location+"3", "wb") as fo:
        pkl.dump(obj, fo, protocol=3)


if __name__ == "__main__":
    import common.file_format
    import common.bio

    for norm_type in common.bio.norm_methods:
        convertToProtocol3(common.file_format.correlation_pkl.format("chds", "21", norm_type))

