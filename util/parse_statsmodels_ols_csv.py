"""
OLS regression results are output in a weird CSV format. This script reads and converts the CSV file into a dictionary.
"""


def readOLSRegressionResultsCsv(file_loc):
    results = dict()
    with open(file_loc, "r") as fi:
        fi.readline()  # header
        results["r2"] = fi.readline().split(",")[3]
        results["r2_adj"] = fi.readline().split(",")[3]
        results["F"] = fi.readline().split(",")[3]
        results["F_p"] = fi.readline().split(",")[4]
        results["log_likelihood"] = fi.readline().split(",")[3]
        line = fi.readline().split(",")
        results["n_observations"] = line[1]
        results["AIC"] = line[3]
        results["BIC"] = fi.readline().split(",")[3]

        # clean up dictionary before moving onto coefficients
        results = {k: float(v.strip()) for k, v in results.items()}
        for _ in range(3):
            fi.readline()
        line = [float(x.strip()) for x in fi.readline().split(",")[1:]]
        results["intercept"] = {"coef": line[0], "std_err": line[1], "t": line[2], "P_greater_abs_t": line[3], "conf_0.025": line[4], "conf_0.975": line[5]}
        line = [float(x.strip()) for x in fi.readline().split(",")[1:]]
        results["gradient"] = {"coef": line[0], "std_err": line[1], "t": line[2], "P_greater_abs_t": line[3], "conf_0.025": line[4], "conf_0.975": line[5]}
    return results
