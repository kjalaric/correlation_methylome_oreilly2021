#! /usr/bin/env python3
'''
API handler for UCSC genome browser.

https://genome.ucsc.edu/goldenPath/help/api.html
'''

import os
import json
import requests
from bs4 import BeautifulSoup

USCS_CGSEARCH_URL = "https://genome.ucsc.edu/cgi-bin/hgTracks?position={}&db={}"


def ucscSearchCg(cg, assembly="hg19"):
    r_url = USCS_CGSEARCH_URL.format(cg, assembly)

    r_params = {}
    r = requests.get(url = r_url, params = r_params)

    soup = BeautifulSoup(r.content, 'html.parser')
    c = soup.find('map', attrs={"name": "map_side_knownGene"})  # we are pulling this out from the gui directly

    gene = str(c).split("title=\"")[1].split("\"/>")[0]

    return gene


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("Search for the gene on which a cpg site is located.")
    parser.add_argument('cg', help="cg site to search for (e.g. cg23779220)")
    print(ucscSearchCg("cg09838562"))