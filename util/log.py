"""
Logging class
e.g. for profiling
"""

from datetime import datetime


class Logger(object):
    def __init__(self, file_loc):
        self._file_loc = file_loc

    def append(self, line, timestamp=True):
        with open(self._file_loc, "a") as fo:
            if timestamp:
                fo.write("{}| ".format(datetime.now().strftime("%H:%M:%S")))
            fo.write(line)
            fo.write("\n")


if __name__ == "__main__":
    # unit test
    log = Logger("K:/log_test.log")
    log.append("Test")
    log.append("Test")
    log.append("Test")
    log.append("Test",timestamp=False)
    log.append("Test",timestamp=False)
    log.append("Test",timestamp=False)
    log.append("Test")
    log.append("Test")
    log.append("Test")


