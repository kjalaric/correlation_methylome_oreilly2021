"""
Generates a table that can be copied into latex document.
"""


def generateTable(row_labels, column_labels, data, caption=None, label=None, significant_figures=3, integer_data=False,
                  label_first_column=False, integers_at=[], long_table=False, string_data=False, size=None):
    """
    :param row_labels:   list of row labels
    :param column_labels: list of column labels
    :param data: list of rows of table elements
    :param caption:
    :param label:
    :param significant_figures:
    :param label_first_column: is the first column labelled too?
    :param integers_at: a list of rown numbers that are integers, rather than floats (if not using integer_data)
    :param long_table: use a long table (multiple pages) instead of a standard table
    :return: list of lines that can be printed and copied into latex, written to a file, etc.
    """
    assert len(row_labels) == len(data)
    if not label_first_column:
        assert len(column_labels) == len(data[0])
    else:
        assert len(column_labels) == len(data[0]) + 1


    lines = list()

    if not label_first_column:
        n_columns = len(column_labels) + 1
    else:
        n_columns = len(column_labels)
    column_header = "| " + " c |"*n_columns
    if not long_table:
        if size:
            string_header = "\\begin{table}[H]\n\t" + size + "\n\t\\centering\n\t\\begin{tabular}{" + column_header + "}\n\t\t\\hline\n\t\t"
        else:
            string_header = "\\begin{table}[H]\n\t\\centering\n\t\\begin{tabular}{" + column_header + "}\n\t\t\\hline\n\t\t"

    else:
        string_header = "\\begin{longtable}{" + column_header + "}\n\t\t\\hline\n\t\t"
    lines.append(string_header)

    column_label_header = ""
    if not label_first_column:
        column_labels = [""] + column_labels
    for x in column_labels:
        column_label_header += "\\textbf{" + x + "} & "
    column_label_header = column_label_header[:-2] + "\\\\"
    lines.append(column_label_header)
    lines.append("\t\t\\hline")

    enum = 0
    for row_name, row_data in zip(row_labels, data):
        string_line = "\t\t\\textbf{" + row_name + "} & "
        for row_entry in row_data:
            if integer_data or enum in integers_at:
                try:
                    string_line += '%s' % int(row_entry) + " & "
                except TypeError:  # text entry, e.g. "-"
                    string_line += row_entry + " & "
            elif string_data:
                string_line += row_entry + " & "
            else:
                try:
                    string_line += '%s' % float(f'%.{significant_figures}g' % row_entry) + " & "
                except TypeError:  # text entry, e.g. "-"
                    string_line += row_entry + " & "
        string_line = string_line[:-2] + "\\\\"
        lines.append(string_line)
        lines.append("\t\t\\hline")
        enum += 1

    if not long_table:
        string_end_table = "\t\\end{tabular}"
        lines.append(string_end_table)
        if caption is not None:
            string_caption = "\t\\caption{" + caption + "}"
            lines.append(string_caption)

        if label is not None:
            string_label = "\t\\label{" + label + "}"
            lines.append(string_label)

        string_footer = "\\end{table}"
        lines.append(string_footer)
    else:

        if caption is not None:
            string_caption = "\t\\caption{" + caption + "}"
            lines.append(string_caption)

        if label is not None:
            string_label = "\t\\label{" + label + "}"
            lines.append(string_label)

        string_end_table = "\t\\end{longtable}"
        lines.append(string_end_table)




    # make sure there are no accidental comments
    for j in range(len(lines)):
        for i, c in enumerate(lines[j]):
            if c == "%":
                if i == 0:
                    continue
                else:
                    if lines[j][i-1] == "\\":
                        continue
                lines[j] = lines[j].replace("%", "\\%")

    return lines
