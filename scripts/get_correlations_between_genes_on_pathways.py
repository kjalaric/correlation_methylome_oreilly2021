import json
import common.file_format
import common.bio
import api.reactome as api


def run(args):
    cohort, chromosome, norm_type, meta = args
    if meta is not None:
        json_file_loc = common.file_format.gene_association_report_meta_json.format(cohort, chromosome, norm_type, meta)
    else:
        json_file_loc = common.file_format.gene_association_report_json.format(cohort, chromosome, norm_type)

    with open(json_file_loc, "r") as fi:
        results = json.load(fi)

    for common_pathway, gene_list in results["common_pathways"].items():
        pathway_response = api.searchPathwayDatabase(common_pathway, save_json=True)
        print(pathway_response)
        print(gene_list)


if __name__ == "__main__":
    cohort_ = "chds"
    chromosome_ = "22"
    meta_ = None
    run([cohort_, chromosome_, common.bio.appropriate_norm_type[chromosome_], meta_])
