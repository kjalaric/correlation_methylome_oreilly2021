import itertools
import json
import pandas as pd
import numpy as np
import scipy.stats as stats
from common.format_functions import formatCorrPkl
import common.file_format
import common.parse_epicarray_manifest as pam
import common.bio
import common.cpg_lookup
import data.info

if __name__ == "__main__":
    cohort = "chds"
    corr_type = "spearman"
    for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:
        norm_type = common.bio.appropriate_norm_type[chromosome]
        print("starting:", cohort, chromosome, norm_type, corr_type)

        json_out_loc = common.file_format.island_correlations_json.format(cohort, chromosome, norm_type, corr_type)
        df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))

        cpg_locations = dict()
        cpg_islands = dict()
        for cpg in common.cpg_lookup.getArraySites(data.info.getArrayType(cohort), chromosome):
            gene = pam.getGene(cpg)  # gene class object has CpG info etc.
            cpg_locations[cpg] = gene.mapinfo
            island_name = gene.island_name
            if not pd.isna(island_name):
                cpg_islands[cpg] = island_name

        islands = dict()
        for k, v in cpg_islands.items():
            try:
                islands[v].append(k)
            except KeyError:
                islands[v] = [k]
        islands = {k: v for k, v in islands.items() if len(v) > 1}

        all_island_correlations = {k: list() for k in islands.keys()}
        adjacent_correlations = {k: list() for k in islands.keys()}

        for island, cpgs in islands.items():
            # overall correlations
            for pair in itertools.combinations(cpgs, 2):
                all_island_correlations[island].append(df.loc[pair[0]].loc[pair[1]])

            # adjacent correlations only
            cpgs_by_mapinfo = sorted([(cpg, cpg_locations[cpg]) for cpg in cpgs], key=lambda x: x[1], reverse=True)
            for i in range(len(cpgs_by_mapinfo)-1):
                adjacent_correlations[island].append(df.loc[cpgs_by_mapinfo[i][0]][cpgs_by_mapinfo[i+1][0]])

        json_write_dict = dict()
        json_write_dict["islands"] = {k: {
            "length": len(v),
        } for k, v in islands.items()}

        # remove diagonal and triangle from the array
        df = df.to_numpy()
        df = df[np.triu_indices(n=df.shape[0], k=1)]
        positive_overall = df[df >= 0]
        negative_overall = df[df < 0]

        # the usual unpacking here doesn't work, so we instead do it in this terrible way
        # all island correlations
        positives_all_island = list()
        for z in [[x for x in y if x >= 0] for y in all_island_correlations.values()]:
            positives_all_island.extend(z)
        negatives_all_island = list()
        for z in [[x for x in y if x < 0] for y in all_island_correlations.values()]:
            negatives_all_island.extend(z)

        # adjacent correlations
        positives_adjacent = list()
        for z in [[x for x in y if x >= 0] for y in adjacent_correlations.values()]:
            positives_adjacent.extend(z)
        negatives_adjacent = list()
        for z in [[x for x in y if x < 0] for y in adjacent_correlations.values()]:
            negatives_adjacent.extend(z)

        json_write_dict["overall"] = {
            "n_positive": len(positive_overall),
            "n_negative": len(negative_overall),
            "average_positive": np.mean(positive_overall),
            "average_negative": np.mean(negative_overall),
        }

        json_write_dict["all_island"] = {
            "n_positive": len(positives_all_island),
            "n_negative": len(negatives_all_island),
            "average_positive": np.mean(positives_all_island),
            "average_negative": np.mean(negatives_all_island),
        }
        
        json_write_dict["adjacent"] = {
            "n_positive": len(positives_adjacent),
            "n_negative": len(negatives_adjacent),
            "average_positive": np.mean(positives_adjacent),
            "average_negative": np.mean(negatives_adjacent),
        }

        print("running positive ANOVA for all-island correlations...")
        positive_anova_results_all_island = stats.f_oneway(positives_all_island, positive_overall)

        print("running negative ANOVA for all-island correlations...")
        negative_anova_results_all_island = stats.f_oneway(negatives_all_island, negative_overall)

        print("running positive ANOVA for adjacent correlations...")
        positive_anova_results_adjacent = stats.f_oneway(positives_adjacent, positive_overall)

        print("running negative ANOVA for adjacent correlations...")
        negative_anova_results_adjacent = stats.f_oneway(negatives_adjacent, negative_overall)

        json_write_dict["anova_positive_all_island"] = {"f": positive_anova_results_all_island.statistic, "p": positive_anova_results_all_island.pvalue}
        json_write_dict["anova_negative_all_island"] = {"f": negative_anova_results_all_island.statistic, "p": negative_anova_results_all_island.pvalue}
        json_write_dict["anova_positive_adjacent"] = {"f": positive_anova_results_adjacent.statistic, "p": positive_anova_results_adjacent.pvalue}
        json_write_dict["anova_negative_adjacent"] = {"f": negative_anova_results_adjacent.statistic, "p": negative_anova_results_adjacent.pvalue}

        with open(json_out_loc, "w") as fo:
            json.dump(json_write_dict, fo)







