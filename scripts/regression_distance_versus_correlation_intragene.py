import os
import random
import json
import pandas as pd
import numpy as np
# from sklearn.linear_model import LinearRegression
import statsmodels.api as sm
import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.bio
import common.cpg_lookup
import common.file_format
from common.format_functions import formatCorrPkl
import data.info
import util.cache
import visualisation.chromosome_size_for_plots

# processed_dir_base = "K:/processed/"
# file_in_format = "chromosome_{}_betas_{}.csv"  # chromosome, norm method
# file_out_format = "{}{}_chr{}_{}.txt"  # strength, direction, chromosome, norm method
# correlation_pickle_format = "beta_chromosome_{}_{}_correlation.pkl"  # chromosome, norm method


def run(args):
    cohort, chromosome, norm_type, corr_type = args
    csv_out_positive = common.file_format.loci_distance_csv_intragene.format(cohort, chromosome, norm_type, corr_type, "positive")
    csv_out_negative = common.file_format.loci_distance_csv_intragene.format(cohort, chromosome, norm_type, corr_type, "negative")
    # json_out = common.file_format.loci_distance_json_intragene.format(cohort, chromosome, norm_type, corr_type)
    plot_out = common.file_format.loci_distance_plot_intragene.format(cohort, chromosome, norm_type, corr_type)

    print("PROCESSING: ", cohort, chromosome, norm_type, corr_type)
    positive_pairs = list()
    negative_pairs = list()

    intragene_df = pd.read_csv(common.file_format.same_gene_correlation_report_all_genes_for_chromosome.format(cohort, chromosome, norm_type, corr_type))
    # remove duplicate entries
    gencode_pairs = {x.cpg1mapinfo: x.cpg2mapinfo for _, x in intragene_df[intragene_df["annotation_source"] == "gencode"].iterrows()}
    for i, row in intragene_df[intragene_df["annotation_source"] == "illumina"].iterrows():
        if row.cpg1mapinfo in gencode_pairs.keys():
            if row.cpg2mapinfo == gencode_pairs[row.cpg1mapinfo]:
                intragene_df.drop(intragene_df[(intragene_df.cpg1 == row.cpg1) & (intragene_df.cpg2 == row.cpg2) & (intragene_df.annotation_source == "gencode")].index, inplace=True)

    print("Generating distances...")
    for _, row in intragene_df.iterrows():
        if row["correlation"] >= 0.0:
            positive_pairs.append((row["correlation"], row["distance"]))
        else:
            negative_pairs.append((row["correlation"], row["distance"]))

    '''
    # chop off any more than 3 stdev away
    positive_stdev = np.std([x[1] for x in positive_pairs])
    negative_stdev = np.std([x[1] for x in negative_pairs])

    prev_pos_len = len(positive_pairs)
    prev_neg_len = len(negative_pairs)

    positive_pairs = [x for x in positive_pairs if x[1] <= np.mean([x[1] for x in positive_pairs]) + 3.0*positive_stdev]
    negative_pairs = [x for x in negative_pairs if x[1] <= np.mean([x[1] for x in negative_pairs]) + 3.0*negative_stdev]

    positive_count = len(positive_pairs)
    negative_count = len(negative_pairs)
    positive_rejected = prev_pos_len - positive_count
    negative_rejected = prev_neg_len - negative_count

    print(f"Removed {positive_rejected} positive outliers and {negative_rejected} negative outliers.")
    '''

    print("Regressing positives...")
    lm = sm.OLS([x[0] for x in positive_pairs], sm.add_constant([x[1] for x in positive_pairs]))
    positive_results = lm.fit()
    
    print("Regressing negatives...")
    lm = sm.OLS([x[0] for x in negative_pairs], sm.add_constant([x[1] for x in negative_pairs]))
    negative_results = lm.fit()

    """ 
    results_d = {
        "pos_coef": positive_results.params[1],
        "pos_int": positive_results.params[0],
        "pos_r2": positive_results.rsquared,
        "pos_r2adj": positive_results.rsquared_adj,
        "pos_f": positive_results.fvalue,
        "pos_fp": positive_results.f_pvalue,
        "neg_coef": negative_results.params[1],
        "neg_int": negative_results.params[0],
        "neg_r2": negative_results.rsquared,
        "neg_r2adj": negative_results.rsquared_adj,
        "neg_f": negative_results.fvalue,
        "neg_fp": negative_results.f_pvalue,
        "pos_count": positive_count,
        "pos_rejected": positive_rejected,
        "neg_count": negative_count,
        "neg_rejected": negative_rejected
    }

    with open(json_out, "w") as fo:
        json.dump(results_d, fo)
    print("Written to", json_out)
    """

    with open(csv_out_positive, "w") as fo:
        fo.write(positive_results.summary().as_csv())
    print("Written to", csv_out_positive)
    with open(csv_out_negative, "w") as fo:
        fo.write(negative_results.summary().as_csv())
    print("Written to", csv_out_negative)


    positive_maximum_distance = max([x[1] for x in positive_pairs])
    negative_maximum_distance = max([x[1] for x in negative_pairs])
    print("Plotting...")

    fig, axs = plt.subplots(figsize=(19.2, 10.8))
    axs.plot([x[1] for x in positive_pairs], [x[0] for x in positive_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.9, color="green")
    axs.plot([x[1] for x in negative_pairs], [x[0] for x in negative_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.9, color="red")

    axs.plot([0, positive_maximum_distance], [positive_results.params[0], positive_maximum_distance * positive_results.params[1] + positive_results.params[0]], c="darkgreen")
    axs.plot([0, negative_maximum_distance], [negative_results.params[0], negative_maximum_distance * negative_results.params[1] + negative_results.params[0]], c="darkred")

    axs.set_title(f"{cohort}: distance vs. {corr_type} correlation for chr{chromosome.upper()} (norm: {norm_type})", fontsize=20)
    axs.set_xlabel("Distance (bp)", fontsize=20)
    axs.set_ylabel("Spearman Rank Correlation", fontsize=20)
    axs.tick_params(axis='both', which='major', labelsize=18)
    axs.tick_params(axis='both', which='minor', labelsize=18)
    axs.set_xlim(0, max([positive_maximum_distance, negative_maximum_distance]))
    axs.set_ylim(-1.0, 1.0)
    axs.grid()
    fig.tight_layout()
    plt.savefig(plot_out, dpi=300)
    print("Plot output to", plot_out)



if __name__ == "__main__":
    # chromosome = "21"
    # #norm_type = "swan"
    cohort_ = "chds"
    corr_type_ = "spearman"
    chromosomes = ["y"]

    # run(cohort_, "y", "swan")  # handled later
    # run(cohort_, "x", "swan")

    import multiprocessing
    args = [[cohort_, chromosome_, common.bio.appropriate_norm_type[chromosome_], corr_type_] for chromosome_ in common.bio.epic_probes_smallest_to_largest]

    with multiprocessing.pool.Pool(processes=5) as pool:
        pool.map(run, args)