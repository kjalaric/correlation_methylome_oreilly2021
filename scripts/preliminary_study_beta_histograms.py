import os
import numpy as np
import pandas as pd
import common.file_format
import matplotlib.pyplot as plt


def formatCpgPkl(cohort, chromosome, normtype):
    return common.file_format.description_for_beta_cpg_pkl.format(cohort, chromosome, normtype)

def formatSamplesPkl(cohort, chromosome, normtype):
    return common.file_format.description_for_beta_sample_pkl.format(cohort, chromosome, normtype)


def generatePkls(cohort, chromosome, normtype, file_override_in=None, file_override_out_cpgs=None, file_override_out_samples=None):
    if not file_override_in:
        file_in = common.file_format.betacsv_per_chromosome.format(cohort, chromosome, normtype)
        file_out_cpgs = formatCpgPkl(cohort, chromosome, normtype)
        file_out_samples = formatSamplesPkl(cohort, chromosome, normtype)
    else:
        file_in = file_override_in
        file_out_cpgs = file_override_out_cpgs
        file_out_samples = file_override_out_samples

    df = pd.read_csv(file_in, index_col=0)
    per_sample = df.describe()
    per_cpg = df.T.describe()

    per_sample.to_pickle(file_out_samples)
    per_cpg.to_pickle(file_out_cpgs)

    return file_out_cpgs, file_out_samples  # filenames


def run(cohort, chromosome, normtype, file_override_beta=None, file_override_cpgs=None, file_override_samples=None, regenerate=False):
    if not file_override_beta or file_override_cpgs:
        file_cpgs = formatCpgPkl(cohort, chromosome, normtype)
        file_samples = formatSamplesPkl(cohort, chromosome, normtype)
        if regenerate or not (os.path.exists(file_cpgs) and os.path.exists(file_samples)):
            generatePkls(cohort, chromosome, normtype)

    else:
        if file_override_beta:
            if not (file_override_cpgs and file_override_samples):
                raise Exception("Override needs to define cpg and sample pkl file locations")
            else:
                if regenerate or not (os.path.exists(file_override_cpgs) and os.path.exists(file_override_samples)):
                    file_cpgs, file_samples = generatePkls(cohort, chromosome, normtype, file_override_beta, file_override_cpgs, file_override_samples)
                else:
                    file_cpgs = file_override_cpgs
                    # file_samples = file_override_samples

    # df_sample = pd.read_pickle(file_samples)
    df_cpg = pd.read_pickle(file_cpgs)

    plt.hist(df_cpg.loc["mean"], bins=100, weights=np.ones(len(df_cpg.loc["mean"]))/len(df_cpg.loc["mean"]))
    plt.title("Distribution of mean beta values\n{} - Chromosome {}, {} normalisation".format(cohort, chromosome, normtype), wrap=True)
    plt.xlabel("Mean beta value")
    plt.ylabel("Frequency")
    plt.grid()
    plt.show()

    plt.hist(df_cpg.loc["std"], bins=100, weights=np.ones(len(df_cpg.loc["mean"]))/len(df_cpg.loc["mean"]))
    plt.title("Distribution of standard deviations of beta values\n{} - Chromosome {}, {} normalisation".format(cohort, chromosome, normtype), wrap=True)
    plt.xlabel("Standard deviation of betas")
    plt.ylabel("Frequency")
    plt.grid()
    plt.show()



    # print(df_cpg.kurtosis)

    '''
    for i, x in df_cpg.iterrows():
        if i in ["mean", "std"]:
            x.hist()
            plt.title(i)
            plt.show()
    '''
    '''
    for i, x in df_sample.iterrows():
        if i in ["mean", "std"]:
            x.hist()
            plt.title(i)
            plt.show()
    '''
    '''
    for i, x in df_sample.kurtosis().iteritems():
        print(x)
        if i in ["mean", "std"]:
            x.hist()
            plt.title(i)
            plt.show()
    '''
    


if __name__ == "__main__":
    # run("chds_subsampled450k", "21", "raw")
    # run("gse43414", "21", "raw")
    run("chds", "21", "noob")
    run("chds", "21", "raw")