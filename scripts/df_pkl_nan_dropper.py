import pickle as pkl
import pandas as pd
import common.file_format


def loadPklAndDropNans(file_loc):
    with open(file_loc, "rb") as fi:
        df = pkl.load(fi)
    shape = df.shape[0]  # drop rows with at least 2 NaN
    nulls = 0
    not_nulls = 0
    null_cpgs = list()

    for i, x in df.iterrows():
        if x.isna().sum() == shape:
            nulls += 1
            null_cpgs.append(i)
        else:
            not_nulls += 1

    df.drop(labels=null_cpgs, axis=0, inplace=True)
    df.drop(labels=null_cpgs, axis=1, inplace=True)

    return df, nulls


if __name__ == "__main__":
    cohort = "chds"
    norm_type = "noob"
    for chromosome in ["21", "1", "x", "y"]:
        for corrtype in ["pearson", "spearman", "kendall"]:
            old_file_loc = common.file_format.correlation_pkl_corrtype.format(cohort, corrtype, chromosome, norm_type)  # cohort, correlation_oldformat type, chromosome, normtype
            new_file_loc = common.file_format.correlation_pkl_corrtype_no_nan.format(cohort, corrtype, chromosome, norm_type)  # cohort, correlation_oldformat type, chromosome, normtype

            loadPklAndDropNans(old_file_loc).to_pickle(new_file_loc)

            '''  replaced with function
            with open(old_file_loc, "rb") as fi:
                df = pkl.load(fi)
            print("Original shape:", df.shape)

            shape = df.shape[0]  # drop rows with at least 2 NaN

            print(len(df) - df.count())

            nulls = 0
            not_nulls = 0
            null_cpgs = list()

            for i, x in df.iterrows():
                if x.isna().sum() == shape:
                    nulls += 1
                    null_cpgs.append(i)
                else:
                    not_nulls += 1

            print(nulls, "/", nulls + not_nulls)

            df.drop(labels=null_cpgs, axis=0, inplace=True)
            df.drop(labels=null_cpgs, axis=1, inplace=True)

            df.to_pickle(new_file_loc)
            '''
            
