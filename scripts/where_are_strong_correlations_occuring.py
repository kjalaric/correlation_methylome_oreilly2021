from datetime import datetime
import matplotlib.pyplot as plt
from common.gene_class import *
from common.defines import *
import common.file_format
from chromosomal_distance.distance_between_sites import *
import common.cpg_lookup
import data.info
import visualisation.chromosome_size_for_plots
import common.bio


def run(cohort, chromosome, norm_types, corr_type="spearman", use_10pc=False, plot_background=False):
    array_type = data.info.getArrayType(cohort)

    if array_type == "EPIC":
        import common.parse_epicarray_manifest as pam
    elif array_type == "450k":
        import common.parse_450k_manifest as pam

    cpg_dict = common.cpg_lookup.getLookupDict(array_type, chromosome)
    cpg_dict = {v: pam.getGene(v) for k, v in cpg_dict.items()}

    positive_correlations = dict()
    negative_correlations = dict()
    for norm_type in norm_types:
        if not use_10pc:
            positive_correlation_file_location = common.file_format.correlation.format(cohort, corr_type, "strong", "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation.format(cohort, corr_type, "strong", "negative", chromosome, norm_type)
            if not plot_background:
                plot_out_file = common.file_format.correlation_loci_plot_multi.format(cohort, chromosome)  # doesn't need to be done every time, left in for simplicity
            else:
                plot_out_file = common.file_format.correlation_loci_plot_multi.format(cohort, chromosome + "_bg")  # doesn't need to be done every time, left in for simplicity

        else:
            positive_correlation_file_location = common.file_format.correlation_strongest_10percent.format(cohort, corr_type, 10, "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation_strongest_10percent.format(cohort, corr_type, 10, "negative", chromosome, norm_type)
            if not use_10pc:
                plot_out_file = common.file_format.correlation_loci_plot_10pc_multi.format(cohort, chromosome)  # doesn't need to be done every time, left in for simplicity
            else:
                plot_out_file = common.file_format.correlation_loci_plot_10pc_multi.format(cohort, chromosome + "_bg")  # doesn't need to be done every time, left in for simplicity


        with open(positive_correlation_file_location, "r") as fi:
            for fline in fi.readlines():
                result = fline.strip().split(",")
                try:
                    pair = tuple(sorted([cpg_dict[result[0]], cpg_dict[result[1]]], key=lambda z: z.cpg))
                except KeyError:  # rare but sometimes happens with the additional (ch.**.***) signals
                    continue
                try:
                    positive_correlations[pair] += 1
                except KeyError:
                    positive_correlations[pair] = 1

        with open(negative_correlation_file_location, "r") as fi:
            for fline in fi.readlines():
                result = fline.strip().split(",")
                try:
                    pair = tuple(sorted([cpg_dict[result[0]], cpg_dict[result[1]]], key=lambda z: z.cpg))
                except KeyError:  # rare but sometimes happens with the additional (ch.**.***) signals
                    continue
                try:
                    negative_correlations[pair] += 1
                except KeyError:
                    negative_correlations[pair] = 1

    in_both = list()
    for k in positive_correlations.keys():
        if k in negative_correlations.keys():
            print("WARNING: positive correlation found in negative correlation list")
            print(k)
            in_both.append(k)


    positive_locus_pairs = list()
    for k, v in positive_correlations.items():
        if k[0].mapinfo is not None and k[1].mapinfo is not None:
            positions = sorted([k[0].mapinfo, k[1].mapinfo])
            positive_locus_pairs.append((*positions, v))

    negative_locus_pairs = list()
    for k, v in negative_correlations.items():
        if k[0].mapinfo is not None and k[1].mapinfo is not None:
            positions = sorted([k[0].mapinfo, k[1].mapinfo])
            negative_locus_pairs.append((*positions, v))

    plot_limit = visualisation.chromosome_size_for_plots.chromosome_sizes[chromosome]
    for x in positive_locus_pairs + negative_locus_pairs:
        assert x[0] < plot_limit, f"{x[0]} beyond plot limit of {plot_limit}"
        assert x[1] < plot_limit, f"{x[1]} beyond plot limit of {plot_limit}"

    fig, axs = plt.subplots(figsize=(19.2, 10.8))

    # alpha is based on how many times it comes up, which is a number between 1 and 6 (inclusive)
    # correlations that appear in all have a different colour
    num_classes = len(norm_types)
    alpha_division = 1.0/(len(norm_types) - 1)
    for i in range(1, num_classes):
        axs.plot([x[0] for x in positive_locus_pairs if x[2] == i], [x[1] for x in positive_locus_pairs if x[2] == i], linestyle='None',
                 markersize=1.0, marker=".", alpha=i*alpha_division, color="green")
        axs.plot([x[0] for x in negative_locus_pairs if x[2] == i], [x[1] for x in negative_locus_pairs if x[2] == i], linestyle='None',
                 markersize=1.0, marker=".", alpha=i*alpha_division, color="red")
    axs.plot([x[0] for x in positive_locus_pairs if x[2] == num_classes], [x[1] for x in positive_locus_pairs if x[2] == num_classes], linestyle='None',
             markersize=1.0, marker=".", alpha=1.0, color="darkgreen")
    axs.plot([x[0] for x in negative_locus_pairs if x[2] == num_classes], [x[1] for x in negative_locus_pairs if x[2] == num_classes], linestyle='None',
             markersize=1.0, marker=".", alpha=1.0, color="darkred")
    axs.plot([x[0] for x in in_both], [x[1] for x in in_both], linestyle='None',
             markersize=1.0, marker=".", alpha=1.0, color="black")

    axs.set_title("Strong correlations in cohort [{}]: chr{} | all norm types".format(cohort, chromosome), fontsize=20)
    axs.set_xlabel("CpG 1 position (bp)", fontsize=20)
    axs.set_ylabel("CpG 2 position (bp)", fontsize=20)
    axs.tick_params(axis='both', which='major', labelsize=18)
    axs.tick_params(axis='both', which='minor', labelsize=18)
    axs.set_aspect("equal", "box")
    axs.set_xlim(0, plot_limit)
    axs.set_ylim(0, plot_limit)
    axs.grid()
    fig.tight_layout()
    plt.savefig(plot_out_file, dpi=300)
    plt.close()


if __name__ == "__main__":
    cohort_ = "chds"
    for chromosome_ in ["21"]:# common.bio.epic_probes_smallest_to_largest[:13]:
        try:
            print("starting", chromosome_)
            start_time = datetime.now()
            run(cohort_, chromosome_, common.bio.norm_types, use_10pc=True, plot_background=True)
            end_time = datetime.now()
            print("finished - time: ", end_time - start_time, "seconds\n")
        except FileNotFoundError as e:
            print("Couldn't find", chromosome_, "|", e)


