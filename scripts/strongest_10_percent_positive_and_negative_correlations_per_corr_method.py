import math
import pandas as pd
import common.file_format
import common.bio


def run(cohort, chromosome, norm_type):
    print("Starting ", cohort, chromosome, norm_type)
    correlation_dfs = [pd.read_pickle(common.file_format.correlation_pkl_corrtype.format(cohort, x, chromosome, norm_type)) for x in common.bio.correlation_types]
    strong_positives_per_df = {x: set() for x in common.bio.correlation_types}
    strong_negatives_per_df = {x: set() for x in common.bio.correlation_types}
    strong_positives_all = dict()
    strong_negatives_all = dict()

    def write(*args):
        with open(common.file_format.correlation_overlap_corr_method.format(cohort, chromosome, norm_type), "a+") as fo:
            fo.write(" ".join([str(x) for x in args]) + "\n")

    write("Starting ", cohort, chromosome, norm_type)

    for i, df in enumerate(correlation_dfs):
        sorted_df = df.unstack().sort_values(ascending=False)[df.shape[1]:]  # quicksort by default, mergesort is more stable. also remove the diagonal
        n_unique_pairs = df.shape[1]*(df.shape[1]-1)/2
        looking_at_this_many = int(math.ceil(0.1*n_unique_pairs))  # expect twice as many due to duplicates
        number_strong_correlations = 2*looking_at_this_many  # expect twice as many due to duplicates

        strong_positives = sorted_df[:number_strong_correlations]  # above will still have two of everything because diagonally-symmetrical
        for ii, x in strong_positives.iteritems():
            strong_positives_per_df[common.bio.correlation_types[i]].add(tuple(sorted(ii)))

        strong_negatives = sorted_df[-number_strong_correlations:].iloc[::-1]  # want strongest first, so have to reverse the order of this one
        for ii, x in strong_negatives.iteritems():
            strong_negatives_per_df[common.bio.correlation_types[i]].add(tuple(sorted(ii)))

        write("\nResults for ", common.bio.correlation_types[i])
        write("Dataframe size:", sorted_df.size, "(", len(strong_positives_per_df[common.bio.correlation_types[i]]), "looked at for each of [positive, negative])")
        write("Positive percentage:", 100.0*((sorted_df.size - sorted_df.lt(0).sum())/sorted_df.size))

    for nt, cpg_set in strong_positives_per_df.items():
        for x in cpg_set:
            try:
                strong_positives_all[x] += 1
            except KeyError:
                strong_positives_all[x] = 1
                
    for nt, cpg_set in strong_negatives_per_df.items():
        for x in cpg_set:
            try:
                strong_negatives_all[x] += 1
            except KeyError:
                strong_negatives_all[x] = 1


    consistent_positive = {x for x, v in strong_positives_all.items() if v >= 3}
    write("\nFound", len(consistent_positive), "consistently-correlating positive sites")

    middling_positive = {x for x, v in strong_positives_all.items() if v == 2}
    print(len(middling_positive))

    unique_positive = {x for x, v in strong_positives_all.items() if v == 1}
    write("\nFound", len(unique_positive), "unique positive sites")

    unique_positive_count = {x: 0 for x in common.bio.correlation_types}

    for x in unique_positive:
        for k, corr_set in strong_positives_per_df.items():
            if x in corr_set:
                unique_positive_count[k] += 1
                break

    write("Unique positives:")
    for k, v in unique_positive_count.items():
        write(k, v)
        
    consistent_negative = {x for x, v in strong_negatives_all.items() if v >= 3}
    write("\nFound", len(consistent_negative), "consistently-correlating negative sites")

    middling_negative = {x for x, v in strong_negatives_all.items() if v == 2}
    print(len(middling_negative))

    unique_negative = {x for x, v in strong_negatives_all.items() if v == 1}
    write("\nFound", len(unique_negative), "unique negative sites")

    unique_negative_count = {x: 0 for x in common.bio.correlation_types}

    for x in unique_negative:
        for k, corr_set in strong_negatives_per_df.items():
            if x in corr_set:
                unique_negative_count[k] += 1
                break

    write("Unique negatives:")
    for k, v in unique_negative_count.items():
        write(k, v)


if __name__ == "__main__":
    cohort = "chds"
    norm_type = "noob"
    for chromosome in ["y", "21", "x"]:
        run(cohort, chromosome, norm_type)
