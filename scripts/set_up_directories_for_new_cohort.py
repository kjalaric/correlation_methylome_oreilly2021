import os
import common.file_format


def createNewCohortDirectories(cohort_name):
    processed_dir = os.path.join(common.file_format.processed_dir, cohort_name)
    try:
        os.mkdir(processed_dir)
    except FileExistsError:
        pass

    for subdir in ["nx", "pkl", "subsamples"]:
        try:
            os.mkdir(os.path.join(processed_dir, subdir))
        except FileExistsError:
            pass

    reports_dir = os.path.join(common.file_format.reports_dir, cohort_name)

    try:
        os.mkdir(reports_dir)
    except FileExistsError:
        pass


if __name__ == "__main__":
    createNewCohortDirectories("metacohort_1")

