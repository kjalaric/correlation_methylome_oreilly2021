import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import common.file_format
from common.format_functions import formatCorrPkl
import common.read_files
import common.cpg_lookup
import data.info
import gencode.feature_lookup
import common.bio
import common.parse_epicarray_manifest as pam


def plot_intragene_correlation_distribution(cohort, chromosome, norm_type, corr_type="spearman"):
    intragene_df = pd.read_csv(common.file_format.same_gene_correlation_report_all_genes_for_chromosome.format(cohort, chromosome, norm_type, corr_type))

    # remove gencode
    intragene_df = intragene_df[intragene_df["annotation_source"] == "illumina"]
    print(intragene_df.shape)

    indicies_to_drop = list()
    for i, row in intragene_df.iterrows():
        gene1 = pam.getGene(row.cpg1)
        if pd.notna(gene1.island_name):
            indicies_to_drop.append(i)
        gene2 = pam.getGene(row.cpg2)
        if pd.notna(gene2.island_name):
            indicies_to_drop.append(i)
    intragene_df.drop(index=indicies_to_drop, inplace=True)

    overall_df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type)).to_numpy().flatten()



    fig, axs = plt.subplots(figsize=(19.2, 10.8))
    axs.hist(intragene_df.correlation, bins=100, alpha=0.5, weights=np.ones(len(intragene_df.correlation))/len(intragene_df.correlation), color="blue", label="Intragene correlations")
    axs.hist(overall_df, bins=100, alpha=0.5, weights=np.ones(len(overall_df))/len(overall_df), color="orange", label="Chromosome-wide correlations")
    axs.set_title(f"Estimated probability distribution functions for {corr_type} correlation coefficients:\nwithin-gene versus entire chromosome "
                  f"for chr{chromosome.upper()}, {norm_type} normalisation ({cohort} cohort)\n"
                  f"n_all: {len(overall_df)} | n_intragenic: {len(intragene_df.correlation)} ({round(100.0*len(intragene_df.correlation)/len(overall_df), 2)}% of chr{chromosome.upper()} correlations)", size=24)
    axs.set_xlabel("Correlation coefficient", size=24)
    axs.set_ylabel("Relative frequency of coefficients", size=24)
    axs.tick_params(labelsize=24)
    axs.grid(which="both")
    plt.savefig(common.file_format.same_gene_correlation_histogram_no_island.format(cohort, chromosome, norm_type, corr_type), dpi=300)



if __name__ == "__main__":
    cohort_ = "chds"
    chromosomes = common.bio.epic_probes_smallest_to_largest[:13]  # also change norm type when running autosomes

    for chromosome_ in chromosomes:
        plot_intragene_correlation_distribution(cohort_, chromosome_, common.bio.appropriate_norm_type[chromosome_])