import json
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines
import common.bio
from common.format_functions import formatBetaCsv, formatBetaCsvPerChromosome, formatBetaCsvAutosomal
import common.file_format
import common.visualisation

if __name__ == "__main__":
    cohort = "chds"  # need to regenerate for non-CHDS cohorts
    chromosomes = common.bio.chromosomes
    norm_types = common.bio.norm_methods

    # generate beta stats
    if (0):
        results = dict()

        # overall genome, for comparison
        results["autosome"] = dict()
        for norm_type in norm_types:
            results["autosome"][norm_type] = dict()
            df = pd.read_csv(formatBetaCsvAutosomal(cohort, norm_type), index_col=0)
            df = df.T

            results["autosome"][norm_type]["mean"] = df.mean().mean()
            results["autosome"][norm_type]["std"] = df.std().mean()

        # individual chromosomes
        for chromosome in chromosomes:

            results[chromosome] = dict()
            for norm_type in norm_types:
                results[chromosome][norm_type] = dict()
                # df = pd.read_pickle(?)
                df = pd.read_csv(formatBetaCsvPerChromosome(cohort, chromosome, norm_type), index_col=0)
                df = df.T

                results[chromosome][norm_type]["mean"] = df.mean().mean()
                results[chromosome][norm_type]["std"] = df.std().mean()

            # for plotting
            # individual
            if (0):
                for k, v in results[chromosome].items():
                    # points[k] = (v["mean"], v["std"])
                    plt.scatter(v["mean"], v["std"])
                    plt.annotate(k, (v["mean"], v["std"]))
                plt.xlabel("mean")
                plt.ylabel("stdev")
                plt.show()

            # add chromosomes to plot
            if (1):
                for k, v in results[chromosome].items():
                    # points[k] = (v["mean"], v["std"])
                    plt.scatter(v["mean"], v["std"], s=2)
                    plt.annotate(chromosome+k[0], (v["mean"], v["std"]))  # chromsome and first letter of norm type


        averages = {x: [0, 0] for x in norm_types}
        for _, v in results.items():
            for kk, vv in v.items():
                averages[kk][0] += vv["mean"]
                averages[kk][1] += vv["std"]
        for k in averages.keys():
            averages[k][0] = averages[k][0]/len(chromosomes)
            averages[k][1] = averages[k][1]/len(chromosomes)

        if (1):
            for k, v in averages.items():
                # points[k] = (v["mean"], v["std"])
                plt.scatter(v[0], v[1])
                plt.annotate(k, (v[0], v[1]))
            plt.xlabel("mean")
            plt.ylabel("stdev")
            plt.grid()
            plt.show()

        with open(common.file_format.stats_beta_json.format(cohort), "w") as fo:
            json.dump(results, fo)


        '''
        # full genome. differences between norm types
        for norm_type in common.bio.norm_methods:
            
            with open(formatBetaCsv())
        '''

    if (1):  # plotting
        with open(common.file_format.stats_beta_json.format(cohort), "r") as fi:
            results = json.load(fi)

        '''
        chromosome_averages = dict()
        for chromosome, v in results.items():
            chromosome_averages[chromosome] = {'mean': 0, 'std': 0}
            for norm_type, vv in v.items():
                chromosome_averages[chromosome]['mean'] += vv["mean"]
                chromosome_averages[chromosome]['std'] += vv["std"]
            chromosome_averages[chromosome] = {
                'mean': chromosome_averages[chromosome]['mean']/len(v),
                'std': chromosome_averages[chromosome]['std']/len(v)}
        '''

        # differences of each chromosome from autosomal average
        # beta_stat_offsets_overall_{} -> cohort
        if 1:
            # plot differences from the chromosome averages
            # fig, ax = plt.subplots()
            plot_points = list()
            for chromosome, v in results.items():
                if chromosome in ["x", "y"]:
                    continue
                elif chromosome == "autosome":
                    '''
                    for norm_type, vv in v.items():
                        plt.scatter(vv["mean"], vv["std"], s=4, marker="x", c=common.visualisation.colours[norm_type], label=norm_type)
                    '''
                    pass  # not needed for offsets, only for absolutes
                else:
                    for norm_type, vv in v.items():
                        plt.scatter(vv["mean"] - results["autosome"][norm_type]["mean"], vv["std"] - results["autosome"][norm_type]["std"], s=5, c=common.visualisation.colours[norm_type], label=norm_type)
                        plt.annotate(chromosome, (vv["mean"] - results["autosome"][norm_type]["mean"], vv["std"] - results["autosome"][norm_type]["std"]))
                        # plt.scatter(vv["mean"] - chromosome_averages[chromosome]["mean"], vv["std"] - chromosome_averages[chromosome]["std"], s=2, c=common.visualisation.colours[norm_type], label=norm_type)
            legend_items = [matplotlib.lines.Line2D([0], [0], color=common.visualisation.colours[norm_type], label=norm_type, marker='o', lw=0, markersize=10) for norm_type in list(results.values())[0].keys()]
            # legend_items.append(matplotlib.lines.Line2D([0], [0], label="average (autosomal)", marker='x', lw=0, markersize=10))
            plt.legend(handles=legend_items)
            plt.xlabel("Offset from autosome-wide overall mean of respective normalisation type, per chromosome")
            plt.ylabel("Offset from autosome-wide average standard deviation\nof respective normalisation type, per chromosome")
            plt.grid()
            plt.show()

        # differences of each normalisation type from chromosome average
        # beta_stat_offsets_pernorm_{} -> cohort
        if 1:
            chromosome_averages = dict()
            for chromosome, v in results.items():
                if chromosome in ["x", "y", "autosome"]:
                    continue
                chromosome_averages[chromosome] = {'mean': 0, 'std': 0}
                for norm_type, vv in v.items():
                    chromosome_averages[chromosome]['mean'] += vv["mean"]
                    chromosome_averages[chromosome]['std'] += vv["std"]

                chromosome_averages[chromosome] = {
                    'mean': chromosome_averages[chromosome]['mean'] / len(common.bio.norm_methods),
                    'std': chromosome_averages[chromosome]['std'] / len(common.bio.norm_methods)}

            print(chromosome_averages)


            # plot differences from the chromosome averages
            # fig, ax = plt.subplots()
            plot_points = list()
            norm_average_offsets = {x: {"mean": 0, "std": 0} for x in common.bio.norm_methods}
            for chromosome, v in results.items():
                if chromosome in ["x", "y"]:
                    continue
                elif chromosome == "autosome":
                    '''
                    for norm_type, vv in v.items():
                        plt.scatter(vv["mean"], vv["std"], s=4, marker="x", c=common.visualisation.colours[norm_type], label=norm_type)
                    '''
                    pass  # not needed for offsets, only for absolutes
                else:

                    for norm_type, vv in v.items():
                        # plt.scatter(vv["mean"] - results["autosome"][norm_type]["mean"], vv["std"] - results["autosome"][norm_type]["std"], s=5, c=common.visualisation.colours[norm_type], label=norm_type)
                        # plt.annotate(chromosome, (vv["mean"] - results["autosome"][norm_type]["mean"], vv["std"] - results["autosome"][norm_type]["std"]))
                        plt.scatter(vv["mean"] - chromosome_averages[chromosome]["mean"], vv["std"] - chromosome_averages[chromosome]["std"], s=2, c=common.visualisation.colours[norm_type], label=norm_type)
                        norm_average_offsets[norm_type]["mean"] += vv["mean"] - chromosome_averages[chromosome]["mean"]
                        norm_average_offsets[norm_type]["std"] += vv["std"] - chromosome_averages[chromosome]["std"]
            norm_average_offsets = {x: {"mean": v["mean"]/len(common.bio.chromosomes_autosomal),
                                        "std": v["std"]/len(common.bio.chromosomes_autosomal)} for x, v in norm_average_offsets.items()}
            for k, v in norm_average_offsets.items():
                plt.scatter(v["mean"], v["std"], s=40, marker="+", c=common.visualisation.dark_colours[k], label=k)

            legend_items = [matplotlib.lines.Line2D([0], [0], color=common.visualisation.colours[norm_type], label=norm_type, marker='o', lw=0, markersize=10) for norm_type in list(results.values())[0].keys()]
            legend_items.append(matplotlib.lines.Line2D([0], [0], label="average", marker='+', lw=0, markersize=10))
            plt.legend(handles=legend_items)
            plt.xlabel("Offset from chromosomal mean of all normalisation types, per chromosome and normalisation type")
            plt.ylabel("Offset from chromosomal average standard deviation\nof all normalisation types, per chromosome and normalisation type")
            plt.grid()
            plt.show()
