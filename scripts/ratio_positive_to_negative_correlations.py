import pandas as pd
from common.format_functions import formatCorrPkl


def run(cohort, chromosome, norm_type, corr_type):
    df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
    num_negatives = df.lt(0).sum().sum()
    num_positives = (df.shape[1] * (df.shape[1] - 1)) - num_negatives
    print(chromosome, corr_type, num_positives / num_negatives)



if __name__ == "__main__":
    cohort_ = "chds"
    norm_type_ = "noob"
    for chromosome_ in ["y", "21", "x"]:
        for corr_type_ in ["pearson", "spearman", "kendall"]:
            run(cohort_, chromosome_, norm_type_, corr_type_)
