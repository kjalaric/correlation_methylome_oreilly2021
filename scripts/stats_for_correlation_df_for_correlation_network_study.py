import json
import itertools
import pandas as pd
import numpy as np
import scipy.stats as stats
import common.file_format
from common.format_functions import formatCorrPkl
import common.bio


def run(cohort, chromosome, norm_type, corr_type="spearman"):
    print("running:", cohort, chromosome, norm_type, corr_type)
    file_out = common.file_format.correlations_same_pathway_overall_report.format(cohort, chromosome, norm_type, corr_type)
    json_write_dict = dict()

    print("loading files...")
    df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type))
    with open(common.file_format.gene_association_report_meta_json.format(cohort, chromosome, norm_type, "all"), "r") as fi:
        pathway_json = json.load(fi)

    print("parsing files...")
    pathway_genes_with_cpgs = {gene: list() for gene in pathway_json["cpg_to_gene_name"].values()}
    for gene in pathway_genes_with_cpgs.keys():
        pathway_genes_with_cpgs[gene] = [k for k, v in pathway_json["cpg_to_gene_name"].items() if v == gene]

    pathway_gene_combinations = set()
    for genes in pathway_json["common_pathways"].values():
        pathway_gene_combinations.add(tuple(genes))
    json_write_dict["identified_overlapping_pathways"] = len(pathway_gene_combinations)

    print("going over genes and cpg sites...")
    # detected genes for json
    detected_genes = set()
    for x in pathway_gene_combinations:
        for xx in x:
            detected_genes.add(xx)
    json_write_dict["n_assessed_genes"] = len(detected_genes)

    # number of cpg sites for json
    json_write_dict["n_cpg_sites"] = df.shape[0]

    print("removing diagonal from main array...")
    # remove diagonal and triangle from the array
    df = df.to_numpy()
    df = df[np.triu_indices(n=df.shape[0], k=1)]
    positive_overall = df[df >= 0]
    negative_overall = df[df < 0]

    print("splitting correlations...")
    json_write_dict["n_positive_correlating_pairs"] = len(positive_overall)
    json_write_dict["n_negative_correlating_pairs"] = len(negative_overall)
    json_write_dict["mean_positive_correlation"] = np.mean(positive_overall)
    json_write_dict["mean_negative_correlation"] = np.mean(negative_overall)

    with open(file_out, "w") as fo:
        json.dump(json_write_dict, fo)
    print("wrote to", file_out)


if __name__ == "__main__":
    for chromosome_ in common.bio.epic_probes_smallest_to_largest:
        run("chds", chromosome_, common.bio.appropriate_norm_type[chromosome_], "spearman")
