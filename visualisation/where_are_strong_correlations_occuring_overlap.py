import matplotlib.pyplot as plt
from common.gene_class import *
from common.defines import *
from chromosomal_distance.distance_between_sites import *
import common.cpg_lookup
import common.file_format
import visualisation.chromosome_size_for_plots
import common.parse_epicarray_manifest as pam  # ASSUMPTION: cpg names didn't change from 450k to EPIC
import common.parse_450k_manifest as pam450k

base_norm_types = ["raw", "noob", "swan", "illumina", "quant", "funnorm"]
meta_norm_types = ["meta6", "metanf"]
all_norm_types = base_norm_types + meta_norm_types

if __name__ == "__main__":
    for cohort in ["chds-450k_dataset_1"]:
        for chromosome in ["21"]:
            for norm_type in all_norm_types:
                positive_correlation_file_location = common.file_format.correlation_overlap.format(cohort, "positive", chromosome, norm_type)
                negative_correlation_file_location = common.file_format.correlation_overlap.format(cohort, "negative", chromosome, norm_type)

                cpg_dict = common.cpg_lookup.getLookupDict("EPIC", chromosome)
                cpg_dict = {v: pam.getGene(v) for k, v in cpg_dict.items()}

                backup_cpg_dict = common.cpg_lookup.getLookupDict("450k", chromosome)
                backup_cpg_dict = {v: pam450k.getGene(v) for k, v in backup_cpg_dict.items()}

                positive_correlations = list()
                with open(positive_correlation_file_location, "r") as fi:
                    for fline in fi.readlines():
                        result = fline.strip().split(",")
                        try:
                            cpg1 = cpg_dict[result[0]]
                        except KeyError:
                            try:
                                cpg1 = backup_cpg_dict[result[0]]
                            except KeyError:
                                print(f"{result[0]} was not found in either dictionary...")
                                continue
                        try:
                            cpg2 = cpg_dict[result[1]]
                        except KeyError:
                            try:
                                cpg2 = backup_cpg_dict[result[1]]
                            except KeyError:
                                print(f"{result[1]} was not found in either dictionary...")
                                continue
                        positive_correlations.append((cpg1, cpg2))

                positive_locus_pairs = list()
                for k in positive_correlations:
                    if k[0].mapinfo is not None and k[1].mapinfo is not None:
                        positive_locus_pairs.append((k[0].mapinfo, k[1].mapinfo))

                negative_correlations = list()
                with open(negative_correlation_file_location, "r") as fi:
                    for fline in fi.readlines():
                        result = fline.strip().split(",")
                        try:
                            cpg1 = cpg_dict[result[0]]
                        except KeyError:
                            try:
                                cpg1 = backup_cpg_dict[result[0]]
                            except KeyError:
                                print(f"{result[0]} was not found in either dictionary...")
                        try:
                            cpg2 = cpg_dict[result[1]]
                        except KeyError:
                            try:
                                cpg2 = backup_cpg_dict[result[1]]
                            except KeyError:
                                print(f"{result[1]} was not found in either dictionary...")
                        negative_correlations.append((cpg1, cpg2))

                negative_locus_pairs = list()
                for k in negative_correlations:
                    if k[0].mapinfo is not None and k[1].mapinfo is not None:
                        negative_locus_pairs.append((k[0].mapinfo, k[1].mapinfo))

                plot_limit = visualisation.chromosome_size_for_plots.chromosome_sizes[chromosome]
                for x in positive_locus_pairs + negative_locus_pairs:
                    assert x[0] < plot_limit, f"{x[0]} beyond plot limit of {plot_limit}"
                    assert x[1] < plot_limit, f"{x[1]} beyond plot limit of {plot_limit}"

                fig, axs = plt.subplots(figsize=(19.2, 10.8))
                axs.plot([x[0] for x in positive_locus_pairs], [x[1] for x in positive_locus_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.7, color="green")
                axs.plot([x[0] for x in negative_locus_pairs], [x[1] for x in negative_locus_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.7, color="red")
                axs.set_title("Strong correlations in cohort [{}]: chr{} | {}".format(cohort, chromosome, norm_type))
                axs.set_xlabel("CpG 1 position")
                axs.set_ylabel("CpG 2 position")
                axs.set_aspect("equal", "box")
                axs.set_xlim(0, plot_limit)
                axs.set_ylim(0, plot_limit)
                axs.grid()
                fig.tight_layout()
                plt.savefig('K:/visualisation/correlation_loci/overlap_{}_chr{}_{}.png'.format(cohort, chromosome, norm_type), dpi=300)