import os
import pandas as pd
import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.cpg_lookup
import data.info
import util.cache

processed_dir_base = "K:/processed/"
file_in_format = "chromosome_{}_betas_{}.csv"  # chromosome, norm method
file_out_format = "{}{}_chr{}_{}.txt"  # strength, direction, chromosome, norm method
correlation_pickle_format = "beta_chromosome_{}_{}_correlation.pkl"  # chromosome, norm method



if __name__ == "__main__":
    chromosome = "21"
    norm_type = "raw"
    cohort = "marmalaid"
    downsample_ratio = 100

    # for lookup only
    array_type = data.info.getArrayType(cohort)  #EPIC, 450k
    cpg_dict = common.cpg_lookup.getLookupDict(array_type, chromosome)

    output_dir = os.path.join(processed_dir_base, cohort)
    correlation_file_pattern = os.path.join(output_dir, file_out_format)
    pkl_dir = os.path.join(output_dir, "pkl")
    corr_pkl_location = os.path.join(pkl_dir, correlation_pickle_format.format(chromosome, norm_type))
    beta_corr = pd.read_pickle(corr_pkl_location)

    x_size = beta_corr.shape[0]
    y_size = beta_corr.shape[1]
    col_names = beta_corr.columns.tolist()

    pairs = list()  # pairs are (correlation_oldformat, distance)
    ds_count = 0
    for x in range(0, x_size):  # row
        if not x % 100:
            print(f"{x}/{x_size}")
        for y in range(x + 1, y_size):  # column
            ds_count += 1
            if not ds_count % downsample_ratio:
                ds_count = 0
            else:
                continue
            try:
                distance = distanceBetweenCpgSites(col_names[y], col_names[x], "450k")  # they should be the same
            except MissingLociException:
                continue
            pairs.append((beta_corr.iloc[x][y], distance))

    try:
        _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}_ds{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type, downsample_ratio
        _cache.save(pairs, cohort, chromosome, norm_type, downsample_ratio)
    except:
        print("Caching didn't work.")

    plt.plot([x[0] for x in pairs], [x[1] for x in pairs], linestyle='None', markersize=1.0, marker=".")
    plt.title(f"{cohort}: distance vs. correlation_oldformat for chr{chromosome} (norm: {norm_type}) - downsample {downsample_ratio}")
    plt.xlabel("Correlation")
    plt.ylabel("Distance (bp)")
    plt.show()