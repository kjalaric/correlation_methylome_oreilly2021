import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from common.format_functions import formatCorrPkl
import common.file_format
import common.parse_epicarray_manifest as pam
import common.bio


if __name__ == "__main__":
    cohort = "chds"
    corr_type = "spearman"
    for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:  # does it in the correct order
        norm_type = common.bio.appropriate_norm_type[chromosome]
        plot_out_file = common.file_format.correlation_loci_heatmap.format(cohort, chromosome, norm_type, corr_type)
        # Create a dataset
        df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))

        np.fill_diagonal(df.values, np.nan)

        mapinfo = {x: pam.getGene(x).mapinfo for x in list(df)}
        df.rename(mapper=mapinfo, axis=1, inplace=True)
        df.rename(mapper=mapinfo, axis=0, inplace=True)
        df.sort_index(axis=1, inplace=True)
        df.sort_index(axis=0, inplace=True)

        fig, ax = plt.subplots(figsize=(19.2, 10.8))

        # Default heatmap
        ax = sns.heatmap(df, cmap='coolwarm_r')
        # plt.show()

        ax.set_title("Heatmap for correlations: chr{} | {} normalisation ({} cohort)".format(chromosome.upper(), norm_type, cohort), fontsize=20)
        ax.set_xlabel("CpG 1 position (bp)", fontsize=20)
        ax.set_ylabel("CpG 2 position (bp)", fontsize=20)
        ax.tick_params(axis='both', which='major', labelsize=10)
        # ax.tick_params(axis='both', which='minor', labelsize=18)
        ax.set_aspect("equal", "box")
        # fig.tight_layout()
        # fig = ax.get_figure()
        fig.savefig(plot_out_file, dpi=300)
        # plt.close()
        print("Written to", plot_out_file)