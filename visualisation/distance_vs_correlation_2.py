import os
import random
import pandas as pd
import numpy as np
# from sklearn.linear_model import LinearRegression
import statsmodels.api as sm
import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.bio
import common.cpg_lookup
import common.file_format
from common.format_functions import formatCorrPkl
import data.info
import util.cache

# processed_dir_base = "K:/processed/"
# file_in_format = "chromosome_{}_betas_{}.csv"  # chromosome, norm method
# file_out_format = "{}{}_chr{}_{}.txt"  # strength, direction, chromosome, norm method
# correlation_pickle_format = "beta_chromosome_{}_{}_correlation.pkl"  # chromosome, norm method


if __name__ == "__main__":
    # chromosome = "21"
    # #norm_type = "swan"
    cohort = "chds"
    corr_type = "spearman"
    plotting_downsample = 1  # in addition to standard downsampling
    downsample_ratio = 1  # only applies if MODE = "generate"
    strongest_percentage = 0.1  # only applies if MODE = "strongest"
    MODES = ["generate", "strongest"]  # generate, strongest, cache
    store_in_cache = True

    for chromosome in ["21", "22", "y"]:  # hasn't been run on hpc yet?
        for norm_type in common.bio.norm_types:
            file_out = common.file_format.loci_distance_csv.format(cohort, corr_type, chromosome, downsample_ratio)

            with open(file_out, "a+") as fo:
                fo.write("norm_type,subset,coef,int,r2,r2adj,f,fp\n")

            print("PROCESSING: ", norm_type)
            pairs_dict = dict()
            results = dict()

            # generate
            if "generate" in MODES:
                pairs_dict["generate"] = list()
                # for lookup only
                # array_type = data.info.getArrayType(cohort)  #EPIC, 450k
                # cpg_dict = common.cpg_lookup.getLookupDict(array_type, chromosome)

                # output_dir = os.path.join(processed_dir_base, cohort)
                # correlation_file_pattern = os.path.join(output_dir, file_out_format)
                # pkl_dir = os.path.join(output_dir, "pkl")
                # corr_pkl_location = os.path.join(pkl_dir, correlation_pickle_format.format(chromosome, norm_type))
                beta_corr = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
                beta_corr = beta_corr.abs()

                x_size = beta_corr.shape[0]
                y_size = beta_corr.shape[1]
                col_names = beta_corr.columns.tolist()

                print("Generating...")
                index = 1
                ct = 1
                if downsample_ratio == 1:
                    for cpg1, row in beta_corr.iterrows():
                        for cpg2, x in row.iteritems():
                            if ct >= index:
                                break
                            ct += 1
                            try:
                                distance = distanceBetweenCpgSites(cpg1, cpg2)
                                pairs_dict["generate"].append((x, distance))
                            except Exception as e:
                                print(e)
                        ct = 1
                        index += 1
                else:  # downsampling
                    ds_count = 0
                    for cpg1, row in beta_corr.iterrows():
                        for cpg2, x in row.iteritems():
                            if ct >= index:
                                break
                            ct += 1
                            if not ds_count % downsample_ratio:
                                ds_count = 0
                            else:
                                continue
                            distance = distanceBetweenCpgSites(cpg1, cpg2)
                            pairs_dict["generate"].append((x, distance))
                        ct = 1
                        index += 1

                if store_in_cache:
                    try:
                        print("Caching...")
                        if downsample_ratio != 1:
                            _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type
                            _cache.save(pairs_dict["generate"], cohort, chromosome, norm_type)
                        else:
                            _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}_ds{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type, downsample_ratio
                            _cache.save(pairs_dict["generate"], cohort, chromosome, norm_type, downsample_ratio)
                    except:
                        print("Caching didn't work.")

            if "strongest" in MODES:
                pairs_dict["strongest"] = list()
                print("Finding the strongest correlations...")

                beta_corr = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
                beta_corr = beta_corr.abs()
                sorted_df = beta_corr.unstack().sort_values(ascending=False)[beta_corr.shape[1]:]  # quicksort by default, mergesort is more stable. also remove the diagonal
                del beta_corr

                strong_correlations = sorted_df[:int(strongest_percentage*len(sorted_df)*2)]  # above will still have two of everything because diagonally-symmetrical
                strong_correlations_dict = dict()  # cant just take every second one in case some correlations are the same
                for i, x in strong_correlations.iteritems():
                    strong_correlations_dict[tuple(sorted(i))] = x
                for k, v in strong_correlations_dict.items():
                    pairs_dict["strongest"].append((v, distanceBetweenCpgSites(k[0], k[1])))

            # use cached results
            if "cache" in MODES:
                print("Using cached results...")
                if downsample_ratio == 1:
                    _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type
                    pairs_dict["cache"] = _cache.load(cohort, chromosome, norm_type)
                else:
                    _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}_ds{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type, downsample_ratio
                    pairs_dict["cache"] = _cache.load(cohort, chromosome, norm_type, downsample_ratio)

            for mode, pairs in pairs_dict.items():
                # apply plot downsampling
                if plotting_downsample != 1:
                    pairs = random.sample(pairs, int(len(pairs)/plotting_downsample))

                print("Regressing...")
                '''
                lm = LinearRegression().fit(np.array([x[1] for x in pairs]).reshape(-1, 1), np.array([x[0] for x in pairs]))
                print(lm.score(np.array([x[1] for x in pairs]).reshape(-1, 1), [x[0] for x in pairs]))
                print(lm.coef_)
                print(lm.intercept_)
                '''

                lm = sm.OLS([x[0] for x in pairs], sm.add_constant([x[1] for x in pairs]))
                results[mode] = lm.fit()

                '''
                # print(results.summary())
                print(results[mode].rsquared)
                print(results[mode].rsquared_adj)
                print(results[mode].fvalue)
                print(results[mode].f_pvalue)
                # print(f"{results.params[1]},{results.params[0]},{results.rsquared},{results.rsquared_adj},{results.fvalue},{results.f_pvalue},{results.rsquared},{results.rsquared},{results.rsquared},")
    
                maximum = max([x[1] for x in pairs])
                print("Plotting...")
                plt.scatter([x[1] for x in pairs], [x[0] for x in pairs], s=1.0, marker=".", alpha=0.1)  # correlation on y axis, distance on x axis so y = mx + c makes more sense
                # plt.plot([0, maximum], [lm.intercept_, maximum*lm.coef_[0]+lm.intercept_], c="red")
                plt.plot([0, maximum], [results[mode].params[0], maximum * results[mode].params[1] + results[mode].params[0]], c="red")
                plt.title(f"{cohort}: distance vs. correlation for chr{chromosome} (norm: {norm_type})")
                plt.ylabel("Spearman Rank Correlation")
                plt.xlabel("Distance (bp)")
                plt.show()
                '''

            with open(file_out, "a+") as fo:
                for k, v in results.items():
                    fo.write(f"{norm_type},{k}," + ",".join(str(x) for x in [v.params[1], v.params[0], v.rsquared, v.rsquared_adj, v.fvalue, v.f_pvalue])+"\n")
