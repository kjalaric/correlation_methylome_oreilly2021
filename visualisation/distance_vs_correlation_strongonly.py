import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.cpg_lookup

file_location_format = "K:/processed/{}/strong{}_chr{}_{}.txt"  # cohort, positive/negative, chr, normtype


if __name__ == "__main__":
    chromosome = "21"
    norm_type = "noob"
    cohort = "chds"
    direction = "positive"

    # for lookup only
    array_type = "EPIC"  #EPIC, 450k

    correlation_file_location = file_location_format.format(cohort, direction, chromosome, norm_type)
    cpg_dict = common.cpg_lookup.getLookupDict(array_type, chromosome)

    correlations = dict()
    with open(correlation_file_location, "r") as fi:
        for fline in fi.readlines():
            result = fline.strip().split(",")
            correlations[(cpg_dict[result[0]], cpg_dict[result[1]])] = [float(result[2]), 0.0]

    bad_pairs = list()
    for k in correlations.keys():
        try:
            correlations[k][1] = distanceBetweenCpgSites(k[0], k[1])
        except MissingLociException:
            bad_pairs.append(k)
    for k in bad_pairs:
        del correlations[k]

    plt.plot([x[0] for x in correlations.values()], [x[1] for x in correlations.values()], linestyle='None', markersize=1.0, marker=".")
    plt.show()