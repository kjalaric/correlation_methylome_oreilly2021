import matplotlib.pyplot as plt
import pandas as pd

FILE_LOC = "K:/cohort1/processed/beta_variance_comparison.csv"
df = pd.read_csv(FILE_LOC)

range

fig, ax = plt.subplots(2,2)
ax[0,0].set_title("Histogram: Standard Deviations")
ax[0,0].hist(df["stdev"], bins=40)
ax[0,0].grid()
ax[1,0].set_title("Histogram: Ranges")
ax[1,0].hist(df["max"]-df["min"], bins=40)
ax[1,0].grid()
ax[0,1].set_title("Histogram: Means")
ax[0,1].hist(df["mean"], bins=40)
ax[0,1].grid()
ax[1,1].set_title("Standard Deviation vs. Mean")
ax[1,1].scatter(df["mean"], df["stdev"], s=1)
ax[1,1].grid()
plt.show()