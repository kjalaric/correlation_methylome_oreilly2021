import json
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.lines
import common.bio
from common.format_functions import formatBetaCorrelationStatReport
import common.file_format
import common.visualisation


if (0):  # overload print
    def print(*x):
        pass


if __name__ == "__main__":
    for cohort in ["chds", "mtab7069"]:  # need to regenerate for non-CHDS cohorts
        chromosomes = common.bio.chromosomes_autosomal
        norm_types = common.bio.norm_methods
        corr_type = "spearman"

        results_per_chromosome = {x: {y: None for y in norm_types} for x in chromosomes}
        averages_per_chromosome = {x: None for x in chromosomes}
        good_chromosomes = list()
        for chromosome, results_per_norm_type in results_per_chromosome.items():
            next_chr = False
            for norm_type in norm_types:
                try:
                    filename = formatBetaCorrelationStatReport(cohort, chromosome, norm_type, corr_type, meta="basic")
                    with open(filename, "r") as fi:
                        results_per_norm_type[norm_type] = json.load(fi)
                except FileNotFoundError:
                    print("Couldn't find file:", filename)
                    print("Skipping chromosome", chromosome)
                    next_chr = True
                    break
            if next_chr:
                continue
            else:
                good_chromosomes.append(chromosome)

            averages_per_chromosome[chromosome] = {x: 0 for x in (next(iter(results_per_norm_type.values())).keys())}
            for _, results in results_per_norm_type.items():
                for k, v in results.items():
                    averages_per_chromosome[chromosome][k] += v
            averages_per_chromosome[chromosome] = {k: v/6.0 for k, v in averages_per_chromosome[chromosome].items()}


        # calculate offsets for mean and standard deviation
        mean_offsets = {x: {y: None for y in good_chromosomes} for x in norm_types}
        variance_offsets = {x: {y: None for y in good_chromosomes} for x in norm_types}

        for chromosome in good_chromosomes:
            for norm_type, results in results_per_chromosome[chromosome].items():
                mean_offsets[norm_type][chromosome] = results["overall_average_correlation"] - averages_per_chromosome[chromosome]["overall_average_correlation"]
                variance_offsets[norm_type][chromosome] = results["overall_variance"] - averages_per_chromosome[chromosome]["overall_variance"]

        for norm_type in norm_types:
            for chromosome in good_chromosomes:
                x_pos = mean_offsets[norm_type][chromosome]
                y_pos = variance_offsets[norm_type][chromosome]  # standard deviation is sqrt of variance
                plt.scatter(x_pos, y_pos, s=5, c=common.visualisation.colours[norm_type], label=norm_type)
                plt.annotate(chromosome, (x_pos, y_pos))

        legend_items = [matplotlib.lines.Line2D([0], [0], color=common.visualisation.colours[norm_type], label=norm_type, marker='o', lw=0, markersize=10) for norm_type in norm_types]
        # legend_items.append(matplotlib.lines.Line2D([0], [0], label="average (autosomal)", marker='x', lw=0, markersize=10))
        plt.legend(handles=legend_items)
        plt.xlabel("Offset from average mean correlation of chromosome, per normalisation type")
        plt.ylabel("Offset from average correlation variance\n of chromosome, per normalisation type")
        plt.grid()
        plt.show()
