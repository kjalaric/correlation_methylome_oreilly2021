'''
Draw pretty diagrams showing links between chromosomes
'''

import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors
import pickle as pkl
import pipeline.correlation_network as cornet

plot_options_spread_out = {
    'node_color': 'purple',
    'node_size': 7,
    'alpha': 0.5
}

plot_options_compact_and_sparse = {
    'node_color': 'grey',
    'node_size': 10,
    'alpha': 1.0,
    'font_color': 'purple'
}

plot_options_compact = {
    'node_color': 'black',
    'node_size': 5,
    'alpha': 0.8,
    'font_color': 'magenta'
}


def plotGraph(G, minimum_children=0, exclusion_list=[], labels=True, chromosomes=False, plot_option_set=plot_options_spread_out):
    if not len(exclusion_list) == 0:
        G.remove_nodes_from(exclusion_list)
        isolate_nodes = list(nx.isolates(G))
        G.remove_nodes_from(isolate_nodes)
        print("Excluding the following nodes: {}\nRemoving resulting isolate nodes: {}".format(exclusion_list, isolate_nodes))

    if minimum_children > 0:
        # can't just delete them outright as that'd make more isolate nodes than there needs to be
        removal_list = list()
        for node in list(G.nodes):
            if len(list(G.neighbors(node))) < minimum_children:
                removal_list.append(node)
        G.remove_nodes_from(removal_list)
        isolate_nodes = list(nx.isolates(G))
        G.remove_nodes_from(isolate_nodes)


    edges = G.edges()
    colors = [G[u][v]['edge_color'] for u, v in edges]
    colors = [matplotlib.colors.to_rgb(x) + (0.3,) for x in colors]  # easier on the eyes
    weights = [G[u][v]['weight'] for u, v in edges]

    nx.draw(G, edges=edges, edge_color=colors, with_labels=labels, **plot_option_set)  # , width=weights
    plt.show()


def generateAndPlotNetwork(cohort, chromosome, norm_type, corrtype="spearman", threshold=0.7, negatives_only=False, regenerate_graph=False,
                           strongest=None, cpgs_only=False, no_labels=False, focus=None, max_focus_dist=None, plot_option_set=plot_options_spread_out,
                           light_colours=False, use_gencode_for_gene_names=True):
    G = cornet.generateNetwork(cohort, chromosome, norm_type, corrtype, threshold, negatives_only, regenerate_graph, strongest, cpgs_only, light_colours, use_gencode_for_gene_names)
    if focus:
        G = cornet.pruneExceptFor(G, focus, max_dist=max_focus_dist)
    plotGraph(G, minimum_children=0, labels=not no_labels, plot_option_set=plot_option_set)


def generateAndPlotSubnetworks(cohort, chromosome, norm_type, corrtype="spearman", threshold=0.7, negatives_only=False, regenerate_graph=False,
                           strongest=None, cpgs_only=False, no_labels=False, focus=None, max_focus_dist=None, plot_option_set=plot_options_spread_out,
                           light_colours=False, use_gencode_for_gene_names=False):
    G = cornet.generateNetwork(cohort, chromosome, norm_type, corrtype, threshold, negatives_only, regenerate_graph, strongest, cpgs_only, light_colours, use_gencode_for_gene_names)
    if focus:
        G = cornet.pruneExceptFor(G, focus, max_dist=max_focus_dist)

    for g in nx.connected_components(G):
        x = G.subgraph(g).copy()
        plotGraph(x, minimum_children=0, labels=not no_labels, plot_option_set=plot_option_set)


def plotFromPkl(pkl_location, no_labels=False, focus=None, max_focus_dist=None):
    with open(pkl_location, "rb") as fi:
        G = pkl.load(fi)
    if focus:
        G = cornet.pruneExceptFor(G, focus, max_dist=max_focus_dist)
    plotGraph(G, labels=not no_labels)


if __name__ == "__main__":
    # plotFromPkl("K:/incoming/spearman_chr21_noob_strongest1060900.nwx", no_labels=False, focus="KRTAP6-1", max_focus_dist=1)
    # sys.exit()


    # unit test
    '''
    connections = [
        ("cg12543216", "cg125468779"),
        ("cg12543216", "cg13644587"),
        ("cg125468779", "cg35426128"),
        ("cg125468779", "cg13644587"),
        ("cg35426128", "cg13644587"),
        ("cg45698743", "cg13644587"),
    ]

    G = cornet.constructGraph(connections)
    for x in connections:
        G[x[0]][x[1]]['edge_color'] = "pink"
        G[x[0]][x[1]]['weight'] = 2.0

    cornet.graphStats(G)
    cornet.rankByNeighbours(G)
    plotGraph(G)
    '''

    # plot_options_spread_out
    # plot_options_compact
    # plot_options_compact_and_sparse

    cohort = "chds"
    chromosome = "y"
    norm_type = "swan"
    corr_type = "spearman"
    threshold = 0.5

    import common.bio

    no_labels = False
    take_strongest = common.bio.epic_probe_count[chromosome]*0.1
    cpgs_only = False
    negatives_only = False
    regenerate_graph = True

    focus = None
    max_focus_dist = None
    plot_option_set = plot_options_compact_and_sparse
    light_colours = False
    gencode = True


    # generateAndPlotNetwork("chds", "21", "noob", "spearman", threshold=0.7, strongest=None, cpgs_only=False, no_labels=False, negatives_only=False, regenerate_graph=True)
    generateAndPlotNetwork(cohort, chromosome, norm_type, corr_type, threshold=threshold, strongest=take_strongest, cpgs_only=cpgs_only, no_labels=no_labels,
                           negatives_only=negatives_only, regenerate_graph=regenerate_graph, focus=focus, max_focus_dist=max_focus_dist, plot_option_set=plot_option_set,
                           light_colours=light_colours, use_gencode_for_gene_names=gencode)
    # generateAndPlotNetwork("chds", "21", "raw", "spearman", threshold=0.8, strongest=None, negatives_only=False, regenerate_graph=True, focus=["DSCAM"], max_focus_dist=1)

