import os
import random
import json
import pandas as pd
import numpy as np
# from sklearn.linear_model import LinearRegression
import statsmodels.api as sm
import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.bio
import common.cpg_lookup
import common.file_format
from common.format_functions import formatCorrPkl
import data.info
import util.cache
import visualisation.chromosome_size_for_plots

# processed_dir_base = "K:/processed/"
# file_in_format = "chromosome_{}_betas_{}.csv"  # chromosome, norm method
# file_out_format = "{}{}_chr{}_{}.txt"  # strength, direction, chromosome, norm method
# correlation_pickle_format = "beta_chromosome_{}_{}_correlation.pkl"  # chromosome, norm method


def run(cohort, chromosome, norm_type, corr_type):
    downsampling = 100

    json_out = common.file_format.loci_distance_json_ds.format(cohort, chromosome, norm_type, corr_type, downsampling)
    plot_out = common.file_format.loci_distance_plot_ds.format(cohort, chromosome, norm_type, corr_type, downsampling)

    print("PROCESSING: ", cohort, chromosome, norm_type, corr_type)
    positive_pairs = list()
    negative_pairs = list()

    # generate
    beta_corr = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
    # beta_corr = beta_corr.abs()


    print("Generating distances...")
    index = 1
    ct = 1
    downsample_ct = 0
    for cpg1, row in beta_corr.iterrows():
        for cpg2, x in row.iteritems():
            if ct >= index:
                break
            ct += 1
            downsample_ct = downsample_ct + 1
            if downsample_ct >= downsampling:
                downsample_ct = 0
                try:
                    distance = distanceBetweenCpgSites(cpg1, cpg2)
                    if x >= 0.0:
                        positive_pairs.append((x, distance))
                    else:
                        negative_pairs.append((x, distance))
                except Exception as e:
                    print(e)
        ct = 1
        index += 1

    print("Regressing positives...")
    lm = sm.OLS([x[0] for x in positive_pairs], sm.add_constant([x[1] for x in positive_pairs]))
    positive_results = lm.fit()
    
    print("Regressing negatives...")
    lm = sm.OLS([x[0] for x in negative_pairs], sm.add_constant([x[1] for x in negative_pairs]))
    negative_results = lm.fit()

    results_d = {
        "pos_coef": positive_results.params[1],
        "pos_int": positive_results.params[0],
        "pos_r2": positive_results.rsquared,
        "pos_r2adj": positive_results.rsquared_adj,
        "pos_f": positive_results.fvalue,
        "pos_fp": positive_results.f_pvalue,
        "neg_coef": negative_results.params[1],
        "neg_int": negative_results.params[0],
        "neg_r2": negative_results.rsquared,
        "neg_r2adj": negative_results.rsquared_adj,
        "neg_f": negative_results.fvalue,
        "neg_fp": negative_results.f_pvalue
    }

    with open(json_out, "w") as fo:
        json.dump(results_d, fo)
    print("Written to", json_out)


    positive_maximum = max([x[1] for x in positive_pairs])
    negative_maximum = max([x[1] for x in negative_pairs])
    plot_x_limit = visualisation.chromosome_size_for_plots.chromosome_sizes[chromosome]
    print("Plotting...")

    fig, axs = plt.subplots(figsize=(19.2, 10.8))
    axs.plot([x[1] for x in positive_pairs], [x[0] for x in positive_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.4, color="lightgreen")
    axs.plot([x[1] for x in negative_pairs], [x[0] for x in negative_pairs], linestyle='None', markersize=1.0, marker=".", alpha=0.4, color="orange")

    axs.plot([0, positive_maximum], [positive_results.params[0], positive_maximum * positive_results.params[1] + positive_results.params[0]], c="darkgreen")
    axs.plot([0, negative_maximum], [negative_results.params[0], negative_maximum * negative_results.params[1] + negative_results.params[0]], c="darkred")

    axs.set_title(f"{cohort}: distance vs. correlation for chr{chromosome} (norm: {norm_type})", fontsize=20)
    axs.set_xlabel("Distance (bp)", fontsize=20)
    axs.set_ylabel("Spearman Rank Correlation", fontsize=20)
    axs.tick_params(axis='both', which='major', labelsize=18)
    axs.tick_params(axis='both', which='minor', labelsize=18)
    axs.set_xlim(0, plot_x_limit)
    axs.set_ylim(-1.0, 1.0)
    axs.grid()
    fig.tight_layout()
    plt.savefig(plot_out, dpi=300)
    print("Plot output to", plot_out)



if __name__ == "__main__":
    # chromosome = "21"
    # #norm_type = "swan"
    cohort_ = "chds"
    corr_type_ = "spearman"

    for chromosome_ in common.bio.epic_probes_smallest_to_largest[:13]:  # hasn't been run on hpc yet?
        for norm_type_ in ["noob"]:
            run(cohort_, chromosome_, norm_type_, corr_type_)