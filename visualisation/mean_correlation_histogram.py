import pandas as pd
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from common.format_functions import *


def run(cohort, chromosome, norm_type, corr_type="spearman"):
    print("Calculating for: ", cohort, chromosome, norm_type, corr_type)
    df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
    vals = [x for x in df.stack() if x != 1.0]

    actual = list()
    reserve = set()
    for x in vals:
        if x in reserve:
            reserve.remove(x)
            actual.append(x)
        else:
            reserve.add(x)

    print("Calculating basics...")
    average = np.average(actual)
    print(average)
    stdev = np.std(actual)
    print(stdev)

    print("Calculating skew...")
    skew_biased = st.skew(actual, bias=True)
    print(skew_biased)
    skew_unbiased = st.skew(actual, bias=False)
    print(skew_unbiased)

    print("Calculating kurtosis...")
    fisher_kurtosis_biased = st.kurtosis(actual, fisher=True, bias=True)
    print(fisher_kurtosis_biased)
    fisher_kurtosis_unbiased = st.kurtosis(actual, fisher=True, bias=False)
    print(fisher_kurtosis_unbiased)
    # pearson_kurtosis_biased = st.kurtosis(actual, fisher=False, bias=True)
    # print(pearson_kurtosis_biased)
    # pearson_kurtosis_unbiased = st.kurtosis(actual, fisher=False, bias=False)
    # print(pearson_kurtosis_unbiased)

    plt.hist(actual, bins=100, weights=np.ones(len(actual))/len(actual))
    plt.title("Distribution of beta correlation ({}) coefficients: {} - Chromosome {}, {} normalisation".format(corr_type, cohort, chromosome, norm_type), wrap=True)
    plt.xlabel("{} correlation coefficient".format(corr_type.capitalize()))
    plt.ylabel("Frequency")
    plt.grid()
    plt.show()


if __name__ == "__main__":
    run("chds", "21", "noob")
    run("chds", "21", "raw")
