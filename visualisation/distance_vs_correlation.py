import os
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from common.gene_class import *
from chromosomal_distance.distance_between_sites import *
import common.cpg_lookup
from common.format_functions import formatCorrPkl
import data.info
import util.cache

# processed_dir_base = "K:/processed/"
# file_in_format = "chromosome_{}_betas_{}.csv"  # chromosome, norm method
# file_out_format = "{}{}_chr{}_{}.txt"  # strength, direction, chromosome, norm method
# correlation_pickle_format = "beta_chromosome_{}_{}_correlation.pkl"  # chromosome, norm method


if __name__ == "__main__":
    chromosome = "21"
    norm_type = "swan"
    cohort = "chds"
    corr_type = "spearman"
    downsample_ratio = 1000

    # generate
    if 1:
        # for lookup only
        array_type = data.info.getArrayType(cohort)  #EPIC, 450k
        cpg_dict = common.cpg_lookup.getLookupDict(array_type, chromosome)

        # output_dir = os.path.join(processed_dir_base, cohort)
        # correlation_file_pattern = os.path.join(output_dir, file_out_format)
        # pkl_dir = os.path.join(output_dir, "pkl")
        # corr_pkl_location = os.path.join(pkl_dir, correlation_pickle_format.format(chromosome, norm_type))
        beta_corr = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corr_type))
        beta_corr = beta_corr.abs()

        x_size = beta_corr.shape[0]
        y_size = beta_corr.shape[1]
        col_names = beta_corr.columns.tolist()

        print("Generating...")
        pairs = list()  # pairs are (correlation_oldformat, distance)
        if downsample_ratio != 1:
            for x in range(0, x_size):  # row
                for y in range(x + 1, y_size):  # column
                    try:
                        distance = distanceBetweenCpgSites(col_names[y], col_names[x])  # they should be the same
                    except MissingLociException:
                        continue
                    pairs.append((beta_corr.iloc[x][y], distance))
        else:  # downsampling
            ds_count = 0
            for x in range(0, x_size):  # row
                for y in range(x + 1, y_size):  # column
                    ds_count += 1
                    if not ds_count % downsample_ratio:
                        ds_count = 0
                    else:
                        continue
                    try:
                        distance = distanceBetweenCpgSites(col_names[y], col_names[x], "450k")  # they should be the same
                    except MissingLociException:
                        continue
                    pairs.append((beta_corr.iloc[x][y], distance))

        try:
            print("Caching...")
            if downsample_ratio != 1:
                _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type
                _cache.save(pairs, cohort, chromosome, norm_type)
            else:
                _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}_ds{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type, downsample_ratio
                _cache.save(pairs, cohort, chromosome, norm_type, downsample_ratio)
        except:
            print("Caching didn't work.")

    # use cached results
    else:
        if downsample_ratio != 1:
            _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type
            pairs = _cache.load(cohort, chromosome, norm_type)
        else:
            _cache = util.cache.Cache("distance_vs_correlation", use_format="{}_{}_{}_ds{}", make_cache_if_doesnt_exist=True)  # cohort, chromosome, norm type, downsample_ratio
            pairs = _cache.load(cohort, chromosome, norm_type, downsample_ratio)


    print("Regressing...")
    lm = LinearRegression().fit(np.array([x[0] for x in pairs]).reshape(-1, 1), np.array([x[1] for x in pairs]))
    print(lm.score(np.array([x[0] for x in pairs]).reshape(-1, 1), [x[1] for x in pairs]))
    print(lm.coef_)
    print(lm.intercept_)

    print("Plotting...")
    plt.scatter([x[0] for x in pairs], [x[1] for x in pairs], s=1.0, marker=".", alpha=0.1)
    plt.plot([0, -lm.intercept_/lm.coef_[0]], [lm.intercept_, 0], c="red")
    plt.title(f"{cohort}: distance vs. correlation_oldformat for chr{chromosome} (norm: {norm_type})")
    plt.xlabel("Correlation")
    plt.ylabel("Distance (bp)")
    plt.show()