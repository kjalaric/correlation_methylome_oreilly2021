base_norm_types = ["raw", "noob", "swan", "illumina", "quant", "funnorm"]
meta_norm_types = ["meta6", "metanf"]
all_norm_types = base_norm_types + meta_norm_types