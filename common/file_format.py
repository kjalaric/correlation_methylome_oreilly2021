import sys
import os
import config.correlation

if sys.platform == "linux":  # RCC
    _repo_dir = "/home/rccuser/masters"
    _base_dir = "/home/rccuser/project"

else:  # home
    _repo_dir = "C:/repo/masters"
    _base_dir = "K:/"

# MAIN DIRECTORIES AND NON-COHORT-SPECIFIC SUBDIRECTORIES
pipeline_dir = os.path.join(_repo_dir, "pipelineold")
processed_dir = os.path.join(_base_dir, "processed")
reports_dir = os.path.join(_base_dir, "reports")
source_dir = os.path.join(_base_dir, "source")
cache_dir = os.path.join(_base_dir, "_cache")
utility_dir = os.path.join(_base_dir, "utility")
database_dir = os.path.join(_base_dir, "database")
visualisation_dir = os.path.join(_base_dir, "visualisation")
pkl_dir = os.path.join(_base_dir, "_pkl")
simulation_dir = os.path.join(_base_dir, "simulations")


# third-party databases
gencode_v37_loc = os.path.join(source_dir, "gencode.v37.annotation.gtf")

# databases
cpg_database = os.path.join(database_dir, "cpg.db")

# manifests
manifest_epic = os.path.join(_base_dir, "infinium-methylationepic-v-1-0-b5-manifest-file.csv")  # header=7, index_col="IlmnID" when using pandas.read_csv
manifest_450k = os.path.join(_base_dir, "illumina450genes.csv")  # header=7, index_col="IlmnID" when using pandas.read_csv

# sitecounts
cpg_sitecount = os.path.join(utility_dir, "{}_sitecount_{}.txt")  # 450k/EPIC, chromosome

# betas
betacsv_all = os.path.join(processed_dir, "{}/beta_{}.csv")  # cohort, norm_type
betacsv_autosomal = os.path.join(processed_dir, "{}/autosomal_beta_{}.csv")  # cohort, norm_type
betacsv_per_chromosome = os.path.join(processed_dir, "{}/chromosome_{}_betas_{}.csv")  # cohort, chromosome, norm_type

# utility directory
cpgsites_per_chromosome = os.path.join(utility_dir, "{}_sitecount_{}.txt")  # array type, chromosome
rna_pair_location = os.path.join(utility_dir, "rna_pairs_{}.txt")  # array type
similar_gene_pairs = os.path.join(utility_dir, "similar_genes_{}.csv")  # array type
unique_sites_per_array = os.path.join(utility_dir, "unique_sites_{}.txt")  # array type

# correlations
correlation_oldformat = os.path.join(processed_dir, "{}/{}{}_chr{}_{}.txt")  # cohort, strength, positive/negative, chr, normtype
correlation = os.path.join(processed_dir, "{}/{}_{}{}_chr{}_{}.txt")  # cohort, corrtype, strength, positive/negative, chr, normtype
correlation_subsample = os.path.join(processed_dir, "{}/{}_{}{}_chr{}_{}_ss{}_{}.txt")  # cohort, corrtype, strength, positive/negative, chr, normtype, subsample_size, iteration
correlation_strongest_set = os.path.join(processed_dir, "{}/{}_{}_{}{}_{}_{}.txt")  # cohort, corrtype, number_of_strongest, strength, positive/negative, chromosome, norm_type
correlation_strongest_percentage = os.path.join(processed_dir, "{}/{}_{}pc_{}_{}_{}.txt")  # cohort, corrtype, percentage, positive/negative, chromosome, norm_type
correlation_strongest_10percent = os.path.join(processed_dir, "{}/{}_{}pc_{}_{}_{}.txt")  # cohort, corrtype, percentage, positive/negative, chromosome, norm_type
correlation_strongest_for_corr_method_test = os.path.join(processed_dir, "{}_{}_{}.txt")  # corrtype, chromosome, direction

# correlation_overlap = os.path.join(processed_dir, "overlap/{}_strong{}_chr{}_{}.txt")  # cohorts, positive/negative, chr, normtype
correlation_meta = os.path.join(processed_dir, "{}/{}{}_chr{}_{}_{}.txt")  # cohort, strength, positive/negative, chr, normtype, meta
# correlation_overlap_meta = os.path.join(processed_dir, "overlap/{}_strong{}_chr{}_{}_{}.txt")  # cohorts, positive/negative, chr, normtype, meta

# correlation overlap reports
correlation_overlap_corr_method = os.path.join(reports_dir, "{}_{}_{}.txt")  # cohort, chromosome, norm_type

# norm type overlap reports
correlation_overlap_norm_type = os.path.join(reports_dir, "{}_{}_{}.txt")  # cohort, chromosome, corr_type

# internet search summary location
internet_search_summary = os.path.join(reports_dir, "internet_search/{}_{}_{}_{}.txt")  # cohort, chromosome, norm_type, some identifier

# network file objects
correlation_network = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}.nwx")  # cohort, corrtype, chromosome, normtype
correlation_network_overlap = os.path.join(processed_dir, "overlap/nx/{}_chr{}_{}.nwx")  # cohort, chromosome, normtype, meta

correlation_network_meta = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}_{}.nwx")  # cohort, corrtype, chromosome, normtype, meta
correlation_network_overlap_meta = os.path.join(processed_dir, "overlap/nx/{}_chr{}_{}_{}.nwx")  # cohort, chromosome, normtype, meta

correlation_network_10pc = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}.nwx")  # cohort, corr_type, chromosome, norm_type
correlation_network_10pc_negatives = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}_n.nwx")  # cohort, corr_type, chromosome, norm_type
correlation_network_10pc_json = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}.json")  # cohort, corr_type, chromosome, norm_type
correlation_network_10pc_negatives_json = os.path.join(processed_dir, "{}/nx/{}_chr{}_{}_n.json")  # cohort, corr_type, chromosome, norm_type


# pickled objects
correlation_pkl = os.path.join(processed_dir, "{}/pkl/", "{}".format(config.correlation.type) + "_beta_chromosome_{}_{}.pkl")  # cohort, chromosome, normtype
correlation_pkl_meta = os.path.join(processed_dir, "{}/pkl/", "{}".format(config.correlation.type) + "_beta_chromosome_{}_{}_correlation_{}.pkl")  # cohort, chromosome, normtype, meta
correlation_pkl_corrtype = os.path.join(processed_dir, "{}/pkl/{}_beta_chromosome_{}_{}.pkl")  # cohort, correlation type, chromosome, normtype
correlation_pkl_corrtype_no_nan = os.path.join(processed_dir, "{}/pkl/{}_beta_chromosome_{}_{}_no_nans.pkl")  # cohort, correlation_oldformat type, chromosome, normtype
correlation_pkl_corrtype_subsampled = os.path.join(processed_dir, "{}/pkl/{}_beta_chromosome_{}_{}_ss{}_{}.pkl")  # cohort, correlation_oldformat type, chromosome, normtype, subsample_size, iteration

pkl_manifest_850k = os.path.join(pkl_dir, "850kmanifest.pkl")
pkl_manifest_450k = os.path.join(pkl_dir, "450kmanifest.pkl")
pkl_gencode_chromosome_notag = os.path.join(pkl_dir, "gencode_{}.pkl")  # chromosome, of format chrN
pkl_gencode_chromosome = os.path.join(pkl_dir, "gencode_chr{}.pkl")  # chromosome, of format chrN


# pkl: genes with cpg information
gene_cpg_information_pkl_dir = os.path.join(pkl_dir, "cpg_info/")
gene_cpg_information_pkl = os.path.join(gene_cpg_information_pkl_dir, "{}/{}.pkl")  # array_type, gene

# jsons of the above
gene_cpg_information_csv_dir = os.path.join(utility_dir, "cpg_info/")
gene_cpg_information_csv = os.path.join(gene_cpg_information_csv_dir, "{}/{}.csv")  # array_type, gene


# correlation_oldformat reports
genome_wide_correlation_report_dir = os.path.join(reports_dir, "correlations/")
full_genome_correlation = os.path.join(genome_wide_correlation_report_dir, "full_genome_{}_{}_{}.csv")  # cpg, cohort, norm_type
full_genome_correlation_with_gene = os.path.join(genome_wide_correlation_report_dir, "full_genome_{}_{}_{}_{}.csv")  # gene, cpg, cohort, norm_type


# statistical reports
statistical_distribution = os.path.join(reports_dir, "{}/stat_dists_{}_{}.json")  # cohort, chromosome, normtype
stats_for_correlation_df = os.path.join(reports_dir, "{}/stats/{}_{}_{}_{}.json")  # cohort, cohort, chromosome, normtype, correlationtype
stats_for_correlation_df_data = os.path.join(reports_dir, "{}/stats/{}_{}_{}_{}.csv")  # cohort, cohort, chromosome, normtype, correlationtype
stats_for_correlation_df_meta = os.path.join(reports_dir, "{}/stats/{}_{}_{}_{}_{}.txt")  # cohort, cohort, chromosome, normtype, correlationtype, meta
stats_for_correlation_df_data_meta = os.path.join(reports_dir, "{}/stats/{}_{}_{}_{}.csv")  # cohort, cohort, chromosome, normtype, meta

description_for_beta_sample_pkl = os.path.join(processed_dir, "{}/pkl/beta_desc_samples_{}_{}.pkl")  # cohort, chromosome, normtype
description_for_beta_cpg_pkl = os.path.join(processed_dir, "{}/pkl/beta_desc_cpg_{}_{}.pkl")  # cohort, chromosome, normtype
stats_beta_json = os.path.join(reports_dir, "{}/beta_stats.json")  # cohort, corr_type

# gene association reports
gene_association_report = os.path.join(reports_dir, "gene_associations/{}_chr{}_{}.txt")  # cohort, chromosome, normtype
gene_association_report_meta = os.path.join(reports_dir, "gene_associations/{}_chr{}_{}_{}.txt")  # cohort, chromosome, normtype, meta
gene_association_report_json = os.path.join(reports_dir, "gene_associations/{}_chr{}_{}.json")  # cohort, chromosome, normtype
gene_association_report_meta_json = os.path.join(reports_dir, "gene_associations/{}_chr{}_{}_{}.json")  # cohort, chromosome, normtype, meta
gene_association_report_strong_json = os.path.join(reports_dir, "gene_associations/{}_chr{}_{}_strong.json")  # cohort, chromosome, normtype

# similar name reports
similar_name_report = os.path.join(reports_dir, "similar_names/{}_chr{}_{}.txt")  # cohort, chromosome, normtype
similar_name_report_meta = os.path.join(reports_dir, "similar_names/{}_chr{}_{}_{}.txt")  # cohort, chromosome, normtype, meta

# gene-rna comparisons
rna_pair_correlations = os.path.join(reports_dir, "{}_chr{}_{}_gene_rna_correlations.csv")  # cohort, chromosome, normtype

# ratio of overlapping correlations to unique correlations (monte carlo)
overlap_ratio_report = os.path.join(simulation_dir, "overlap_ratio_results/{}_{}_{}_{}_{}.csv")  # correlation_type, cohort, chromosome, norm_type, iteration/meta

# visualisation
correlation_loci_dir = os.path.join(visualisation_dir, "correlation_loci")
correlation_loci_plot = os.path.join(correlation_loci_dir, "{}_chr{}_{}.png")   # cohort, chromosome, normtype
correlation_loci_plot_10pc = os.path.join(correlation_loci_dir, "{}_chr{}_{}_10pc.png")   # cohort, chromosome, normtype
correlation_loci_plot_multi = os.path.join(correlation_loci_dir, "{}_chr{}.png")   # cohort, chromosome
correlation_loci_plot_10pc_multi = os.path.join(correlation_loci_dir, "{}_chr{}_10pc.png")   # cohort, chromosome, normtype
correlation_loci_heatmap = os.path.join(correlation_loci_dir, "heatmap_{}_chr{}_{}_{}.png")   # cohort, chromosome, normtype, corr_type

# subsampling
subsample_betas = os.path.join(processed_dir, "{}/subsamples/{}_chromosome_{}_betas_{}_{}.csv")  # cohort, subsample_size, chromosome, norm_type, iteration)
subsample_compare = os.path.join(reports_dir, "subsample_comparison_{}_{}_{}.json")  # cohort, chromosome, norm_type

# monte carlo
montecarlo_corr_baseline_pkl = os.path.join(simulation_dir, "montecarlo/mc_{}_{}_{}_{}_{}_baseline.pkl")  # test_run, corrtype, cohort, chromosome, norm_type
montecarlo_corr_pkl = os.path.join(simulation_dir, "montecarlo/mc_{}_{}_{}_{}_{}.pkl") # test_run, corrtype, cohort, chromosome, iteration
montecarlo_corr_pkl_two_norms = os.path.join(simulation_dir, "montecarlo/mc_{}_{}_{}_{}_{}_{}_{}.pkl") # test_run, corrtype, cohort, chromosome, norm_1, norm_2, iteration
montecarlo_corr_results = os.path.join(simulation_dir, "montecarlo_results/results_{}_{}_{}_{}.csv") # test_run, corrtype, cohort, chromosome
montecarlo_corr_combination_comparison_results = os.path.join(simulation_dir, "montecarlo/results_{}_{}_{}_{}_{}_{}.csv") # test_run, norm_1, norm_2, corrtype, cohort, chromosome

# loci distance
loci_distance_dir = os.path.join(reports_dir, "loci_distance/")
loci_distance_csv_ds = os.path.join(loci_distance_dir, "{}_{}_{}_{}.csv")  # cohort, corr_type, chromosome, downsample_ratio
loci_distance_json = os.path.join(loci_distance_dir, "{}/{}_{}_{}.json")  # cohort, chromosome, norm_type, corr_type
loci_distance_json_intragene = os.path.join(loci_distance_dir, "{}/intragene_{}_{}_{}.json")  # cohort, chromosome, norm_type, corr_type
loci_distance_csv = os.path.join(loci_distance_dir, "{}/{}_{}_{}_{}.csv")  # cohort, chromosome, norm_type, corr_type, positive/negative
loci_distance_csv_intragene = os.path.join(loci_distance_dir, "{}/intragene_{}_{}_{}_{}.csv")  # cohort, chromosome, norm_type, corr_type, positive/negative
loci_distance_json_ds = os.path.join(loci_distance_dir, "{}/{}_{}_{}_ds{}.json")  # cohort, chromosome, norm_type, corr_type, downsample
loci_distance_plot = os.path.join(loci_distance_dir, "{}/{}_{}_{}.png")  # cohort, chromosome, norm_type, corr_type
loci_distance_plot_intragene = os.path.join(loci_distance_dir, "{}/intragene_{}_{}_{}.png")  # cohort, chromosome, norm_type, corr_type
loci_distance_plot_ds = os.path.join(loci_distance_dir, "{}/{}_{}_{}_ds{}.png")  # cohort, chromosome, norm_type, corr_type, downsample

# correlations on same gene
same_gene_correlation_dir = os.path.join(reports_dir, "intragenic_correlation/")
same_gene_correlation_report = os.path.join(same_gene_correlation_dir, "{}_{}_{}_{}_{}_{}.csv")  # cohort, chromosome, norm_type, corr_type, direction, meta (threshold or percentage)
same_gene_correlation_report_by_gene = os.path.join(same_gene_correlation_dir, "{}_{}_{}_{}_{}_{}_by_gene.csv")  # cohort, chromosome, norm_type, corr_type, direction, meta (threshold or percentage)
same_gene_correlation_report_all_genes_for_chromosome = os.path.join(same_gene_correlation_dir, "intragene_{}_{}_{}_{}.csv")  # cohort, chromosome, norm_type, corr_type
same_gene_correlation_histogram = os.path.join(visualisation_dir, "intragene_histograms/{}_{}_{}_{}.png")  # cohort, chromosome, norm_type, corr_type
same_gene_correlation_histogram_no_island = os.path.join(visualisation_dir, "intragene_histograms/{}_{}_{}_{}_no_island.png")  # cohort, chromosome, norm_type, corr_type


# correlation networks within pathways
correlations_same_pathway_dir = os.path.join(reports_dir, "pathway_associations/")
correlations_same_pathway_report = os.path.join(correlations_same_pathway_dir, "{}_{}_{}_{}.json")  # cohort, chromosome, norm_type, corr_type
correlations_same_pathway_overall_report = os.path.join(correlations_same_pathway_dir, "{}_{}_{}_{}_overall.json")  # cohort, chromosome, norm_type, corr_type
correlations_same_pathway_strong_report = os.path.join(correlations_same_pathway_dir, "{}_{}_{}_{}_strong.json")  # cohort, chromosome, norm_type, corr_type
correlations_same_pathway_strong_overall_report = os.path.join(correlations_same_pathway_dir, "{}_{}_{}_{}_strong_overall.json")  # cohort, chromosome, norm_type, corr_type

# island_correlations
island_correlations_json = os.path.join(reports_dir, "islands_{}_{}_{}_{}.json")  # cohort, chromosome, norm_type, corr_type
