'''
Takes a list of pairs (e.g. cpg sites, as tuples) and produces lists of variable length where there is some association
'''

def condense(list_of_tuples):
    current_tag = 0
    tag = dict()
    for x in list_of_tuples:
        if x[0] in tag.keys() and x[1] not in tag.keys():  # assign x[1]
            tag[x[1]] = tag[x[0]]
            # print(f"Assigned {x[1]} to {tag[x[0]]}")
        elif x[1] in tag.keys() and x[0] not in tag.keys():  # assign x[0]
            tag[x[0]] = tag[x[1]]
            # print(f"Assigned {x[0]} to {tag[x[1]]}")
        elif x[0] in tag.keys() and x[1] in tag.keys():  # combine tags
            tag1 = tag[x[0]]
            tag2 = tag[x[1]]
            for k in tag.keys():
                if tag[k] == tag2:
                    tag[k] = tag1
        else:
            tag[x[0]] = current_tag
            tag[x[1]] = current_tag
            # print(f"Assigned {x[0]} and {x[1]} ({current_tag})")
            current_tag += 1

    condensed = {x: list() for x in range(current_tag)}
    for k, v in tag.items():
        condensed[v].append(k)

    return [x for x in condensed.values() if len(x)]
