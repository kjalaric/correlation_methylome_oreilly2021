import pandas as pd
from common.gene_class import *
import common.file_format

# manifest = pd.read_csv("K:/infinium-methylationepic-v-1-0-b5-manifest-file.csv", header=7, index_col="IlmnID")
# import pickle
# with open("K:/cohort1/pkl/850kmanifest.pkl", "wb") as fo:
#     pickle.dump(manifest, fo)

import pickle
with open(common.file_format.pkl_manifest_850k, "rb") as fi:
    manifest = pickle.load(fi)


def getGene(cpg, throwException=False):
    try:
        gene = manifest.loc[cpg]
    except:
        if not throwException:
            print(f"Couldn't find {cpg} in manifest...")
            return Gene(None, None, cpg, cpg)
        else:
            raise Exception()
    try:
        name = gene['UCSC_RefGene_Name']
    except:
        name = cpg
    return Gene(gene['CHR'], gene['MAPINFO'], name, cpg, gene['UCSC_CpG_Islands_Name'])


def getLocus(cpg, throwException=False):
    try:
        gene = manifest.loc[cpg]
    except:
        print(f"Couldn't find {cpg} in manifest...")
        if not throwException:
            return None, None
        else:
            raise Exception()
    return gene["MAPINFO"]


def getChromosomeAndLocus(cpg, throwException=False):
    try:
        gene = manifest.loc[cpg]
    except:
        print(f"Couldn't find {cpg} in manifest...")
        if not throwException:
            return None, None
        else:
            raise Exception()
    return gene["CHR"], gene["MAPINFO"]


if __name__ == "__main__":
    print(getChromosomeAndLocus("cg27659677"))


    if (0):
        for x in [
            "PCP4",
            "MIR4327",
        ]:
            print(manifest.loc[manifest["UCSC_RefGene_Name"] == x])

