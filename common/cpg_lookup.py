# correlation_oldformat files store indicies of cpgs rather than the cpgs themselves
# until this changes, this file can be used to get the cpg name
import common.file_format


def getLookupDict(array_type, chromosome, as_int=False):
    with open(common.file_format.cpg_sitecount.format(array_type, chromosome.upper()), "r") as fi:
        cpgs = fi.readlines()
    if not as_int:
        return {str(i): v.strip() for i, v in enumerate(cpgs)}  # index is a string to make it easier to do direct comparisons
    else:
        return {i: v.strip() for i, v in enumerate(cpgs)}  # index is a string to make it easier to do direct comparisons


def getArraySites(array_type, chromosome):
    with open(common.file_format.cpg_sitecount.format(array_type, chromosome.upper()), "r") as fi:
        sites = [x.strip() for x in fi.readlines()]
    return sites

