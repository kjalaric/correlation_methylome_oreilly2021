import common.file_format


def formatBetaCsv(cohort, norm_type):
    return common.file_format.betacsv_all.format(cohort, norm_type)


def formatBetaCsvPerChromosome(cohort, chromosome, norm_type):
    return common.file_format.betacsv_per_chromosome.format(cohort, chromosome, norm_type)


def formatBetaCsvAutosomal(cohort, norm_type):
    return common.file_format.betacsv_autosomal.format(cohort, norm_type)


def formatCorrPkl(cohort, chromosome, norm_type, corr_type="spearman"):
    return common.file_format.correlation_pkl_corrtype.format(cohort, corr_type, chromosome, norm_type)


def formatSitecount(array, chromosome):
    return common.file_format.cpg_sitecount.format(array, chromosome)


def formatStrongCorr(cohort, chromosome, norm_type, corr_type, direction, number_of_strongest=None, subsample_size=None, subsample_iteration=None):
    if subsample_size and number_of_strongest:
        raise Exception("File for subsampling and strongest_n not implemented yet")
    if subsample_size and not subsample_iteration:
        raise Exception("subsample_iteration invalid, needs to be something.")

    if not number_of_strongest:
        if subsample_size:
            return common.file_format.correlation_subsample.format(cohort, corr_type, "strong", direction, chromosome, norm_type, subsample_size, subsample_iteration)
        else:
            return common.file_format.correlation.format(cohort, corr_type, "strong", direction, chromosome, norm_type)
    else:
        return common.file_format.correlation_strongest_set.format(cohort, corr_type, number_of_strongest, "strong", direction, chromosome, norm_type)


def formatStrongCorrPercentage(cohort, chromosome, norm_type, corr_type, direction, percentage):
    return common.file_format.correlation_strongest_percentage.format(cohort, corr_type, percentage, direction, chromosome, norm_type)


# statistical reports for correlations
def formatBetaCorrelationStatCsv(cohort, chromosome, norm_type, corrtype, meta=None):
    if meta is None:
        return common.file_format.stats_for_correlation_df_data.format(cohort, cohort, chromosome, norm_type, corrtype)
    else:
        return common.file_format.stats_for_correlation_df_data_meta.format(cohort, cohort, chromosome, norm_type, corrtype, meta)


def formatBetaCorrelationStatReport(cohort, chromosome, norm_type, corrtype, meta=None):
    if meta is None:
        return common.file_format.stats_for_correlation_df.format(cohort, cohort, chromosome.lower(), norm_type, corrtype)
    else:
        return common.file_format.stats_for_correlation_df_meta.format(cohort, cohort, chromosome.lower(), norm_type, corrtype, meta)


# correlation networks
def format10pcCorrelationNetwork(cohort, chromosome, norm_type, corr_type="spearman", negatives_only=False, meta=None):
    norm_and_metadata = norm_type
    if meta is not None:
        norm_and_metadata += f"_{meta}"

    if negatives_only:
        return common.file_format.correlation_network_10pc_negatives.format(cohort, corr_type, chromosome, norm_and_metadata)
    else:
        return common.file_format.correlation_network_10pc.format(cohort, corr_type, chromosome, norm_and_metadata)


def format10pcCorrelationNetworkJson(cohort, chromosome, norm_type, corr_type, negatives_only=False, meta=None):
    norm_and_metadata = norm_type
    if meta is not None:
        norm_and_metadata += f"_{meta}"

    if negatives_only:
        return common.file_format.correlation_network_10pc_negatives_json.format(cohort, corr_type, chromosome, norm_and_metadata)
    else:
        return common.file_format.correlation_network_10pc_json.format(cohort, corr_type, chromosome, norm_and_metadata)
