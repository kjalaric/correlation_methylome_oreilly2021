class Gene(object):
    def __init__(self, chromosome, mapinfo, ucsc_refgene_name, cpg, island_name=None):
        self.chromosome = chromosome
        try:
            self.mapinfo = int(mapinfo)
        except:
            self.mapinfo = None
        self.cpg = cpg
        self.island_name = island_name

        self.ucsc_refgene = str(ucsc_refgene_name)
        if isinstance(ucsc_refgene_name, float) and self.ucsc_refgene == "nan":
            self.ucsc_refgene = cpg
            self.name = cpg
            self.alt_names = list()
        else:
            self.name = str(ucsc_refgene_name).split(';')[0]
            self.alt_names = str(ucsc_refgene_name).split(';')