"""
this is a very quick and dirty way of checking to see if a string is a cpg identifier
"""

def isCpg(candidate):
    # returns true if it is a cpg
    try:
        splitline = candidate.split("cg")  # will provide something like ['', '12345678']
        if not len(splitline) == 2:
            return False
        if not len(splitline[1]) == 8:
            return False
        if len(splitline[0]):
            return False
        int(splitline[1])  # will raise an error if it can't be cast
    except Exception as e:
        return False
    return True


if __name__ == "__main__":
    # unit test
    cg = "cg00050873"

    nots = [
        "BRCAM1",
        "BRcgCAM2",
        "cghej",
        "cgabcdefgh",
        "0554cg46684",
        "acg01234567"
    ]

    assert isCpg(cg)
    for x in nots:
        assert not isCpg(x), x