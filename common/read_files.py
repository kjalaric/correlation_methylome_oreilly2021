import common.defines
import common.file_format
from common.format_functions import formatStrongCorrPercentage


def get10pcStrongCorrelationsFromFile(cohort, chromosome, norm_type, corr_type="spearman"):
    positive_correlations = dict()
    negative_correlations = dict()
    with open(formatStrongCorrPercentage(cohort, chromosome, norm_type, corr_type, "positive", 10), "r") as fi:
        for fline in fi.readlines():
            row = fline.strip().split(",")
            positive_correlations[(row[0], row[1])] = float(row[2])

    with open(formatStrongCorrPercentage(cohort, chromosome, norm_type, corr_type, "negative", 10), "r") as fi:
        for fline in fi.readlines():
            row = fline.strip().split(",")
            negative_correlations[(row[0], row[1])] = float(row[2])

    return positive_correlations, negative_correlations


def getCorrelationsFromFile(cohort, chromosome, norm_type, strength="strong", meta=None, overlap=False):
    positive_correlations = dict()
    negative_correlations = dict()

    if not overlap:
        if meta is None:
            positive_correlation_file_location = common.file_format.correlation_oldformat.format(cohort, strength, "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation_oldformat.format(cohort, strength, "negative", chromosome, norm_type)
        else:
            positive_correlation_file_location = common.file_format.correlation_meta.format(cohort, strength, "positive", chromosome, norm_type, meta)
            negative_correlation_file_location = common.file_format.correlation_meta.format(cohort, strength, "negative", chromosome, norm_type, meta)
    else:
        if meta is None:
            positive_correlation_file_location = common.file_format.correlation_overlap.format(cohort, strength, "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation_overlap.format(cohort, strength, "negative", chromosome, norm_type)
        else:
            positive_correlation_file_location = common.file_format.correlation_overlap_meta.format(cohort, strength, "positive", chromosome, norm_type, meta)
            negative_correlation_file_location = common.file_format.correlation_overlap_meta.format(cohort, strength, "negative", chromosome, norm_type, meta)

    with open(positive_correlation_file_location, "r") as fi:
        for fline in fi.readlines():
            row = fline.strip().split(",")
            positive_correlations[(row[0], row[1])] = float(row[2])

    with open(negative_correlation_file_location, "r") as fi:
        for fline in fi.readlines():
            row = fline.strip().split(",")
            negative_correlations[(row[0], row[1])] = float(row[2])

    return positive_correlations, negative_correlations


def getNormCorrelationsFromFiles(cohort, chromosome, norm_type_list=common.defines.base_norm_types, overlap=False, corrtype="spearman"):
    """
    Returns dictionaries
    :param cohort:
    :param chromosome:
    :param norm_type_list: norm types of interest, contained within a list
    :param overlap:
    :param corrtype:
    :return: two dicts (positive and negative correlations) of format {norm_type: c...} where c is a dict of format {(cpg1, cpg2): strength...}
    """
    positive_norm_correlations = {x: dict() for x in norm_type_list}  # dicts are (x, y): corr
    negative_norm_correlations = {x: dict() for x in norm_type_list}

    for norm_type in norm_type_list:
        if not overlap:
            positive_correlation_file_location = common.file_format.correlation.format(cohort, corrtype, "strong", "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation.format(cohort, corrtype, "strong", "negative", chromosome, norm_type)
        else:
            positive_correlation_file_location = common.file_format.correlation_overlap.format(cohort, corrtype, "positive", chromosome, norm_type)
            negative_correlation_file_location = common.file_format.correlation_overlap.format(cohort, corrtype, "negative", chromosome, norm_type)

        with open(positive_correlation_file_location, "r") as fi:
            for fline in fi.readlines():
                row = fline.strip().split(",")
                positive_norm_correlations[norm_type][(row[0], row[1])] = float(row[2])

        with open(negative_correlation_file_location, "r") as fi:
            for fline in fi.readlines():
                row = fline.strip().split(",")
                negative_norm_correlations[norm_type][(row[0], row[1])] = float(row[2])

    return positive_norm_correlations, negative_norm_correlations

