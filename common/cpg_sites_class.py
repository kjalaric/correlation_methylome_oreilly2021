class GeneCpgSites(object):
    def __init__(self, gene_name, site_and_locus_tuples):
        self.name = gene_name
        self.cpg_loci = sorted(site_and_locus_tuples, key=lambda x: x[1])


if __name__ == "__main__":
    # unit test
    gene_name = "TEST"
    site_and_locus_tuples = [
        ("cg32345678", 1442556),
        ("cg22345678", 1442538),
        ("cg12345678", 420),
        ("cg42345678", 2442536)
    ]
    gene_with_cpgs = GeneCpgSites(gene_name, site_and_locus_tuples)
    print(gene_with_cpgs.cpg_loci)
