# correlation_methylome_boreilly2021

Scripts used for my master's thesis: Patterns of correlation in the human methylome (Brook O'Reilly, 2021)


# documentation

I've either documented as I've gone along, discussed how code works in the script itself, or described things in appendix D of my thesis (as appropriate). If something isn't clear then I apologise! I either didn't think it needed explaining or was too distracted to document my code. 


# licensing

See the relevant file in this repo. Should be MIT license.
