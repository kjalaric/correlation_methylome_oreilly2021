from common.gene_class import *

# MANIFEST = "850k"  # 850k, 450k

_pam_epic_imported = False
_pam_450k_imported = False

def _importEpic(verbose=True):
    global _pam_epic_imported, pam
    if not _pam_epic_imported:
        if verbose:
            print("Importing EPIC array manifest...")
        import common.parse_epicarray_manifest as pam
        _pam_epic_imported = True


def _import450k(verbose=True):
    global _pam_450k_imported, pam450k
    if not _pam_450k_imported:
        if verbose:
            print("Importing 450k array manifest...")
        import common.parse_450k_manifest as pam450k
        _pam_450k_imported = True


class DifferentChromosomeException(Exception):
    def __init__(self, chr1, chr2):
        self.message = "Loci were not on same chromosome (got {} and {})".format(chr1, chr2)
        super().__init__(self.message)


class MissingLociException(Exception):
    def __init__(self):
        super().__init__("Required positional information was not available.")


def distanceBetweenCpgSites(cpg1, cpg2, array="EPIC"):
    if array == "EPIC":
        _importEpic()
        c1, l1 = pam.getChromosomeAndLocus(cpg1)
        c2, l2 = pam.getChromosomeAndLocus(cpg2)
    else:
        _import450k()
        c1, l1 = pam450k.getChromosomeAndLocus(cpg1)
        c2, l2 = pam450k.getChromosomeAndLocus(cpg2)
    if l1 is None or l2 is None:
        raise MissingLociException()
    elif c1 != c2:
        raise DifferentChromosomeException(c1, c2)
    else:
        return abs(l1 - l2)


if __name__ == "__main__":
    # unit test: cg05173528 is "SVOPL", cg12409226 is "TSC22D4" and is 38235901 bp earlier on chromosome 7
    _importEpic()
    assert(pam.getGene("cg05173528").name == "SVOPL")
    assert(pam.getGene("cg12409226").name == "TSC22D4")
    assert(pam.getGene("cg05173528").mapinfo - pam.getGene("cg12409226").mapinfo == 38235901)

    assert(distanceBetweenCpgSites("cg05173528", "cg12409226") == 38235901)
