import common.parse_epicarray_manifest as pam



def getCpgName(number, namelist):
    return namelist[number]

def getCpgGene(number, namelist):
    cpgname = getCpgName(number, namelist)
    return pam.getGene(cpgname)

def getGeneName(number):
    return getCpgGene(number).name


# unit test
if __name__ == "__main__":
    for x in ["cg05380455" , "cg09889997" , "cg03161767" , "cg00666247"  ,"cg19443705"]:
        print(pam.getGene(x).name, pam.getGene(x).chromosome)