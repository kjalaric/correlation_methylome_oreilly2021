'''
Draw pretty diagrams showing links between chromosomes
'''

import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.colors
import pickle as pk
from gene_class import *

plot_options = {
    'node_color': 'magenta',
    'node_size': 10,
}
# plot_options_no_edgecolour = {k: v for k, v in plot_options.items() if k is not "edge_color"}

NORMAL_WEIGHT = 1
THICC_WEIGHT = 3

gene_list = set()  # this set contains the Gene objects produced by PAM (used when sorting by chromosomes)

chromosome_size = {  # https://en.wikipedia.org/wiki/Human_genome
    1	:	248956422	,
    2	:	242193529	,
    3	:	198295559	,
    4	:	190214555	,
    5	:	181538259	,
    6	:	170805979	,
    7	:	159345973	,
    8	:	145138636	,
    9	:	138394717	,
    10	:	133797422	,
    11	:	135086622	,
    12	:	133275309	,
    13	:	114364328	,
    14	:	107043718	,
    15	:	101991189	,
    16	:	90338345	,
    17	:	83257441	,
    18	:	80373285	,
    19	:	58617616	,
    20	:	64444167	,
    21	:	46709983	,
    22	:	50818468	,
    'x'	:	156040895	,
    'y'	:	57227415	,
}

chromosome_size_cumulative = chromosome_size.copy()
chromosome_size_cumulative[1] = 0
chr_total = 0
for k in range(1,23):
    chromosome_size_cumulative[k] += chr_total
    chr_total += chromosome_size[k]
for k in ['x', 'y']:
    chromosome_size_cumulative[k] += chr_total
    chr_total += chromosome_size[k]



def constructGraph(connections):
    # automatically adds cpgs to the graph according to connections which are provided as 2-tuples
    G = nx.Graph()

    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    G.add_edges_from(connections)

    return G


def constructGraphWithChromosomes(connections):
    '''
    Edges in the graph are colour coded as follows:
        > green: same chromosome, positive correlation_oldformat
        > cyan: different chromosome, positive correlation_oldformat
        > red: same chromosome, negative correlation_oldformat
        > orange: different chromosome, negative correlation_oldformat
    Lines are thicker if there are multiple correlated CpG sites on the two genes.
    If there is conflicting correlation_oldformat (i.e. there are at least two pairs of
    correlated CpGs on two genes, and they're not all positive or all negative correlations) then the line is black.
    '''

    # automatically adds cpgs to the graph according to connectionm which are provided as 3-tuples along with chromosome
    G = nx.Graph()

    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    for connection in connections:
        # already exists: make the line thicker, update colour if connection[2] is true (otherwise leave it)
        if G.has_edge(connection[0], connection[1]):
            G[connection[0]][connection[1]]['weight'] = THICC_WEIGHT
            # print(f"THICC detected, this hasn't happened before: {connection[0]}, {connection[1]}")
            if connection[2]:
                G[connection[0]][connection[1]]['edge_color'] = 'green'
        else:
            if connection[2]:
                G.add_edge(connection[0], connection[1], edge_color='green', weight=NORMAL_WEIGHT)
            else:
                G.add_edge(connection[0], connection[1], edge_color='cyan', weight=NORMAL_WEIGHT)

    return G


def addNegativeCorrelationsToGraph(G, connections):
    '''
    Edges in the graph are colour coded as follows:
        > green: same chromosome, positive correlation_oldformat
        > cyan: different chromosome, positive correlation_oldformat
        > red: same chromosome, negative correlation_oldformat
        > orange: different chromosome, negative correlation_oldformat
    Lines are thicker if there are multiple correlated CpG sites on the two genes.
    If there is conflicting correlation_oldformat (i.e. there are at least two pairs of
    correlated CpGs on two genes, and they're not all positive or all negative correlations) then the line is black.
    '''

    # automatically adds cpgs to the graph according to connectionm which are provided as 3-tuples along with chromosome
    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    for connection in connections:
        # already exists: make the line thicker, update colour if connection[2] is true (otherwise leave it)
        # UNLESS colour is green, cyan or black - in which case the colour needs to be set to black
        if G.has_edge(connection[0], connection[1]):
            G[connection[0]][connection[1]]['weight'] = THICC_WEIGHT

            # conflicting interactions
            if (G[connection[0]][connection[1]]['edge_color'] == 'green') or (G[connection[0]][connection[1]]['edge_color'] == 'cyan'):
                G[connection[0]][connection[1]]['edge_color'] = 'black'
                print("Conflicting interaction: {} - {}".format(connection[0], connection[1]))
            elif G[connection[0]][connection[1]]['edge_color'] == 'black':
                pass
            elif connection[2]:
                G[connection[0]][connection[1]]['edge_color'] = 'red'
        else:
            if connection[2]:
                G.add_edge(connection[0], connection[1], edge_color='red', weight=NORMAL_WEIGHT)
            else:
                G.add_edge(connection[0], connection[1], edge_color='orange', weight=NORMAL_WEIGHT)

    return G


def pruneExceptFor(G, prunelist):
    new_graph = nx.Graph()

    for find_me in prunelist:
        new_graph.add_node(find_me)
        neighbours = set()
        converged = False
        neighbourhood = G.neighbors(find_me)
        while not converged:
            converged = True
            for n in neighbourhood:
                neighbours.add(n)
            for n in neighbours.copy():
                neighbourhood = G.neighbors(n)
                for nn in neighbourhood:
                    if nn not in neighbours:
                        converged = False
                        neighbours.add(nn)

        for n in neighbours:
            new_graph.add_node(n)
            for edge in G.edges(n):
                new_graph.add_edge(edge[0], edge[1])
                try:
                    new_graph[edge[0]][edge[1]]['edge_color'] = G[edge[0]][edge[1]]['edge_color']
                except:
                    new_graph[edge[0]][edge[1]]['edge_color'] = 'blue'
                try:
                    new_graph[edge[0]][edge[1]]['weight'] = G[n][edge]['weight']
                except:
                    new_graph[edge[0]][edge[1]]['weight'] = NORMAL_WEIGHT

    return new_graph




def graphStats(G):
    print(f"{G.number_of_nodes()} nodes\n{G.number_of_edges()} edges")


def plotGraph(G, minimum_children=0, exclusion_list=[], labels=True, chromosomes=False, circularise=True):
    if not len(exclusion_list) == 0:
        G.remove_nodes_from(exclusion_list)
        isolate_nodes = list(nx.isolates(G))
        G.remove_nodes_from(isolate_nodes)
        print("Excluding the following nodes: {}\nRemoving resulting isolate nodes: {}".format(exclusion_list, isolate_nodes))

    if minimum_children:
        # can't just delete them outright as that'd make more isolate nodes than there needs to be
        removal_list = list()
        for node in list(G.nodes):
            if len(list(G.neighbors(node))) < minimum_children:
                removal_list.append(node)
        G.remove_nodes_from(removal_list)
        isolate_nodes = list(nx.isolates(G))
        G.remove_nodes_from(isolate_nodes)


    edges = G.edges()
    colors = [G[u][v]['edge_color'] for u, v in edges]
    colors = [matplotlib.colors.to_rgb(x) + (0.3,) for x in colors]  # easier on the eyes
    weights = [G[u][v]['weight'] for u, v in edges]
    if not circularise:
        nx.draw(G, edges=edges, edge_color=colors, with_labels=labels, **plot_options)  # , width=weights
    else:
        circle_pos = nx.circular_layout(G)
        if chromosomes:
            # arrange on circle based on chromosome and position of locus
            new_circle_layout = [[x, 0] for x in circle_pos.keys()]
            for gene_position in new_circle_layout:
                current_gene = [x for x in gene_list if x.name == gene_position[0]][0]
                gene_position[1] = int(current_gene.mapinfo) + chromosome_size_cumulative[int(current_gene.chromosome)]
            new_circle_layout.sort(key = lambda x: x[1])  # sort by chromosomes
            print(new_circle_layout)

            new_circle_pos = dict()
            circleposval = list(circle_pos.values())
            for i, v in enumerate(new_circle_layout):
                new_circle_pos[v[0]] = circleposval[i]

            circle_pos = new_circle_pos

        # colors = [matplotlib.colors.to_rgb(x) + (0.3,) for x in colors]
        nx.draw(G, edges=edges, edge_color=colors, pos=circle_pos, with_labels=labels, **plot_options)  # , width=weights


    plt.show()


def rankByNeighbours(G):
    # returns a dictionary of nodes sorted by the number of neighbours
    neighbours = dict([(x, len(G.adj[x])) for x in G])
    return sorted(neighbours.items(), key=lambda x:x[1], reverse=True)



def saveGraph(G, filename):
    pk.dump(G, filename)


def loadGraph(filename):
    return pk.load(filename)


if __name__ == "__main__":
    connections = [
        ("cg12543216", "cg125468779"),
        ("cg12543216", "cg13644587"),
        ("cg125468779", "cg35426128"),
        ("cg125468779", "cg13644587"),
        ("cg35426128", "cg13644587"),
        ("cg45698743", "cg13644587"),
    ]

    G = constructGraph(connections)
    graphStats(G)

    print(list(G.adj["cg45698743"]))  # alt. G.neighbours(1)
    print(G.degree["cg12543216"])

    rankByNeighbours(G)
    # plotGraph(G)

