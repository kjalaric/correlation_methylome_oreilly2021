#todo this is so janky, needs a re-write

import pandas as pd
import networkx as nx
import correlation_compare.chromosome_networks as chrnet
import correlation_compare.cpg_number_conversion as cnc
import common.parse_epicarray_manifest as pam
import common.file_format
import internet_search.wikisearch as searcher

CHROMOSOME = '21'
norm_type = "noob"

names = list(pd.read_csv(f"K:/processed/chds/chromosome_{CHROMOSOME}_betas_{norm_type}.csv", index_col=0).index)
print(len(names))

def formatForGraphWithChromosomes(connections, names, cpgs_as_names=False,
                                  internal_connections=True, external_connections=True):
    updated_connections = [(cnc.getCpgGene(x[0], names), cnc.getCpgGene(x[1], names)) for x in connections]
    if internal_connections and not external_connections:
        updated_connections = [x for x in updated_connections if x[0].chromosome == x[1].chromosome]
    elif external_connections and not internal_connections:
        updated_connections = [x for x in updated_connections if x[0].chromosome != x[1].chromosome]

    for x in updated_connections:
        chrnet.gene_list.add(x[0])
        chrnet.gene_list.add(x[1])
    if not cpgs_as_names:
        return [(x[0].name, x[1].name, x[0].chromosome == x[1].chromosome) for x in updated_connections]
    else:
        return [(x[0].cpg, x[1].cpg, x[0].chromosome == x[1].chromosome) for x in updated_connections]


def load_connections(file_location, minimum_correlation=0.88):
    connections = list()
    with open(file_location, "r") as fi:
        for line in fi.readlines():
            x = line.strip().split(",")
            if abs(float(x[2])) >= minimum_correlation:
                connections.append((x[0], x[1], float(x[2])))
    return connections


def run(minimum_children=0, exclusion_list=[], find=[], show_labels = True, use_cpgs_as_labels=False,
        show_positives=True, show_negatives=True,
        show_internal_links=True, show_external_links=True,
        show_graph_stats=True, rank_by_neighbours=True,
        threshold=0.7, plot=True,
        type=None, chromosome=None):

    if type or chromosome:
        global names, CHROMOSOME
        names = list(pd.read_csv(f"K:/processed/chds/chromosome_{chromosome}_betas_{type}.csv", index_col=0).index)
        CHROMOSOME = chromosome

    connections = load_connections(f"K:/processed/chds/strongpositive_chr{CHROMOSOME}_{norm_type}.txt", threshold)
    negative_connections = load_connections(f"K:/processed/chds/strongnegative_chr{CHROMOSOME}_{norm_type}.txt", threshold)



    connections_genes = [(pam.getGene(names[int(x[0])]), pam.getGene(names[int(x[1])])) for x in connections]
    negative_connections_genes = [(pam.getGene(names[int(x[0])]), pam.getGene(names[int(x[1])])) for x in negative_connections]

    connections = [(x[0].name, x[1].name, True) for x in connections_genes]
    negative_connections = [(x[0].name, x[1].name, True) for x in negative_connections_genes]

    if show_positives:
        G = chrnet.constructGraphWithChromosomes(connections)
    else:
        G = nx.Graph()

    if show_negatives:
        G = chrnet.addNegativeCorrelationsToGraph(G, negative_connections)

    if len(find):
        G = chrnet.pruneExceptFor(G, find)

    if show_graph_stats: chrnet.graphStats(G)
    if rank_by_neighbours:
        ranked_neighbours = chrnet.rankByNeighbours(G)
        print(ranked_neighbours)

    '''
    shortest_path_from = "MTOR"
    shortest_path_to = "RPTOR"
    shortest_path = nx.shortest_path(G, source=shortest_path_from, target=shortest_path_to)
    print(f"shortest path {shortest_path_from} - {shortest_path_to}: {shortest_path}")
    '''

    ''' CHR23 downs syndrome genes
    print(list(G.neighbors('DSCAM')))
    print(list(G.neighbors('DOPEY2')))
    print(set(G.neighbors('DSCAM')).intersection('DOPEY2'))
    '''

    if plot:
        chrnet.plotGraph(G, minimum_children=minimum_children, exclusion_list=exclusion_list,
            labels=show_labels, chromosomes=True, circularise=False)  # exclusion_list=['cg06743060']

    if rank_by_neighbours:
        return ranked_neighbours


if __name__ == "__main__":
    threshold = 0.7
    ranked_neighbours = run(show_external_links=False, show_positives=True, show_labels=True,
        threshold=threshold, rank_by_neighbours=True)

    with open(f"K:/reports/chds/ranked_neighbours_with_descriptions_chr{CHROMOSOME}_{norm_type}.txt", "w", encoding="utf-8") as fo:
        for x in ranked_neighbours:
            searched_gene = searcher.searchForGene(x[0])
            fo.write(f"{x[0]} ({searched_gene[0]}) - {x[1]}\n")
            fo.write(searched_gene[1])
            fo.write("\n\n")
