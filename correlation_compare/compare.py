import os

pattern_positive = "strongpositive_chr{}_fixed2.txt"
pattern_negative = "strongnegative_chr{}_fixed2.txt"

overlap_output_positive = "K:/marmalaid/processed/overlap_with_cohort1_positive.txt"
overlap_output_negative = "K:/marmalaid/processed/overlap_with_cohort1_negative.txt"


if __name__ == "__main__":
    chromosome = 21
    dir1 = "K:/cohort1/"
    dir2 = "K:/marmalaid/processed/"

    pos1 = os.path.join(dir1, pattern_positive.format(chromosome))
    pos2 = os.path.join(dir2, pattern_positive.format(chromosome))
    neg1 = os.path.join(dir1, pattern_negative.format(chromosome))
    neg2 = os.path.join(dir2, pattern_negative.format(chromosome))

    pos1_correlations = list()
    with open(pos1, "r") as fi:
        for x in fi.readlines():
            pos1_correlations.append([y for y in x.split(",")[:-1]])  # if p values removed from correlation_oldformat list will need to remove newline from correlation_oldformat value instead

    pos2_correlations = list()
    with open(pos2, "r") as fi:
        for x in fi.readlines():
            pos2_correlations.append([y for y in x.split(",")[:-1]])

    neg1_correlations = list()
    with open(neg1, "r") as fi:
        for x in fi.readlines():
            neg1_correlations.append([y for y in x.split(",")[:-1]])

    neg2_correlations = list()
    with open(neg2, "r") as fi:
        for x in fi.readlines():
            neg2_correlations.append([y for y in x.split(",")[:-1]])

    pos1_pairs = set([(x[0], x[1]) for x in pos1_correlations])
    pos2_pairs = set([(x[0], x[1]) for x in pos2_correlations])
    neg1_pairs = set([(x[0], x[1]) for x in neg1_correlations])
    neg2_pairs = set([(x[0], x[1]) for x in neg2_correlations])

    # inefficient but i'm lazy
    # [cpg1, cpg2, correlation1, correlation2]
    positive_overlaps = list()
    for x in pos1_pairs:
        if x in pos2_pairs:
            positive_overlaps.append(x)

    print(f"Overlapping positive correlations: {len(positive_overlaps)}")
    print(f"\t{100.0*len(positive_overlaps)/len(pos1_pairs)}% of file 1")
    print(f"\t{100.0*len(positive_overlaps)/len(pos2_pairs)}% of file 2")

    negative_overlaps = list()
    for x in neg1_pairs:
        if x in neg2_pairs:
            negative_overlaps.append(x)

    print(f"Overlapping negative correlations: {len(negative_overlaps)}")
    print(f"\t{100.0*len(negative_overlaps)/len(neg1_pairs)}% of file 1")
    print(f"\t{100.0*len(negative_overlaps)/len(neg2_pairs)}% of file 2")

    # pos1_cors_dict = {(x[0], x[1]) : x[2] for x in pos1_correlations}
    # pos2_cors_dict = {(x[0], x[1]) : x[2] for x in pos2_correlations}
    # neg1_cors_dict = {(x[0], x[1]) : x[2] for x in neg1_correlations}
    # neg2_cors_dict = {(x[0], x[1]) : x[2] for x in neg2_correlations}

    with open(overlap_output_positive, "w") as fo:
        for x in positive_overlaps:
            fo.write(f"{x[0]},{x[1]},0.95\n")

    with open(overlap_output_negative, "w") as fo:
        for x in negative_overlaps:
            fo.write(f"{x[0]},{x[1]},0.95\n")
