import itertools
import common.bio

array = {
    "chds": "EPIC",
    "450k_dataset_1": "450k",
    "marmalaid": "450k",
    "dbgap76983": "450k",
    "mtab7069": "EPIC",
    "metacohort_1": "450k",
    "gse43414": "450k"
}

cohort_size = {
    "chds": 120,
}

cohort_overhead_coefficient = {  # for array size estimation. there is a better way of doing it but this is faster for me now.
    "chds": 1.0084,
}

cohorts = [x for x in array.keys()]
arrays = [x for x in array.values()]

overlaps = [
    "chds-450k_dataset_1",
    "chds-450k_dataset_1-marmalaid",
]

limited_sets = {
    "marmalaid": "raw",
    "dbgap76983": "raw",
}

droplists = {
    "chds": None,
    "mtab7069": [  # umbilical cord blood
        "201465940018_R03C01",
        "201465940018_R08C01",
        "201465940036_R03C01",
        "201465940036_R08C01",
        "201869680139_R03C01",
        "201869680139_R08C01",
        "201870620104_R03C01",
        "201870620104_R08C01",
        "201876940020_R03C01",
        "201876940020_R08C01",
        "201876940062_R03C01"
    ]
}




def overlapName(_cohorts):
    return "+".join(_cohorts)


def cohortsInOverlap(overlap_name):
    return overlap_name.strip().split("+")


def possibleOverlaps():
    for x in range(2, len(cohorts) + 1):
        for combination in itertools.combinations(cohorts, x):
            yield combination


def getOverlapArray(_cohorts):
    _arrays = set()
    for x in _cohorts:
        _arrays.add(array[x])

    # use 450k by default unless cohorts are exclusively EPIC
    if not "450k" in _arrays:
        return "EPIC"
    else:
        return "450k"


def getArrayType(cohort, throw_exception=False):
    try:
        return array[cohort]
    except KeyError:
        if throw_exception:
            raise
        else:
            return getOverlapArray(cohortsInOverlap(cohort))


def estimateBetaSizeInMemory(cohort, chromosome):
    return (common.bio.epic_probe_count[chromosome] * cohort_size[cohort] * 8) * cohort_overhead_coefficient[cohort]  # 8: float64 size


def estimateBetaCorrSizeInMemory(chromosome):
    return (8 * common.bio.epic_probe_count[chromosome] ** 2) * 1.002  # 8: float64 size, 1.002 = chromosome-dependent overhead coefficient which im too lazy to define


def bytesToGigabytes(value_in_bytes):
    return value_in_bytes/1000000000


def gigabytesToBytes(value_in_gigabytes):
    return value_in_gigabytes*1000000000