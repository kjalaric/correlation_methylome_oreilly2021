import common.file_format

with open(common.file_format.unique_sites_per_array.format("450k"), "r") as fi:
    unique_sites_450k = list(fi.readlines())

with open(common.file_format.unique_sites_per_array.format("EPIC"), "r") as fi:
    unique_sites_epic = list(fi.readlines())