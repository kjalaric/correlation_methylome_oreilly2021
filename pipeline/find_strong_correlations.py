import time
import pandas as pd
import common.file_format
from common.format_functions import formatCorrPkl, formatStrongCorr
import common.bio
from datetime import datetime


def findStrongCorrelations(cohort, chromosome, norm_type, corr_type, subsample_size=None, iteration=None, threshold=0.7, dont_write=False, return_correlations=False, file_override=None):
    if file_override:
        df = pd.read_pickle(file_override)
    elif subsample_size:
        df = pd.read_pickle(common.file_format.correlation_pkl_corrtype_subsampled.format(cohort, corr_type, chromosome, norm_type, subsample_size, iteration))
    else:
        df = pd.read_pickle(common.file_format.correlation_pkl_corrtype.format(cohort, "spearman", chromosome, norm_type))

    # searches through the lower triangle
    index = 1
    ct = 1
    with open(common.file_format.correlation.format(cohort, corr_type, "strong", "positive", chromosome, norm_type), "w") as fo_p:
        with open(common.file_format.correlation.format(cohort, corr_type, "strong", "negative", chromosome, norm_type), "w") as fo_n:
            for cpg1, row in df.iterrows():
                for cpg2, x in row.iteritems():
                    if ct >= index:
                        break
                    ct += 1
                    if x >= threshold:
                        fo_p.write(f"{cpg1},{cpg2},{x}\n")
                    elif x <= -threshold:
                        fo_n.write(f"{cpg1},{cpg2},{x}\n")
                ct = 1
                index += 1


def findStrongest10PercentCorrelations(cohort, chromosome, norm_type, corr_type):
    number_of_strongest_correlations = int(common.bio.epic_probe_count[chromosome]*0.1)

    df = pd.read_pickle(common.file_format.correlation_pkl_corrtype.format(cohort, "spearman", chromosome, norm_type))
    df = df.unstack().sort_values(ascending=False)[df.shape[1]:]  # quicksort by default, mergesort is more stable. also remove the diagonal

    strong_positives_dict = dict()  # cant just take every second one in case some correlations are the same
    for i, x in df[:number_of_strongest_correlations*2].iteritems():
        strong_positives_dict[tuple(sorted(i))] = x

    with open(common.file_format.correlation_strongest_10percent.format(cohort, corr_type, "10", "positive", chromosome, norm_type), "w") as fo:  #
        for k, v in strong_positives_dict.items():
            fo.write(f"{k[0]},{k[1]},{v}\n")
    print("Wrote to ", common.file_format.correlation_strongest_10percent.format(cohort, corr_type, "10", "positive", chromosome, norm_type))

    del strong_positives_dict

    strong_negatives_dict = dict()
    for i, x in df[-number_of_strongest_correlations*2:].iloc[::-1].iteritems():  # want strongest first, so have to reverse the order of this one
        strong_negatives_dict[tuple(sorted(i))] = x

    with open(common.file_format.correlation_strongest_10percent.format(cohort, corr_type, "10", "negative", chromosome, norm_type), "w") as fo:  #
        for k, v in strong_negatives_dict.items():
            fo.write(f"{k[0]},{k[1]},{v}\n")
    print("Wrote to ", common.file_format.correlation_strongest_10percent.format(cohort, corr_type, "10", "negative", chromosome, norm_type))


if __name__ == "__main__":
    cohort = "chds"
    corr_type = "spearman"
    # norm_type = "swan"

    '''
    for chromosome in ["x", "y"]:  # should start at 16
        norm_type = "swan"
        print("starting", norm_type, chromosome)
        start_time = datetime.now()
        # findStrongCorrelations(cohort, chromosome, norm_type, corr_type)
        findStrongest10PercentCorrelations(cohort, chromosome, norm_type, corr_type)
        end_time = datetime.now()
        print("finished - time: ", end_time - start_time, "seconds\n")
    '''

    for norm_type in ["raw", "swan", "illumina", "funnorm", "quant"]:  # noob already done
        for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:  # should start at 16
            print("starting", norm_type, chromosome)
            start_time = datetime.now()
            # findStrongCorrelations(cohort, chromosome, norm_type, corr_type)
            findStrongest10PercentCorrelations(cohort, chromosome, norm_type, corr_type)
            end_time = datetime.now()
            print("finished - time: ", end_time - start_time, "seconds\n")
