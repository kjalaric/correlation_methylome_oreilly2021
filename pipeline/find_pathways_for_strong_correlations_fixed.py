import re
import json
import api.reactome as api
import common.file_format
import common.read_files
import common.parse_epicarray_manifest as pam  # assuming cpgs from 450k is the same in this one, but with more recent annotations
import common.parse_450k_manifest as pam450k  # assuming cpgs from 450k is the same in this one, but with more recent annotations

import common.set_combine
from common.check_cpg import isCpg


def cleanhtml(string):
    cleanr = re.compile('<.*?>')
    return re.sub(cleanr, '', string)


def condenseToGenelists(correlation_set):
    cpglists = common.set_combine.condense(correlation_set)
    cpglists = sorted(cpglists, key=lambda x: len(x), reverse=True)
    for id, cpglist in enumerate(cpglists):
        genelist = list()
        for cpg in cpglist:
            try:
                genelist.append(pam.getGene(cpg, throwException=True))
            except:
                try:
                    genelist.append(pam450k.getGene(cpg, throwException=True))
                except:
                    genelist.append(cpg)
        genelist = sorted(genelist, key=lambda x: x.name)
        yield genelist


def run(cohort, chromosome, norm_type):
    print("Running:", cohort, chromosome, norm_type)
    outfile = common.file_format.gene_association_report_strong_json.format(cohort, chromosome, norm_type)

    positive_correlations, negative_correlations = common.read_files.get10pcStrongCorrelationsFromFile(cohort, chromosome, norm_type)
    correlation_set = set()
    for x in positive_correlations.keys():
        correlation_set.add(x)
    for x in negative_correlations.keys():
        correlation_set.add(x)

    pathways = dict()
    pathway_id_to_name = dict()
    cpg_to_used_gene_name = dict()
    gene_name_to_id = dict()
    id_to_exacttype = dict()
    skipped_genes = 0
    assessed_genes = 0
    alternative_name_genes = 0

    entries = set()

    for genelist in condenseToGenelists(correlation_set):
        for gene in genelist:
            gene_name = gene.name
            if isCpg(gene_name):
                continue

            gene_id, entry_type = api.findGeneProductId(gene_name)

            entries.add(entry_type)

            if gene_id is None:
                alt_ids = dict()
                alt_names = dict()
                for alt_name in gene.alt_names:
                    alt_id, alt_entry_type = api.findGeneProductId(alt_name)
                    if alt_id is not None:
                        alt_ids[alt_id] = alt_entry_type
                        alt_names[alt_id] = alt_name
                if len(alt_ids):
                    first_alt_id = list(alt_ids.keys())[0]
                    entry_type = alt_ids[first_alt_id]
                    gene_id = first_alt_id
                    cpg_to_used_gene_name[gene.cpg] = first_alt_id
                    gene_name = alt_names[first_alt_id]
                    alternative_name_genes += 1
                else:
                    print("Couldn't find ID for gene", gene_name)
                    skipped_genes += 1
                    continue
            else:
                assessed_genes += 1

            cpg_to_used_gene_name[gene.cpg] = gene_name

            id_to_exacttype[gene_id] = entry_type
            gene_name_to_id[gene_name] = gene_id  # assuming there aren't multiple IDs for the same gene

            pathway_id_to_name[gene_id] = gene_name
            pathways[gene_name] = api.getPathwaysAssociatedWithId(gene_id)


    # common_pathways = {x: list() for x in pathway_id_to_name.keys()}
    common_pathways = dict()
    for k, v in pathways.items():
        for pathway in v:
            # common_pathways[pathway].append(k)
            try:
                common_pathways[pathway].append(k)
            except KeyError:
                common_pathways[pathway] = [k]

    dellist = set()
    for x in common_pathways.keys():
        if not len(common_pathways[x]) > 1:
            dellist.add(x)
    for x in dellist:
        del common_pathways[x]


    json_dict = {"common_pathways": common_pathways,
                 "pathways": {k: list(v) for k, v in pathways.items()},
                 "cpg_to_gene_name": cpg_to_used_gene_name,
                 "gene_name_to_id": gene_name_to_id,
                 "id_to_exacttype": id_to_exacttype,
                 "gene_count": {"good": assessed_genes, "skipped": skipped_genes, "alt_names_used": alternative_name_genes},
                 }


    with open(outfile, "w") as fo:
        json.dump(json_dict, fo)
    print("Wrote file to", outfile)


if __name__ == "__main__":
    import common.bio
    cohort_ = "chds"

    for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:
        run(cohort_, chromosome, common.bio.appropriate_norm_type[chromosome])


