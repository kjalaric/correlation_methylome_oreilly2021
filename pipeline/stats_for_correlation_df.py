import json
import numpy as np
import pandas as pd
import scipy.stats as st
from common.format_functions import formatCorrPkl, formatBetaCorrelationStatCsv, formatBetaCorrelationStatReport
from scripts.df_pkl_nan_dropper import loadPklAndDropNans
import common.bio


def run(cohort, chromosome, norm_type, corrtype="spearman"):
    df_file_loc = formatCorrPkl(cohort, chromosome, norm_type)
    csv_out_loc = formatBetaCorrelationStatCsv(cohort, chromosome, norm_type, corrtype)
    report_out_loc = formatBetaCorrelationStatReport(cohort, chromosome, norm_type, corrtype)

    # df = pd.read_pickle(df_file_loc)
    df, nulls = loadPklAndDropNans(df_file_loc)
    sample_count = len(df)

    # global statistics
    vals = [x for x in df.stack() if x != 1.0]  # may remove legitimate 1.0 correlations, but they will be extremely rare and it doesn't really influence these stats
    actual = list()
    reserve = set()
    for x in vals:
        if x in reserve:
            reserve.remove(x)
            actual.append(x)
        else:
            reserve.add(x)
    overall_average_correlation = np.average(actual)
    overall_median = np.median(actual)
    global_variance = np.var(actual)
    # skew_biased = st.skew(actual, bias=True)
    skew_unbiased = st.skew(actual, bias=False)
    # fisher_kurtosis_biased = st.kurtosis(actual, fisher=True, bias=True)
    fisher_kurtosis_unbiased = st.kurtosis(actual, fisher=True, bias=False)
    global_positives = 0
    global_negatives = 0

    overall_average_variance = 0
    overall_positive_correlation = 0
    overall_negative_correlation = 0
    overall_positive_correlations = 0
    overall_negative_correlations = 0

    zero_division_errors = 0
    with open(csv_out_loc, "w") as fo:
        fo.write("cpg,average_correlation,median_correlation,variance,skew_biased,skew_unbiased,excess_kurtosis_biased,excess_kurtosis_unbiased,num_positive_correlations,average_positive_correlation,positive_variance,num_negative_correlations,average_negative_correlation,negative_variance\n")
        for i, row in df.iterrows():
            fixed_row = row.drop(i)

            # average_correlation_overall = (sum(row) - 1.0) / (sample_count - 1.0)  # average correlation_oldformat with all others
            average_correlation = np.average(fixed_row)
            median_correlation = np.median(fixed_row)

            positive_correlations = [x for x in fixed_row if x >= 0.0]
            negative_correlations = [x for x in fixed_row if x < 0.0]

            global_positives += len(positive_correlations)
            global_negatives += len(negative_correlations)
            try:
                average_positive_correlation = float(sum(positive_correlations)) / float(len(positive_correlations))
                average_negative_correlation = float(sum(negative_correlations)) / float(len(negative_correlations))
            except ZeroDivisionError:  # rarely happens
                zero_division_errors += 1
                continue

            positive_variance = np.var(positive_correlations)
            negative_variance = np.var(negative_correlations)
            overall_variance = np.var(fixed_row)

            cpg_skew_biased = st.skew(fixed_row, bias=True)
            cpg_skew_unbiased = st.skew(fixed_row, bias=False)
            cpg_kurtosis_biased = st.kurtosis(fixed_row, fisher=True, bias=True)
            cpg_kurtosis_unbiased = st.kurtosis(fixed_row, fisher=True, bias=False)



            fo.write("{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(
                i, average_correlation, median_correlation, overall_variance, cpg_skew_biased, cpg_skew_unbiased, cpg_kurtosis_biased, cpg_kurtosis_unbiased,
                len(positive_correlations), average_positive_correlation, positive_variance,
                len(negative_correlations), average_negative_correlation, negative_variance))
            overall_positive_correlation += average_positive_correlation
            overall_negative_correlation += average_negative_correlation
            overall_average_variance += overall_variance
            overall_positive_correlations += len(positive_correlations)
            overall_negative_correlations += len(negative_correlations)

    overall_positive_correlation /= sample_count
    overall_negative_correlation /= sample_count
    overall_average_variance /= sample_count
    overall_positive_correlations /= sample_count
    overall_negative_correlations /= sample_count

    positive_negative_ratio = global_positives/global_negatives

    json_write_dict = {
        "overall_average_correlation": overall_average_correlation,
        "overall_variance": global_variance,
        "overall_skew_unbiased": skew_unbiased,
        "overall_excess_kurtosis_unbiased": fisher_kurtosis_unbiased,
        "average_cpg_variance": overall_average_variance,
        "average_positive_correlation": overall_positive_correlation,
        "average_negative_correlation": overall_negative_correlation,
        "average_number_of_positive_correlations": overall_positive_correlations,
        "average_number_of_negative_correlations": overall_negative_correlations,
        "positive_to_negative_ratio": positive_negative_ratio,
        "bad_lines_in_corr_file": zero_division_errors,
        "nans_dropped_from_corr_file": nulls,
    }

    with open(report_out_loc, "w") as fo:
        json.dump(json_write_dict, fo, indent=4)

    print("Wrote to", report_out_loc)


if __name__ == "__main__":
    cohort = "chds"
    for chromosome in ["21", "22", "x", "y"]: # ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "x"]:
        for norm_type in ["noob"]:
            print("Running: ", cohort, chromosome, norm_type)
            run(cohort, chromosome, norm_type, "spearman")
