import os
import itertools
import multiprocessing
import pickle as pkl
import pandas as pd
import common.file_format
from common.format_functions import formatCorrPkl
import common.read_files
import common.cpg_lookup
import data.info
import gencode.feature_lookup
import common.bio


def findCorrelationsWithinGenes(args):
    cohort, chromosome, norm_type, corrtype = args
    print(cohort, chromosome, norm_type, corrtype)
    df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type, corrtype))

    # definitions based on array type
    array_type = data.info.getArrayType(cohort)
    if array_type == "EPIC":
        import common.parse_epicarray_manifest as pam

        def getGene(cpg):
            return pam.getGene(cpg)
    elif array_type == "450k":
        import common.parse_450k_manifest as pam

        def getGene(cpg):
            return pam.getGene(cpg)
    elif array_type == "both":
        import common.parse_epicarray_manifest as pam
        import common.parse_450k_manifest as pam450k

        def getGene(cpg):
            try:
                return pam.getGene(cpg)
            except KeyError:
                return pam450k.getGene(cpg)

    # generate two dictionaries - one for illumina-annotated genes, one for gencode
    cpg_to_gene_object = {x: getGene(x) for x in list(df)}
    cpg_to_illumina_name = {k: v.name for k, v in cpg_to_gene_object.items()}
    cpg_to_gencode_name = {k: gencode.feature_lookup.findAnnotatedFeature(chromosome, v.mapinfo) for k, v in cpg_to_gene_object.items() if v.mapinfo is not None}

    gencode_gene_to_cpg = dict()
    '''  # this is causing too much trouble
    for k, v in cpg_to_gencode_name.items():
        if v[0] is not None:
            try:
                gencode_gene_to_cpg[v[0]].append(k)
            except KeyError:
                gencode_gene_to_cpg[v[0]] = [k]
    gencode_gene_to_cpg = {k: v for k, v in gencode_gene_to_cpg.items() if len(v) > 1}
    '''

    illumina_gene_to_cpg = dict()
    for k, v in cpg_to_illumina_name.items():
        if v != k:  # getGene name will be equal to the cpg site if no annotated gene was found
            try:
                illumina_gene_to_cpg[v].append(k)
            except KeyError:
                illumina_gene_to_cpg[v] = [k]
    illumina_gene_to_cpg = {k: v for k, v in illumina_gene_to_cpg.items() if len(v) > 1}

    # file output scheme: gene, gencode/illumina, cpg1, cpg1 mapinfo, cpg2, cpg2 mapinfo, distance, correlation
    out_file_loc = common.file_format.same_gene_correlation_report_all_genes_for_chromosome.format(cohort, chromosome, norm_type, corrtype)
    with open(out_file_loc, "w") as fo:
        fo.write("gene,annotation_source,cpg1,cpg1mapinfo,cpg2,cpg2mapinfo,distance,correlation\n")
        for database_name, dd in {"gencode": gencode_gene_to_cpg, "illumina": illumina_gene_to_cpg}.items():
            for gene, cpgs in dd.items():
                cpg_mapinfo = {x: cpg_to_gene_object[x].mapinfo for x in cpgs}
                for cpg_pair in itertools.combinations(cpgs, 2):
                    if cpg_mapinfo[cpg_pair[0]] > cpg_mapinfo[cpg_pair[1]]:  # always want distances to be positive
                        cpg1 = cpg_pair[0]
                        cpg2 = cpg_pair[1]
                    else:
                        cpg1 = cpg_pair[1]
                        cpg2 = cpg_pair[0]
                    distance = cpg_mapinfo[cpg1] - cpg_mapinfo[cpg2]
                    correlation_coef = df.loc[cpg1].loc[cpg2]
                    fo.write(f"{gene},{database_name},{cpg1},{cpg_mapinfo[cpg1]},{cpg2},{cpg_mapinfo[cpg2]},{distance},{correlation_coef}\n")


if __name__ == "__main__":
    cohort_ = "chds"
    norm_type_ = "noob"
    use_gencode_for_gene_names_ = False
    corr_type = "spearman"

    findCorrelationsWithinGenes([cohort_, "y", "swan", "spearman"])
    # findCorrelationsWithinGenes(cohort, "x", "swan", corrtype="spearman", use_gencode_for_gene_names=use_gencode_for_gene_names)

    '''
    args = [[cohort_, chromosome_, common.bio.appropriate_norm_type[chromosome_], corr_type] for chromosome_ in common.bio.epic_probes_smallest_to_largest]
    with multiprocessing.Pool(processes=3) as pool:
        pool.map(findCorrelationsWithinGenes, args)
    '''