import json
import itertools
import pandas as pd
import numpy as np
import scipy.stats as stats
import common.file_format
from common.format_functions import formatCorrPkl
import common.bio
import common.read_files


def run(cohort, chromosome, norm_type, corr_type="spearman"):
    print("running:", cohort, chromosome, norm_type, corr_type)
    file_out = common.file_format.correlations_same_pathway_strong_report.format(cohort, chromosome, norm_type, corr_type)
    overall_file_out = common.file_format.correlations_same_pathway_strong_overall_report.format(cohort, chromosome, norm_type, corr_type)

    json_write_dict = dict()
    overall_json_write_dict = dict()

    print("loading files...")
    strong_positives, strong_negatives = common.read_files.get10pcStrongCorrelationsFromFile(cohort, chromosome, norm_type, corr_type)
    strong_positives = [(sorted(k)[0], sorted(k)[1], v) for k, v in strong_positives.items()]
    strong_negatives = [(sorted(k)[0], sorted(k)[1], v) for k, v in strong_negatives.items()]

    with open(common.file_format.gene_association_report_strong_json.format(cohort, chromosome, norm_type, "all"), "r") as fi:
        pathway_json = json.load(fi)

    print("parsing files...")
    pathway_genes_with_cpgs = {gene: list() for gene in pathway_json["cpg_to_gene_name"].values()}
    for gene in pathway_genes_with_cpgs.keys():
        pathway_genes_with_cpgs[gene] = [k for k, v in pathway_json["cpg_to_gene_name"].items() if v == gene]


    pathway_gene_combinations = set()
    for genes in pathway_json["common_pathways"].values():
        pathway_gene_combinations.add(tuple(genes))

    print("going over genes and cpg sites...")
    # detected genes for json
    detected_genes = set()
    for x in pathway_gene_combinations:
        for xx in x:
            detected_genes.add(xx)

    overall_json_write_dict["identified_overlapping_pathways"] = len(pathway_gene_combinations)
    overall_json_write_dict["n_assessed_genes"] = len(detected_genes)


    print("appending correlations...")
    possible_cpg_combinations = dict()
    for combination in pathway_gene_combinations:
        possible_cpg_combinations[combination] = list()
        for i, x in enumerate(combination):
            for ii, xx in enumerate(combination):
                if i == ii:
                    break
                for cpgx in pathway_genes_with_cpgs[x]:
                    for cpgxx in pathway_genes_with_cpgs[xx]:
                        possible_cpg_combinations[combination].append(sorted([cpgx, cpgxx]))

    valid_combinations = set()
    positives = list()
    negatives = list()
    for combination, cpg_combinations in possible_cpg_combinations.items():
        for strong_positive in strong_positives:
            done = False
            if cpg_combinations[0] == strong_positive[0] and cpg_combinations[1] == strong_positive[1]:
                valid_combinations.add(combination)
                positives.append(strong_positive)
                done = True
                break
        if done:
            break
        for strong_negative in strong_negatives:
            if cpg_combinations[0] == strong_negative[0] and cpg_combinations[1] == strong_negative[1]:
                valid_combinations.add(combination)
                negatives.append(strong_negative)
                break


    json_write_dict["identified_overlapping_pathways"] = len(valid_combinations)
    valid_genes = set()
    for x in valid_combinations:
        for y in x:
            valid_genes.add(y)
    json_write_dict["n_assessed_genes"] = len(valid_genes)

    # number of cpg sites in pathways
    all_cpgs_in_pathways = set()
    for corr_direction in [positives, negatives]:
        for x in corr_direction:
            all_cpgs_in_pathways.add(x[0])
            all_cpgs_in_pathways.add(x[1])

    # number of cpg sites overall
    all_cpgs_overall = set()
    for corr_direction in [strong_positives, strong_negatives]:
        for x in corr_direction:
            all_cpgs_overall.add(x[0])
            all_cpgs_overall.add(x[1])

    json_write_dict["n_cpg_sites"] = len(all_cpgs_in_pathways)
    overall_json_write_dict["n_cpg_sites"] = len(all_cpgs_overall)

    positive_overall = [x[2] for x in strong_positives]
    negative_overall = [x[2] for x in strong_negatives]
    positive_pathway = [x[2] for x in positives]
    negative_pathway = [x[2] for x in negatives]

    json_write_dict["n_positive_correlating_pairs"] = len(positive_pathway)
    json_write_dict["n_negative_correlating_pairs"] = len(negative_pathway)
    json_write_dict["mean_positive_correlation"] = np.mean(positive_pathway)
    json_write_dict["mean_negative_correlation"] = np.mean(negative_pathway)



    overall_json_write_dict["n_positive_correlating_pairs"] = len(strong_positives)
    overall_json_write_dict["n_negative_correlating_pairs"] = len(strong_negatives)
    overall_json_write_dict["mean_positive_correlation"] = np.mean(positive_overall)
    overall_json_write_dict["mean_negative_correlation"] = np.mean(negative_overall)

    print("running positive ANOVA...")
    positive_anova_results = stats.f_oneway(positive_pathway, positive_overall)
    
    print("running negative ANOVA...")
    negative_anova_results = stats.f_oneway(negative_pathway, negative_overall)
    
    json_write_dict["anova_positive"] = {"f": positive_anova_results.statistic, "p": positive_anova_results.pvalue}
    json_write_dict["anova_negative"] = {"f": negative_anova_results.statistic, "p": negative_anova_results.pvalue}
    overall_json_write_dict["anova_positive"] = {"f": positive_anova_results.statistic, "p": positive_anova_results.pvalue}  # write the same things into both, can use to check for consistency if required
    overall_json_write_dict["anova_negative"] = {"f": negative_anova_results.statistic, "p": negative_anova_results.pvalue}

    with open(file_out, "w") as fo:
        json.dump(json_write_dict, fo)
    print("wrote to", file_out)

    with open(overall_file_out, "w") as fo:
        json.dump(overall_json_write_dict, fo)
    print("wrote to", overall_file_out)


if __name__ == "__main__":
    for chromosome_ in ["21"]:#common.bio.epic_probes_smallest_to_largest[:13]:  # doing the later ones
        run("chds", chromosome_, common.bio.appropriate_norm_type[chromosome_], "spearman")