import os
import json
import pickle as pkl
import networkx as nx
import common.file_format
import common.read_files
import common.cpg_lookup
import data.info
import config.protocol
import gencode.feature_lookup
from common.format_functions import format10pcCorrelationNetwork, format10pcCorrelationNetworkJson


# plot_options_no_edgecolour = {k: v for k, v in plot_options.items() if k is not "edge_color"}

NORMAL_WEIGHT = 1
THICC_WEIGHT = 3


class MockCpg(object):
    def __init__(self, cpg):
        self.name = cpg


def constructGraph(connections, light_colours=False):
    '''
    # automatically adds cpgs to the graph according to connections which are provided as 2-tuples
    G = nx.Graph()

    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    G.add_edges_from(connections)

    return G
    '''
    return constructGraphWithChromosomes(connections, light_colours)


def constructGraphWithChromosomes(connections, light_colours=False):
    '''
    Edges in the graph are colour coded as follows:
        > green: same chromosome, positive correlation_oldformat
        > cyan: different chromosome, positive correlation_oldformat
        > red: same chromosome, negative correlation_oldformat
        > orange: different chromosome, negative correlation_oldformat
    Lines are thicker if there are multiple correlated CpG sites on the two genes.
    If there is conflicting correlation_oldformat (i.e. there are at least two pairs of
    correlated CpGs on two genes, and they're not all positive or all negative correlations) then the line is black.
    '''

    if light_colours:
        greencolour = 'greenyellow'
    else:
        greencolour = "green"


    # automatically adds cpgs to the graph according to connectionm which are provided as 3-tuples along with chromosome
    G = nx.Graph()

    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    for connection in connections:
        # already exists: make the line thicker, update colour if connection[2] is true (otherwise leave it)
        if G.has_edge(connection[0], connection[1]):
            G[connection[0]][connection[1]]['weight'] = THICC_WEIGHT
            # print(f"THICC detected, this hasn't happened before: {connection[0]}, {connection[1]}")
            if connection[2]:
                G[connection[0]][connection[1]]['edge_color'] = greencolour
        else:
            if connection[2]:
                G.add_edge(connection[0], connection[1], edge_color=greencolour, weight=NORMAL_WEIGHT)
            else:
                G.add_edge(connection[0], connection[1], edge_color='cyan', weight=NORMAL_WEIGHT)

    return G


def addNegativeCorrelationsToGraph(G, connections, light_colours=False):
    '''
    Edges in the graph are colour coded as follows:
        > green: same chromosome, positive correlation_oldformat
        > cyan: different chromosome, positive correlation_oldformat
        > red: same chromosome, negative correlation_oldformat
        > orange: different chromosome, negative correlation_oldformat
    Lines are thicker if there are multiple correlated CpG sites on the two genes.
    If there is conflicting correlation_oldformat (i.e. there are at least two pairs of
    correlated CpGs on two genes, and they're not all positive or all negative correlations) then the line is black.
    '''

    if light_colours:
        redcolour = 'lightcoral'
        conflictcolour = 'silver'
    else:
        redcolour = "red"
        conflictcolour = "black"

    # automatically adds cpgs to the graph according to connectionm which are provided as 3-tuples along with chromosome
    cpgs = set()
    for connection in connections:
        cpgs.add(connection[0])
        cpgs.add(connection[1])

    G.add_nodes_from(cpgs)

    del(cpgs)  # there could be a lot of them

    for connection in connections:
        # already exists: make the line thicker, update colour if connection[2] is true (otherwise leave it)
        # UNLESS colour is green, cyan or black - in which case the colour needs to be set to black
        if G.has_edge(connection[0], connection[1]):
            G[connection[0]][connection[1]]['weight'] = THICC_WEIGHT

            # conflicting interactions
            if (G[connection[0]][connection[1]]['edge_color'] in ['green', 'cyan', 'greenyellow']):
                G[connection[0]][connection[1]]['edge_color'] = conflictcolour
                print("Conflicting interaction: {} - {}".format(connection[0], connection[1]))
            elif G[connection[0]][connection[1]]['edge_color'] == conflictcolour:
                pass
            elif connection[2]:
                G[connection[0]][connection[1]]['edge_color'] = redcolour
        else:
            if connection[2]:
                G.add_edge(connection[0], connection[1], edge_color=redcolour, weight=NORMAL_WEIGHT)
            else:
                G.add_edge(connection[0], connection[1], edge_color='orange', weight=NORMAL_WEIGHT)
    return G


def pruneExceptFor(G, prunelist, max_dist=None):
    if isinstance(prunelist, str):
        prunelist = [prunelist]

    new_graph = nx.Graph()

    for find_me in prunelist:
        new_graph.add_node(find_me)
        neighbours = set()
        converged = False
        neighbourhood = G.neighbors(find_me)

        while not converged:
            converged = True
            for n in neighbourhood:
                neighbours.add(n)
            for n in neighbours.copy():
                neighbourhood = G.neighbors(n)
                for nn in neighbourhood:
                    if nn not in neighbours:
                        converged = False
                        neighbours.add(nn)

        for n in neighbours:
            new_graph.add_node(n)
            for edge in G.edges(n):
                new_graph.add_edge(edge[0], edge[1])
                try:
                    new_graph[edge[0]][edge[1]]['edge_color'] = G[edge[0]][edge[1]]['edge_color']
                except:
                    new_graph[edge[0]][edge[1]]['edge_color'] = 'blue'
                try:
                    new_graph[edge[0]][edge[1]]['weight'] = G[n][edge]['weight']
                except:
                    new_graph[edge[0]][edge[1]]['weight'] = NORMAL_WEIGHT

    if max_dist is not None:  # more efficient to do this in the 'converged' algortihm but i'll do that later
        ego_graphs = list()
        for x in prunelist:
            ego_graphs.append(nx.generators.ego_graph(new_graph, x, radius=max_dist))
        composed_graph = nx.Graph()
        for x in ego_graphs:
            composed_graph = nx.compose(composed_graph, x)
        return composed_graph

    return new_graph


def graphStats(G):
    print(f"{G.number_of_nodes()} nodes\n{G.number_of_edges()} edges")


def rankByNeighbours(G):
    # returns a dictionary of nodes sorted by the number of neighbours
    neighbours = dict([(x, len(G.adj[x])) for x in G])
    return sorted(neighbours.items(), key=lambda x:x[1], reverse=True)


def saveGraph(G, cohort, chromosome, norm_type, corrtype, meta=None, overlap=False, verbose=False, override_filename=None, pkl_protocol=config.protocol.pkl):
    if override_filename:
        file_loc = override_filename
    elif meta is not None:
        if not overlap:
            file_loc = common.file_format.correlation_network_meta.format(cohort, corrtype, chromosome, norm_type, meta)
        else:
            file_loc = common.file_format.correlation_network_overlap_meta.format(cohort, corrtype, chromosome, norm_type, meta)
    else:
        if not overlap:
            file_loc = common.file_format.correlation_network.format(cohort, corrtype, chromosome, norm_type)
        else:
            file_loc = common.file_format.correlation_network_overlap.format(cohort, corrtype, chromosome, norm_type)

    with open(file_loc, "wb") as fo:
        pkl.dump(G, fo, protocol=pkl_protocol)
    if verbose:
        print(f"Saved network to {file_loc}")


def loadGraph(cohort, chromosome, norm_type, corrtype="spearman", meta=None, overlap=False, verbose=False):
    if meta is not None:
        if not overlap:
            file_loc = common.file_format.correlation_network_meta.format(cohort, corrtype, chromosome, norm_type, meta)
        else:
            file_loc = common.file_format.correlation_network_overlap_meta.format(cohort, corrtype, chromosome, norm_type, meta)
    else:
        if not overlap:
            file_loc = common.file_format.correlation_network.format(cohort, corrtype, chromosome, norm_type)
        else:
            file_loc = common.file_format.correlation_network_overlap.format(cohort, corrtype, chromosome, norm_type)
    with open(file_loc, "rb") as fi:
        G = pkl.load(fi)
    if verbose:
        print(f"Loaded network from {file_loc}")
    return G


def generateNetwork(cohort, chromosome, norm_type, corrtype="spearman", threshold=0.7, negatives_only=False, regenerate_graph=False, strongest=None, cpgs_only=False, light_colours=False,
        use_gencode_for_gene_names=True, file_override_positive=None, file_override_negative=None, file_override_out=None):
    # this uses thresholded correlations
    if cohort in data.info.overlaps:
        overlap = True
        array_type = "both"
    else:
        overlap = False
        array_type = data.info.getArrayType(cohort)

    if not regenerate_graph:
        if not negatives_only:
            if os.path.exists(common.file_format.correlation_network.format(cohort, corrtype, chromosome, norm_type)):
                G = loadGraph(cohort, chromosome, norm_type, meta=None, overlap=overlap, verbose=True)
            else:
                G = None
        else:
            if os.path.exists(common.file_format.correlation_network_meta.format(cohort, corrtype, chromosome, norm_type, "negatives")):
                G = loadGraph(cohort, chromosome, norm_type, meta="negatives", overlap=overlap, verbose=True)
            else:
                G = None
    else:
        G = None

    if G is None:
        if cpgs_only:
            def getGene(cpg):
                return MockCpg(cpg)
        elif array_type == "EPIC":
            import common.parse_epicarray_manifest as pam
            def getGene(cpg):
                return pam.getGene(cpg)
        elif array_type == "450k":
            import common.parse_450k_manifest as pam

            def getGene(cpg):
                return pam.getGene(cpg)
        elif array_type == "both":
            import common.parse_epicarray_manifest as pam
            import common.parse_450k_manifest as pam450k

            def getGene(cpg):
                try:
                    return pam.getGene(cpg)
                except KeyError:
                    return pam450k.getGene(cpg)

        if not file_override_negative:
            pnc, nnc = common.read_files.getNormCorrelationsFromFiles(cohort, chromosome, [norm_type], overlap=overlap)
        else:
            nnc = {norm_type: dict()}
            with open(file_override_negative, "r") as fi:
                for fline in fi.readlines():
                    row = fline.strip().split(",")
                    nnc[norm_type][(row[0], row[1])] = float(row[2])
            if not negatives_only:
                pnc = {norm_type: dict()}
                with open(file_override_positive, "r") as fi:
                    for fline in fi.readlines():
                        row = fline.strip().split(",")
                        pnc[norm_type][(row[0], row[1])] = float(row[2])

        if strongest is None:
            if not negatives_only:
                for key in pnc.keys():
                    pnc[key] = {k: v for k, v in pnc[key].items() if v >= threshold}
            for key in nnc.keys():
                nnc[key] = {k: v for k, v in nnc[key].items() if v <= -threshold}
        else:
            if not negatives_only:
                pnc_sorted = dict()
                for key in pnc.keys():
                    pnc[key] = {k: v for k, v in pnc[key].items() if v >= threshold}  # threshold before sorting
                    pnc_sorted[key] = sorted(pnc[key].items(), key = lambda x: x[1], reverse=True)
                    try:
                        pnc[key] = {x[0]: x[1] for x in pnc_sorted[key][:strongest]}
                    except:
                        pass  # use all of them
            nnc_sorted = dict()
            for key in nnc.keys():
                nnc[key] = {k: v for k, v in nnc[key].items() if v <= -threshold}
                nnc_sorted[key] = sorted(nnc[key].items(), key=lambda x: x[1], reverse=False)
                try:
                    nnc[key] = {x[0]: x[1] for x in nnc_sorted[key][:strongest]}
                except:
                    pass  # use all of them


        if use_gencode_for_gene_names:
            def lookupGencode(gene_class_object):
                try:
                    name, feature, gene_type = gencode.feature_lookup.findAnnotatedFeature(gene_class_object.chromosome, gene_class_object.mapinfo)
                    if name is not None:
                        return name
                    else:
                        return gene_class_object.name
                except Exception as e:
                    print(e)
                    return gene_class_object.name
            if not negatives_only:
                positive_connections = [(lookupGencode(getGene(x[0])), lookupGencode(getGene(x[1])), True) for x in pnc[norm_type].keys()]
            negative_connections = [(lookupGencode(getGene(x[0])), lookupGencode(getGene(x[1])), True) for x in nnc[norm_type].keys()]
        else:
            if not negatives_only:
                positive_connections = [(getGene(x[0]).name, getGene(x[1]).name, True) for x in pnc[norm_type].keys()]
            negative_connections = [(getGene(x[0]).name, getGene(x[1]).name, True) for x in nnc[norm_type].keys()]


        if not negatives_only:
            G = constructGraph(positive_connections, light_colours=light_colours)
            G = addNegativeCorrelationsToGraph(G, negative_connections, light_colours=light_colours)
            saveGraph(G, cohort, chromosome, norm_type, corrtype, meta=None, overlap=overlap, verbose=True, override_filename=file_override_out)

        else:
            G = nx.Graph()
            G = addNegativeCorrelationsToGraph(G, negative_connections, light_colours=light_colours)
            saveGraph(G, cohort, chromosome, norm_type, corrtype, meta="negatives", overlap=overlap, verbose=True, override_filename=file_override_out)
    return G



def generateNetwork10pc(cohort, chromosome, norm_type, corr_type="spearman",
                        negatives_only=False, cpgs_only=False, light_colours=False, use_gencode_for_gene_names=True):

    array_type = data.info.getArrayType(cohort)

    # cpg conversion function
    if cpgs_only:
        def getGene(cpg):
            return MockCpg(cpg)
    elif array_type == "EPIC":
        import common.parse_epicarray_manifest as pam
        def getGene(cpg):
            return pam.getGene(cpg)
    elif array_type == "450k":
        import common.parse_450k_manifest as pam
        def getGene(cpg):
            return pam.getGene(cpg)
    elif array_type == "both":
        import common.parse_epicarray_manifest as pam
        import common.parse_450k_manifest as pam450k
        def getGene(cpg):
            try:
                return pam.getGene(cpg)
            except KeyError:
                return pam450k.getGene(cpg)

    pnc, nnc = common.read_files.get10pcStrongCorrelationsFromFile(cohort, chromosome, norm_type, corr_type)

    if use_gencode_for_gene_names:
        def lookupGencode(gene_class_object):
            try:
                name, feature, gene_type = gencode.feature_lookup.findAnnotatedFeature(gene_class_object.chromosome, gene_class_object.mapinfo)
                if name is not None:
                    return name
                else:
                    return gene_class_object.name
            except Exception as e:
                print(e)
                return gene_class_object.name
        if not negatives_only:
            positive_connections = [(lookupGencode(getGene(x[0])), lookupGencode(getGene(x[1])), True) for x in pnc.keys()]
        negative_connections = [(lookupGencode(getGene(x[0])), lookupGencode(getGene(x[1])), True) for x in nnc.keys()]
    else:
        if not negatives_only:
            positive_connections = [(getGene(x[0]).name, getGene(x[1]).name, True) for x in pnc.keys()]
        negative_connections = [(getGene(x[0]).name, getGene(x[1]).name, True) for x in nnc.keys()]

    # sort out metadata
    if use_gencode_for_gene_names:
        meta = "gencode"
    else:
        meta = None


    if not negatives_only:
        file_loc = format10pcCorrelationNetwork(cohort, chromosome, norm_type, corr_type, negatives_only=False, meta=meta)
        G = constructGraph(positive_connections, light_colours=light_colours)
        G = addNegativeCorrelationsToGraph(G, negative_connections, light_colours=light_colours)
        with open(file_loc, "wb") as fo:
            pkl.dump(G, fo, protocol=config.protocol.pkl)
        print(f"Saved network to {file_loc}")

        json_file_loc = format10pcCorrelationNetworkJson(cohort, chromosome, norm_type, corr_type, negatives_only=False, meta=meta)
        with open(json_file_loc, "w") as fo:
            fo.write(nx.jit_data(G))
        print(f"Saved json to {json_file_loc}")

    else:
        file_loc = format10pcCorrelationNetwork(cohort, chromosome, norm_type, corr_type, negatives_only=True)
        G = nx.Graph()
        G = addNegativeCorrelationsToGraph(G, negative_connections, light_colours=light_colours)
        with open(file_loc, "wb") as fo:
            pkl.dump(G, fo, protocol=config.protocol.pkl)
        print(f"Saved network to {file_loc}")
    return G


if __name__ == "__main__":
    import common.bio

    for use_gencode in [True, False]:
        generateNetwork10pc("chds", "y", "swan", corr_type="spearman", negatives_only=False, use_gencode_for_gene_names=use_gencode)
        generateNetwork10pc("chds", "x", "swan", corr_type="spearman", negatives_only=False, use_gencode_for_gene_names=use_gencode)

        for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:
            generateNetwork10pc("chds", chromosome, "noob", corr_type="spearman", negatives_only=False, use_gencode_for_gene_names=use_gencode)





    # for clique in nx.find_cliques(G):
#         print(clique)

