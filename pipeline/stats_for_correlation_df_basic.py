from datetime import datetime
import json
import multiprocessing
import numpy as np
import pandas as pd
import scipy.stats as st
from common.format_functions import formatCorrPkl, formatBetaCorrelationStatCsv, formatBetaCorrelationStatReport
from scripts.df_pkl_nan_dropper import loadPklAndDropNans
import common.bio


def run(cohort, chromosome, norm_type, corr_type="spearman"):
    df_file_loc = formatCorrPkl(cohort, chromosome, norm_type, corr_type)
    report_out_loc = formatBetaCorrelationStatReport(cohort, chromosome, norm_type, corr_type, meta="basic")

    # df = pd.read_pickle(df_file_loc)
    df, nulls = loadPklAndDropNans(df_file_loc)
    sample_count = len(df)

    # global statistics
    vals = [x for x in df.stack() if x != 1.0]  # may remove legitimate 1.0 correlations, but they will be extremely rare and it doesn't really influence these stats
    actual = list()
    reserve = set()
    for x in vals:
        if x in reserve:
            reserve.remove(x)
            actual.append(x)
        else:
            reserve.add(x)
    overall_average_correlation = np.average(actual)
    global_variance = np.var(actual)

    overall_average_variance = 0
    overall_positive_correlation = 0
    overall_negative_correlation = 0
    overall_positive_correlations = 0
    overall_negative_correlations = 0

    zero_division_errors = 0
    for i, row in df.iterrows():
        fixed_row = row.drop(i)

        positive_correlations = [x for x in fixed_row if x >= 0.0]
        negative_correlations = [x for x in fixed_row if x < 0.0]

        # remove 1.0 from positives as it'll always correlate with itself  < DONT NEED TO DO THIS ANYMORE
        try:
            average_positive_correlation = float(sum(positive_correlations)) / float(len(positive_correlations))
            average_negative_correlation = float(sum(negative_correlations)) / float(len(negative_correlations))
        except ZeroDivisionError:  # rarely happens
            zero_division_errors += 1
            continue

        overall_variance = np.var(fixed_row)

        overall_positive_correlation += average_positive_correlation
        overall_negative_correlation += average_negative_correlation
        overall_average_variance += overall_variance
        overall_positive_correlations += len(positive_correlations)
        overall_negative_correlations += len(negative_correlations)

    overall_positive_correlation /= sample_count
    overall_negative_correlation /= sample_count
    overall_average_variance /= sample_count
    overall_positive_correlations /= sample_count
    overall_negative_correlations /= sample_count

    json_write_dict = {
        "overall_average_correlation": overall_average_correlation,
        "overall_variance": global_variance,
        "average_cpg_variance": overall_average_variance,
        "average_positive_correlation": overall_positive_correlation,
        "average_negative_correlation": overall_negative_correlation,
        "average_number_of_positive_correlations": overall_positive_correlations,
        "average_number_of_negative_correlations": overall_negative_correlations,
        "bad_lines_in_corr_file": zero_division_errors,
        "nans_dropped_from_corr_file": nulls,
    }

    with open(report_out_loc, "w") as fo:
        json.dump(json_write_dict, fo, indent=4)

    print("Wrote to", report_out_loc)


if __name__ == "__main__":
    cohort_ = "mtab7069"

    done = {None: [None]}  # {"18": ["raw", "illumina", "noob", "quant", "swan"]}

    for chromosome_ in common.bio.epic_probes_smallest_to_largest[:13]:  # first 13 goes up to chr10 i think
        for norm_type_ in common.bio.norm_types:
            for corr_type_ in ["spearman"]:
                skip = False
                for k, v in done.items():
                    if chromosome_ == k:
                        if norm_type_ in v:
                            skip = True
                            break
                if skip:
                    continue

                print("\nRunning: ", cohort_, chromosome_, norm_type_)
                start_time = datetime.now()
                run(cohort_, chromosome_, norm_type_, corr_type_)
                end_time = datetime.now()
                print(end_time-start_time)
