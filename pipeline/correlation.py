import os
import pandas as pd
import common.file_format
import common.cpg_lookup
from common.format_functions import formatCorrPkl


def run(cohort, chromosome, norm_type, correlation_type):
    valid_correlation_types = ["pearson", "spearman", "kendall"]
    if correlation_type not in valid_correlation_types:
        raise Exception(f"Invalid correlation_oldformat type! Got {correlation_type}, needs to be one of {valid_correlation_types}")

    # writer = Writer(cohort, chromosome, norm_type, correlation_type)
    # cpg_lookup = common.cpg_lookup.getLookupDict(data.info.getArrayType(cohort), chromosome, as_int=True)

    print("Starting {} chr {}: {}...".format(cohort, chromosome, norm_type))

    df = pd.read_csv(os.path.join(common.file_format.betacsv_per_chromosome.format(cohort, chromosome, norm_type)), index_col=0)
    df = df.T

    beta_corr = df.corr(method=correlation_type)
    beta_corr.to_pickle(formatCorrPkl(cohort, chromosome, norm_type, correlation_type))
    return


if __name__ == "__main__":
    # run("chds", "20", "noob", threshold=0.7, correlation_type="pearson")
    run("chds", "x", "noob", threshold=0.7, correlation_type="pearson")
    run("chds", "x", "noob", threshold=0.7, correlation_type="kendall")
    # run("chds", "20", "noob", threshold=0.7, correlation_type="kendall")


