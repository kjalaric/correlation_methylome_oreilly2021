import os
import multiprocessing
import pipelineold.correlation_for_individual_chromosome
import common.bio
import common.file_format
import data.info
from datetime import datetime


def start(args):
    # args: cohort_name, chromosome, norm_type
    # print(f"Starting: {args[0]} {args[1]}, {args[2]}")
    try:
        pipelineold.correlation_for_individual_chromosome.run(args[0], args[1], args[2], args[3], args[4], args[5], args[6])
    except Exception as e:
        print(f"ERROR! [{args[0]} {args[1]} {args[2]}]: {e}")


if __name__ == "__main__":
    args = list()  # estimated ram usage, arguments
    threshold = 0.7
    metadata = None
    regenerate = True
    pkls_only = True
    max_ram_use_in_gb = 60
    process_limit = 12
    process_ram_use_overhead_coefficient = 2.0  # for pickling etc.

    chromosomes = common.bio.epic_probes_smallest_to_largest
    chromosomes.remove("21")
    chromosomes.remove("y")
    chromosomes.remove("x")
    chromosomes.remove("22")

    with multiprocessing.Pool(processes=4) as pool:
        pool.map(start, [["chds", "21", "noob", threshold, metadata, pkls_only, regenerate], ["chds", "y", "noob", threshold, metadata, pkls_only, regenerate],
                         ["chds", "x", "noob", threshold, metadata, pkls_only, regenerate], ["chds", "22", "noob", threshold, metadata, pkls_only, regenerate]])



    for cohort_name in ["chds"]:
        for norm_type in ["noob"]:
            for chromosome in common.bio.epic_probes_smallest_to_largest:  # [str(x) for x in [3, 4, 5, 8, 12, 14, 16, 17]]:# [x for x in common.bio.chromosomes if x not in ["1", "2", "3"]]:
                if regenerate or not os.path.exists(common.file_format.correlation_oldformat.format(cohort_name, "strong", "positive", chromosome, norm_type)):
                    estimated_ram_use = (data.info.estimateBetaSizeInMemory(cohort_name, chromosome) + data.info.estimateBetaCorrSizeInMemory(chromosome)) * process_ram_use_overhead_coefficient
                    args.append([estimated_ram_use, [cohort_name, chromosome, norm_type, threshold, metadata, pkls_only, regenerate]])
                else:
                    print("Found correlation_oldformat files for {} | {} | {}".format(cohort_name, chromosome, norm_type))

    args.sort(key=lambda z: z[0], reverse=True)
    process_chunks = list()

    last_arg_vector_length = 0
    while len(args):
        process_chunks.append([args[0]])
        del args[0]
        indicies = list()
        for index, arg in enumerate(reversed(args)):
            if len(process_chunks[-1]) >= process_limit:
                break
            elif sum(y[0] for y in process_chunks[-1]) + arg[0] >= data.info.gigabytesToBytes(max_ram_use_in_gb):
                break
            else:
                process_chunks[-1].append(arg)
                indicies.append(len(args) - index - 1)
        for x in indicies:
            del args[x]
        if len(args) == last_arg_vector_length:
            raise Exception("Didn't converge, something is probably too big to run...")
        else:
            last_arg_vector_length = len(args)


    # print(data.info.toGigabytes(arg[0]), arg[1][1])
    for j in process_chunks:
        start_time = datetime.now()
        with multiprocessing.Pool(processes=process_limit) as pool:  # 6 processes uses about 80% CPU
            pool.map(start, [x[1] for x in j])
        end_time = datetime.now()
        print("Chunk runtime: ", end_time - start_time)
