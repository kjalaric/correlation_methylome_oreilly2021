import re
import json
import api.reactome as api
import common.file_format
import common.read_files
import common.parse_epicarray_manifest as pam  # assuming cpgs from 450k is the same in this one, but with more recent annotations
import common.parse_450k_manifest as pam450k  # assuming cpgs from 450k is the same in this one, but with more recent annotations

import common.set_combine
from common.check_cpg import isCpg


def cleanhtml(string):
    cleanr = re.compile('<.*?>')
    return re.sub(cleanr, '', string)


def condenseToGenelists(correlation_set):
    cpglists = common.set_combine.condense(correlation_set)
    cpglists = sorted(cpglists, key=lambda x: len(x), reverse=True)
    for id, cpglist in enumerate(cpglists):
        genelist = list()
        for cpg in cpglist:
            try:
                genelist.append(pam.getGene(cpg, throwException=True))
            except:
                try:
                    genelist.append(pam450k.getGene(cpg, throwException=True))
                except:
                    genelist.append(cpg)
        genelist = sorted(genelist, key=lambda x: x.name)
        yield genelist


def run_report(cohort, chromosome, norm_type, meta=None):
    if meta is not None:
        outfile = common.file_format.gene_association_report_meta.format(cohort, chromosome, norm_type, meta)
    else:
        outfile = common.file_format.gene_association_report.format(cohort, chromosome, norm_type)


    positive_correlations, negative_correlations = common.read_files.getCorrelationsFromFile(cohort, chromosome, norm_type, meta=meta)
    correlation_set = set()
    for x in positive_correlations.keys():
        correlation_set.add(x)
    for x in negative_correlations.keys():
        correlation_set.add(x)

    pathways = dict()
    pathway_id_to_name = dict()


    for genelist in condenseToGenelists(correlation_set):
        for gene in genelist:
            gene_name = gene.name
            if isCpg(gene_name):
                continue

            pathways[gene_name] = set()
            gene_results = api.searchReactome(gene_name)
            try:
                for results in gene_results["results"]:
                    if results['typeName'] == "Pathway" or results['typeName'] == 'Reaction':
                        for entry in results["entries"]:
                            # todo i think this is trying to add them repetitively?
                            try:
                                pathways[gene_name].add(entry["id"])
                                pathway_id_to_name[entry["id"]] = cleanhtml(entry["name"])
                            except:
                                continue
            except KeyError:
                continue

    common_pathways = {x: list() for x in pathway_id_to_name.keys()}
    for k, v in pathways.items():
        for pathway in v:
            common_pathways[pathway].append(k)

    dellist = set()
    for x in common_pathways.keys():
        if not len(common_pathways[x]) > 1:
            dellist.add(x)
    for x in dellist:
        del common_pathways[x]


    with open(outfile, "w") as fo:
        if len(common_pathways.keys()):
            if meta is not None:
                fo.write(f"\n=== Common pathways found in correlations: {cohort}, Chr{chromosome}, {norm_type} ({meta}) ===\n")
            else:
                fo.write(f"\n=== Common pathways found in correlations: {cohort}, Chr{chromosome}, {norm_type} ===\n")
            for k, v in common_pathways.items():
                fo.write(pathway_id_to_name[k])
                fo.write("\n")
                for x in v:
                    fo.write("\t{}\n".format(x))
            fo.write("\n")
        else:
            if meta is not None:
                fo.write(f"\n=== NO COMMON PATHWAYS WERE FOUND FOR: {cohort}, Chr{chromosome}, {norm_type} ({meta}) ===\n\n")
            else:
                fo.write(f"\n=== NO COMMON PATHWAYS WERE FOUND FOR: {cohort}, Chr{chromosome}, {norm_type} ===\n\n")


        for k, v in pathways.items():
            if len(v):
                fo.write(f"\n=== Pathways for {k} ===\n")
                for x in v:
                    fo.write(pathway_id_to_name[x])
                    fo.write("\n")


def run(cohort, chromosome, norm_type, meta=None):
    if meta is not None:
        outfile = common.file_format.gene_association_report_meta_json.format(cohort, chromosome, norm_type, meta)
    else:
        outfile = common.file_format.gene_association_report_json.format(cohort, chromosome, norm_type)

    positive_correlations, negative_correlations = common.read_files.get10pcStrongCorrelationsFromFile(cohort, chromosome, norm_type)
    correlation_set = set()
    for x in positive_correlations.keys():
        correlation_set.add(x)
    for x in negative_correlations.keys():
        correlation_set.add(x)

    pathways = dict()
    pathway_id_to_name = dict()

    for genelist in condenseToGenelists(correlation_set):
        for gene in genelist:
            gene_name = gene.name
            if isCpg(gene_name):
                continue

            pathways[gene_name] = set()
            gene_results = api.searchReactome(gene_name)

            try:
                for results in gene_results["results"]:
                    if results['typeName'] == "Pathway" or results['typeName'] == 'Reaction':
                        for entry in results["entries"]:
                            # todo i think this is trying to add them repetitively?
                            try:
                                pathways[gene_name].add(entry["id"])
                                pathway_id_to_name[entry["id"]] = cleanhtml(entry["name"])
                            except:
                                continue
            except KeyError:
                continue

    common_pathways = {x: list() for x in pathway_id_to_name.keys()}
    for k, v in pathways.items():
        for pathway in v:
            common_pathways[pathway].append(k)

    dellist = set()
    for x in common_pathways.keys():
        if not len(common_pathways[x]) > 1:
            dellist.add(x)
    for x in dellist:
        del common_pathways[x]


    json_dict = {"common_pathways": common_pathways, "pathways": {k: list(v) for k, v in pathways.items()}}


    with open(outfile, "w") as fo:
        json.dump(json_dict, fo)
    print("Wrote file to", outfile)


if __name__ == "__main__":
    import common.bio
    cohort_ = "chds"

    for chromosome in common.bio.chromosomes_allosomal:
        run(cohort_, chromosome, "swan")

    for chromosome in common.bio.epic_probes_smallest_to_largest[:13]:
        run(cohort_, chromosome, "noob")


