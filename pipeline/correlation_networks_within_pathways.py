import json
import itertools
import pandas as pd
import numpy as np
import scipy.stats as stats
import common.file_format
from common.format_functions import formatCorrPkl
import common.bio


def run(cohort, chromosome, norm_type, corr_type="spearman"):
    print("running:", cohort, chromosome, norm_type, corr_type)
    file_out = common.file_format.correlations_same_pathway_report.format(cohort, chromosome, norm_type, corr_type)
    overall_file_out = common.file_format.correlations_same_pathway_overall_report.format(cohort, chromosome, norm_type, corr_type)

    json_write_dict = dict()
    overall_json_write_dict = dict()

    print("loading files...")
    df = pd.read_pickle(formatCorrPkl(cohort, chromosome, norm_type))
    with open(common.file_format.gene_association_report_meta_json.format(cohort, chromosome, norm_type, "all"), "r") as fi:
        pathway_json = json.load(fi)

    print("parsing files...")
    pathway_genes_with_cpgs = {gene: list() for gene in pathway_json["cpg_to_gene_name"].values()}
    for gene in pathway_genes_with_cpgs.keys():
        pathway_genes_with_cpgs[gene] = [k for k, v in pathway_json["cpg_to_gene_name"].items() if v == gene]

    pathway_gene_combinations = set()
    for genes in pathway_json["common_pathways"].values():
        pathway_gene_combinations.add(tuple(genes))
    json_write_dict["identified_overlapping_pathways"] = len(pathway_gene_combinations)
    overall_json_write_dict["identified_overlapping_pathways"] = len(pathway_gene_combinations)

    print("going over genes and cpg sites...")
    # detected genes for json
    detected_genes = set()
    for x in pathway_gene_combinations:
        for xx in x:
            detected_genes.add(xx)
    json_write_dict["n_assessed_genes"] = len(detected_genes)
    overall_json_write_dict["n_assessed_genes"] = len(detected_genes)

    # number of cpg sites for json
    json_write_dict["n_cpg_sites"] = sum([len(x) for x in pathway_genes_with_cpgs if x in detected_genes])
    overall_json_write_dict["n_cpg_sites"] = df.shape[0]

    print("appending correlations...")
    correlations = list()
    for combination in pathway_gene_combinations:
        pathway_cpg_lists = list()
        for gene in combination:
            pathway_cpg_lists.append(pathway_genes_with_cpgs[gene])
        for ix, cpg_list_x in enumerate(pathway_cpg_lists):
            for iy, cpg_list_y in enumerate(pathway_cpg_lists):
                if ix == iy:
                    continue
                for cpg_x in cpg_list_x:
                    for cpg_y in cpg_list_y:
                        correlations.append(df.loc[cpg_x].loc[cpg_y])

    print("splitting correlations...")
    negatives = [x for x in correlations if x < 0]
    positives = [x for x in correlations if x >= 0]
    json_write_dict["n_positive_correlating_pairs"] = len(positives)
    json_write_dict["n_negative_correlating_pairs"] = len(negatives)
    json_write_dict["mean_positive_correlation"] = sum(positives)/len(positives)
    json_write_dict["mean_negative_correlation"] = sum(negatives)/len(negatives)

    print("removing diagonal from main array...")
    # remove diagonal and triangle from the array
    df = df.to_numpy()
    df = df[np.triu_indices(n=df.shape[0], k=1)]
    positive_overall = df[df >= 0]
    negative_overall = df[df < 0]

    overall_json_write_dict["n_positive_correlating_pairs"] = len(positive_overall)
    overall_json_write_dict["n_negative_correlating_pairs"] = len(negative_overall)
    overall_json_write_dict["mean_positive_correlation"] = np.mean(positive_overall)
    overall_json_write_dict["mean_negative_correlation"] = np.mean(negative_overall)


    print("running positive ANOVA...")
    positive_anova_results = stats.f_oneway(positives, positive_overall)
    
    print("running negative ANOVA...")
    negative_anova_results = stats.f_oneway(negatives, negative_overall)
    
    json_write_dict["anova_positive"] = {"f": positive_anova_results.statistic, "p": positive_anova_results.pvalue}
    json_write_dict["anova_negative"] = {"f": negative_anova_results.statistic, "p": negative_anova_results.pvalue}
    overall_json_write_dict["anova_positive"] = {"f": positive_anova_results.statistic, "p": positive_anova_results.pvalue}  # write the same things into both, can use to check for consistency if required
    overall_json_write_dict["anova_negative"] = {"f": negative_anova_results.statistic, "p": negative_anova_results.pvalue}

    with open(file_out, "w") as fo:
        json.dump(json_write_dict, fo)
    print("wrote to", file_out)

    with open(overall_file_out, "w") as fo:
        json.dump(overall_json_write_dict, fo)
    print("wrote to", overall_file_out)


if __name__ == "__main__":
    for chromosome_ in common.bio.epic_probes_smallest_to_largest[13:]:  # doing the later ones
        run("chds", chromosome_, common.bio.appropriate_norm_type[chromosome_], "spearman")