"""
Splits beta CSV files for the selected cohort and normalisation types into individual chromosome CSV files
"""
import pandas as pd
import common.bio
import common.file_format
import data.info


def run(cohort, norm_types, droplist=None):
    array_type = data.info.getArrayType(cohort)
    print(array_type)

    for norm_type in norm_types:
        beta_mat = pd.read_csv(common.file_format.betacsv_all.format(cohort, norm_type), index_col=0)
        if droplist:
            beta_mat.drop(droplist, axis=1, inplace=True)  # drop columns
        for chromosome in common.bio.chromosomes:
            with open(common.file_format.cpgsites_per_chromosome.format(array_type, chromosome.upper()), "r") as fi:
                sites = [x.strip() for x in fi.readlines()]
            beta_mat.loc[sites].to_csv(common.file_format.betacsv_per_chromosome.format(cohort, chromosome, norm_type))
            print("Wrote file for: {} | {}".format(norm_type, chromosome))


if __name__ == "__main__":
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('cohort')
    parsed_args = parser.parse_args()
    run(parsed_args.cohort, common.bio.norm_methods)
    '''
    cohort_ = "chds"
    run(cohort_, ["noob"], data.info.droplists[cohort_])
